/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol;

/**
 * This is used to represent a Cobol datatype. A lot of the stuff here is just to normalize and parse out the
 * information.
 * Really needs a review with a Cobol textbook and TestNG! It doesn't cover all the data types of course.
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CobolDataType {
    private final static Logger _log = LoggerFactory.getLogger(CobolDataType.class);
    public static final String LOW_VALUES = null;
    public static final String SPACE = " ";

    private static Pattern pic9n;

    private static Pattern picX;

    private static Pattern picS9n;

    private static Pattern picS9nV;

    private static Pattern picS9nVn;

    private static List<Pattern> validCobolPatterns;

    private static Pattern picXn;

    private static Pattern pic9;


    static {
        validCobolPatterns = new ArrayList<Pattern>();

        pic9 = Pattern.compile("^(9+)$"); // 9(nnn) , note max should be 18
        pic9n = Pattern.compile("9\\((\\d+)\\)"); // 9(nnn) , note max should be 18
        picX = Pattern.compile("^(X+)"); // X, should normalize to X(1)   -- note this ignore anything acter X(2) like OCCURS x times.
        picXn = Pattern.compile("^X\\((\\d+)\\)"); // X(nnn)
        picS9n = Pattern.compile("S?9\\((\\d+)\\)(COMP-3|COMP-4|BINARY|COMP-1)?"); // S9(nnn)
        picS9nV = Pattern.compile("S?9\\((\\d+)\\)V(9+)(COMP-3|COMP-4|BINARY|COMP-1)?"); // S9(nnn)V99
        picS9nVn = Pattern.compile("S?9\\((\\d+)\\)V9\\((\\d+)\\)(COMP-3|COMP-4|BINARY|COMP-1)?"); // S9(nnn)V(nnn)

        validCobolPatterns.add(picX);
        validCobolPatterns.add(picXn);

        // For CICS-WS PIC 9 cannot exceed length of 18, so seperated out. Note no compression allowed.
        // validCobolPatterns.add(pic9);
        // validCobolPatterns.add(pic9n);

        validCobolPatterns.add(picS9n);
        validCobolPatterns.add(picS9nV);
        validCobolPatterns.add(picS9nVn);


        //validCobolPatterns.add(pic9nComp);

    }


    private String rawType;
    private String normalizedType;
    private Integer lengthInBytes;
    private Integer numDecimals;
    private Integer numDigits;
    private boolean signed;
    private boolean comp1;
    private boolean comp3;
    private boolean binary;

    public CobolDataType(String descriptor) throws Exception {
        this.rawType = descriptor;
        this.normalizedType = normalizeCobol(this.rawType);
        this.lengthInBytes = parse(this.normalizedType);

    }


    /**
     * Converts Cobol datatype to canonical format, like pic xx to X(2). Always removes the PIC for now
     */
    private static String normalizeCobol(String type) {
        String res = type.toUpperCase().trim();
        res = res.replace(" ", ""); // remove all internal spaces.
        res = res.replace("COMP3", "COMP-3");
        res = res.replace("PIC", ""); // Remove all PIC
        res = res.replace("USAGE", ""); // Some wipes put usage before comp-3
        if (res.endsWith("COMP")) {
            res = res.replace("COMP", "COMP-1");
        }
        if (!isCobolDatatype(res)) {
            throw new IllegalArgumentException("Invalid Cobol Datatype [" + type + "]");
        }
        return res;
    }


    /**
     * Checks to see if the normalized Cobol type string matches one of our known valid patterns.
     *
     * @param type Normalized text describing cobol datatype.
     * @return true if we know about the datatype and can proces, else false.
     */
    private static boolean isCobolDatatype(String type) {
        for (Pattern p : validCobolPatterns) {
            Matcher m = p.matcher(type);
            if (m.matches()) {
                return true;
            }
        }
        _log.warn("Found Non-Valid Cobol Datatype: [" + type + "]");
        return false;
    }

    /**
     * Goes through and extract all needed information from the Cobol datatype, doing a few validations.
     * On completion, the number of digits, the number of decimal digits are set if numeric.
     *
     * @param type Normlized Cobol datatype string.
     * @return Length in bytes of the native cobol represenation of the datatype.
     */
    private int parse(String type) throws Exception {
        try {
            for (Pattern p : validCobolPatterns) {
                Matcher m = p.matcher(type);
                if (m.matches()) {
                    _log.debug("Type: [" + type + "] Matched: " + p.pattern() + " Number of Groups: " + m.groupCount());
                    if (p == picXn || p == picX) {
                        Integer len = new Integer(m.group(1));
                        _log.debug("Len: " + len);
                        return len;
                    }

                    signed = type.startsWith("S");
                    comp1 = type.endsWith("COMP-1");
                    comp3 = type.endsWith("COMP-3");
                    binary = type.endsWith("BINARY") || type.endsWith("COMP-4");
                    boolean packed = comp1 | comp3 | binary;

                    int digitLength = 0;        // This is the number of decimal and integer digits (excluding decimal point)
                    // Secret is that the picS9.... stuff all match signed and unsigned.
                    if (p == picS9n) {
                        digitLength = new Integer(m.group(1));
                        numDigits = digitLength;
                        numDecimals = 0;
                        _log.debug("S9(n) Length {}", numDigits);
                    } else if (p == picS9nV) {
                        numDecimals = m.group(2).length();
                        numDigits = new Integer(m.group(1));
                        digitLength = numDecimals + numDigits;
                    } else if (p == picS9nVn) {
                        numDigits = new Integer(m.group(1));
                        numDecimals = new Integer(m.group(2));
                        digitLength = numDecimals + numDigits;
                    } else {
                        throw new Exception("Matched Unhandled Pattern " + p);
                    }

                    // Okay, now we have the number of digits in a decimal field. May be signed or compressed.
                    if (!packed) { // Pure text format
                        // If it is signed and has a decimal point add one else return num digits.
                        if (signed && (numDecimals != null) && (numDecimals != 0L)) {
                            return digitLength + 1;        // Signed
                        } else {
                            return digitLength;
                        }
                    }

                    if (comp1) {
                        // There is a standard comp1 forumla, but also for some things we need to pad
                        // like in the BRIH headers it seems. Not sure how to engineer this.
                        if (isInteger()) {
                            return  digitLength/2; // TODO: Made up comp-1 length calculation True or not?
                        }
                    }

                    if (comp3) {
                        return digitLength / 2 + 1;  // 2 digits per byte + end byte with sign and last odd digit
                    }

                    if (signed && binary) { // S9(4)BINARY is common in OPN header :-(
                        if (digitLength <= 4) {
                            return 2;
                        }
                        if (digitLength <= 9) {
                            return 4;
                        }
                        if (digitLength <= 18) {
                            return 8;
                        }
                    }

                    // Note we are not handling unsigned COMP-4 / BINARY

                    throw new Exception("Didn't know how to compute compressed length for field type " +type);

                }
            }
            throw new Exception("No Pattern Matched DataType");
        } catch (Exception e) {
            //  return 0;
            throw new Exception("Could not determine length of type [" + type + "]", e);
        }
    }

    public int getLengthInBytes() throws Exception {
        return lengthInBytes;
    }

    public String getNormalized() {
        return normalizedType;
    }

    /**
     * Determines the number of decimal places this datatype supports.
     * For PIC X returns zero.
     *
     * @return
     */
    public int getNumDecimalPlaces() {
        if (numDecimals == null) {
            return 0;
        } else {
            return numDecimals;
        }
    }

    /**
     * Return number of integer digits (left of decimal)
     *
     * @return
     */
    public int getNumDigits() {
        if (numDigits == null) {
            return 0;
        } else {
            return numDigits;
        }
    }

    public boolean isPICX() {
        return this.normalizedType.startsWith("X");
    }

    /**
     * This does not preclude the field having some sort of compression, e.g. COMP-3
     * or having decimal placed.
     *
     * @return
     */
    public boolean isPIC9() {
        return this.normalizedType.startsWith("9");
    }

    /**
     * This does not preclude the field having some sort of compression, e.g. COMP-3
     *
     * @return
     */
    public boolean isPICS9() {
        return this.normalizedType.startsWith("S9");
    }

    public boolean isCOMP1() {
        return comp1;
    }

    public boolean isCOMP3() {
        return comp3;
    }

    /**
     * COMP-4 and Binary are same thing?
     * @return
     */
    public boolean isCOMP4() {
        return binary;
    }

    public boolean isBinary() {
        return this.normalizedType.contains("BINARY");
    }



    /**
     * @return True is PIC 9 or PIC S9 regardless of number of decimal places or COMP formatting.
     */
    public boolean isNumeric() {
        return isPIC9() || isPICS9();
    }

    /**
     * @return true is COMP-3, COMP-4 or BINARY (COMP) or COMP-1 else false.
     */
    public boolean isPacked() {
        return isCOMP3() || isCOMP4() || isBinary() || isCOMP1();
    }

    public boolean isInteger() {
        return !(this.getNumDecimalPlaces() > 0);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CobolDataType");
        sb.append("{lengthInBytes=").append(lengthInBytes);
        sb.append(", normalizedType='").append(normalizedType).append('\'');
        sb.append(", numDecimals=").append(numDecimals);
        sb.append(", numDigits=").append(numDigits);
        sb.append(", rawType='").append(rawType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
