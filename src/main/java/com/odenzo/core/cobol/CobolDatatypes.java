/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol; /**
 *
 * User: stevef
 * Date: Mar 19, 2009
 * $Id: CobolDatatypes.java 161 2012-05-14 07:45:43Z e1040775 $
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * These stores information about Cobol datatype, including allowability in WSDL.
 * Part of this functionality is to limit PIC 9(n) to n <=18
 * TODO: In Progress
 */
@Deprecated
public class CobolDatatypes {
	private final static Logger _log = LoggerFactory.getLogger(CobolDatatypes.class);

	/*
	 * Known types:   X(n)  is character based and takes n characters/bytes
	 *                x is a single character
	 *               9(n) is character based number taking n characters
	 *               S9(13)V99 Means 13 digits and then 2 decimal points, can have COMP-3
	 *               Segment means occurance record
	 */

	/**
	 * Current "A" for alphanumeric PIC X, "N" for PIC 9,  "P" for COMP-3  and B for Binary
	 */
	private String type;

	/**
	 * Type as specific in source document
	 */
	private String rawType;

	private Integer numDecimals;

	private Character sign;


	/**
	 * Initializes a datatype based on requirements excel Description. This is varied to say the least.
	 * @param desc
	 */
	public CobolDatatypes(String desc) {
		String val = desc.toUpperCase().trim();
		_log.debug("Source Value ["+val+"]");

		// All may start with PIC so remove
		if (val.startsWith("pic")) {
			val = val.substring(3);
			_log.debug("Normalized PIC ["+val+"]");			
		}


		boolean comp3 = false;

		if (val.contains("comp-3")) {
			comp3 = true;
			val = val.replace("comp-3", "");
		}
		if (val.contains("comp 3")) {
			comp3 = true;
			val = val.replace("comp 3", "");
		}



	}

	/**
	 * Length in Bytes
	 */
	private int length;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRawType() {
		return rawType;
	}

	public void setRawType(String rawType) {
		this.rawType = rawType;
	}

	public Integer getNumDecimals() {
		return numDecimals;
	}

	public void setNumDecimals(Integer numDecimals) {
		this.numDecimals = numDecimals;
	}

	public Character getSign() {
		return sign;
	}

	public void setSign(Character sign) {
		this.sign = sign;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
