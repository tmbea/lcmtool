/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * TODO: Move to StringUtils or maybe FormattingUtils.
 * Created by IntelliJ IDEA.
 * User: stevef
 * Date: Oct 7, 2009
 * Time: 10:42:32 AM
 *
 * @version $Id: CopybookUtils.java 161 2012-05-14 07:45:43Z e1040775 $
 *          <p/>
 *
 */
public class CopybookUtils {
	private final static Logger _log = LoggerFactory.getLogger(CopybookUtils.class);

	static String spaces;

	static {
		spaces = "     ";
		for (int i = 0; i < 20; i++) {
			spaces += spaces;
		}
	}

	/**
	 * If n is less than one that exactly one space is returned.
	 *
	 * @param n
	 * @return
	 */
	static String getNSpaces(int n) {
        if (n > spaces.length()) {
            throw new IllegalArgumentException("Can only get " + spaces.length() + " spaces max. Not " + n);
        }
		n = Math.max(1, n);
		return spaces.substring(0, n);
	}

	/**
	 * This left zero pads the src string to total length of limit and returns new string.
	 *
	 * @param strbuf
	 * @param limit
	 * @return
	 * @throws java.io.IOException
	 */
	static String getZero(String strbuf, int limit) throws IOException {

		try {
			int cnt = strbuf.length();
			int len = limit - cnt;
			String buf = "";
			for (int i = 0; i < len; i++) {
				buf += "0";
			}
			return buf + strbuf;
		} catch (Exception e) {
			return "";
		}

	}

	/**
	 * Left pads with spaces
	 *
	 * @param strbuf
	 * @param limit
	 * @return
	 * @throws java.io.IOException
	 */
	static String appendSpace(String strbuf, int limit) throws IOException {

		try {
			int cnt = strbuf.length();
			if (cnt > 10) {
				return strbuf.substring(0, 10);
			} else {
				int len = limit - cnt;
				String buf = "";
				for (int i = 0; i < len; i++) {
					buf += " ";
				}
				return strbuf + buf;
			}
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * This truncates strbuf to the limit and appends a space if its greater than limit, else return strbuf.
	 *
	 * @param strbuf
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	static String limitFieldName(String strbuf, int limit) throws Exception {

		String strLimit = "";
		try {
			int cnt = strbuf.length();
			if (cnt > limit) {
				strLimit = strbuf.substring(0, limit) + " ";
			} else {
				strLimit = strbuf;
			}
			return strLimit;
		} catch (Exception e) {
			throw new Exception("Trouble limitFieldName to " + limit + " for String " + strbuf, e);
		}
	}

}
