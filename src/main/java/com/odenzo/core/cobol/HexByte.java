package com.odenzo.core.cobol;

import com.odenzo.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a simple class to represent a Hex value. These (in BAY) are stored in PIC X() fields.
 * I am not sure the bit by bit representation so need to extract.
 * Now I think we need even pair of hex digits since everything is by Byte.
 * Like said, not sure if this is really hex digits as one  EBCDIC character (codepoint) or what in Cobol. Ask Supinya.
 * And not sure if dependant on the codepage being used on the mainframe.
 */
public class HexByte {
    final public static Logger _log = LoggerFactory.getLogger(HexByte.class);

    /**
     * Short is 2 bytes (2-s complement), so plenty of room for 1 byte. Values between 0-256 will be represented
     * the same in two complement as in base 2 so no worries.
     * Not that a char is 2 bytes unsigned but not messing with that yet.
     * It still seems that bit operators work on int only?
     */
    private int val;

    /**
     * Example usage  new HexByte(0xFA).
     *
     * @param val Value between 0 and 255 inclusive (0x00 to 0xFF)
     */
    public HexByte(int val) {
        if (val < 0 || val > 255) {
            throw new IllegalArgumentException("Value must be between 0 and 255 not " + String.valueOf(val));
        }
        this.val = val;

    }

    /**
     * @param bitNum     Value between 0 and 7, high bit is 7.
     * @param setOrClear
     */
    public void setBit(int bitNum, boolean setOrClear) {
        if (bitNum < 0 || bitNum > 7) {
            throw new IllegalArgumentException("Bit Number must be between 0 and 7, not " + bitNum);
        }
        int mask = 0x80 >> bitNum;

        if (setOrClear) { // Set the bit
            val |= mask;
        } else { // Clear the bit
            val &= (~mask);
        }
    }

    public void setBit(int bitNum) {
        setBit(bitNum, true);
    }

    public void clearBit(int bitNum) {
        setBit(bitNum, false);
    }

    public boolean isSet(int bitNum) {
        return ((val ^ (0x00001<<bitNum)) > 0);
    }

    /**
     * OK, this little ditty could be in HexValue or HexByte.
     * In here now, even though it will be BAY specific. This hex byte is going to go into a Copybook PIC X field in CP838/IBM-Thai encoding.
     * The transport over CICS-WS or CTG is not decided, but the encoding of CICS-WS or CTG packet will have to be non transcoded.
     * Therefor, just putting the actually binary value will work.
     * So, it may be generic after all.
     *
     * @return Base2 encoded representation of the Hex Digits. NOT 2-complement.
     */
    public byte getCobolByte() {
        /* Dev Notes:
            If the value is (0...127), we can just cast to byte (truncated) and should be OK.
            If it is negative that casting to byte will give us 2-complement encoding when we want base2 encoding.

            Now, char is NOT base2 encoded, so can test its behavious to make simpler perhaps.
        */

    	byte res = (byte) val;    // integer to byte cast takes int value 255 and wraps to -1 as a byte. I guess this is in the spec?
        _log.debug("Val {} Converted to {}", this.toString(), ByteUtils.byteToBitString(res));
        return res;


    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Hex 0x");
        sb.append(ByteUtils.byteToHexString(this.val));
        return sb.toString();
    }
}
