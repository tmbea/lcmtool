package com.odenzo.core.cobol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a simple class to represent a Hex value. These (in BAY) are stored in PIC X() fields.
 * A HexValue is an arbitrarily long sequence of Hex digits. If it is odd length the a 0 is added to the left.
 */
public class HexValue {
    final public static Logger _log = LoggerFactory.getLogger(HexValue.class);
    private String charVal;

    private List<HexByte> hexBytes;


    /**
     * Create a hex value by String
     *
     * @param val Format is eithe a series of Hex digits [0-F] or 0x[HexDigits]
     */
    public HexValue(String val) throws IOException {
        if (val.startsWith("0x")) {
            val = val.substring(2);
        }

        this.charVal = val;
        if (this.charVal.length() % 2 != 0) {
            this.charVal = "0" + charVal;
        }
        

        _log.debug("Set Value to {} based on source {}", this.charVal, val);
        int numBytes = this.charVal.length() / 2;
        String[] byteStrs = new String[numBytes];
        
        for (int i=0, j=0; i<val.length(); i+=2){
        	byteStrs[j++]=val.substring(i, i+2);
        }
        
        hexBytes = new ArrayList<HexByte>(numBytes);
        for (String byteStr : byteStrs) {
            hexBytes.add(new HexByte(Short.valueOf(byteStr, 16)));
        }

    }

    public byte[] getCobolBytes() {
        // Eventually need somethign efficient to pop into a ByteBuffer at some location.
        byte[] res = new byte[hexBytes.size()];
        int indx = 0;
        for (HexByte b : hexBytes) {
            res[indx++] = b.getCobolByte();
        }
        return res;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Hex 0x");
        sb.append(charVal);
        return sb.toString();
    }
}
