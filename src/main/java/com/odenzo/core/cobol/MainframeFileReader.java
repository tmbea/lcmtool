/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


/**
 * Like line number reader but only uses \n as end-of-line character.
 * It does some goofy stuff because a mix of binary and text.
 * Not very safe file access re closing down stuff.
 * TO-DO: May want to download as binary fixed record to avoid all this line ending nonsense.
 * TODO: Generify this, really its fixed record file loading eh?
 * @author stevef
 * @version $Id: MainframeFileReader.java 161 2012-05-14 07:45:43Z e1040775 $
 */

public class MainframeFileReader {
	private static Logger _log = LoggerFactory.getLogger(MainframeFileReader.class);

	private BufferedReader buffReader;

	/**
	 * The current line number
	 */
	private int lineNumber = 0;
	private FileInputStream fr;
	private InputStreamReader fsr;
	private String mode = "Text";   // Text, Binary of FixedLength
	private int fixedLength = 9305;
	private String encoding;

	/**
	 * This treats each line as a set number of bytes. In practice, some of these bytes are NULL (^@) and some
	 *
	 * @param inputFile
	 * @param lineLength If null then 9305 or 15000
	 */
	public MainframeFileReader(File inputFile, String encoding, Integer lineLength) throws Exception {
		try {
			if (lineLength != null) {
				fixedLength = lineLength;
			}
			mode = "FixedLength";
			this.encoding = encoding;
			fr = new FileInputStream(inputFile);
			long totalLength = inputFile.length();
			long numRecords = totalLength / fixedLength;
			_log.info("Total File Length: " + totalLength + " with record size " + fixedLength + " gives " + numRecords + " records.");
			_log.info("Number of Stray Bytes: " + totalLength % fixedLength);
		} catch (FileNotFoundException e) {
			throw new IOException("Trouble Initializing FixedLength Record MainframeFileReader", e);
		}
	}

	/**
	 * A MainFrameFileReader instanciated with this constructor will attempt to read the file as text in the given encoding.
	 * Other consutructors use different methods to deal with embedded control charaters in the semi-binary file.
	 *
	 * @param inputfile
	 * @param encoding
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.UnsupportedEncodingException
	 */
	public MainframeFileReader(File inputfile, String encoding) throws FileNotFoundException, UnsupportedEncodingException {
		fr = new FileInputStream(inputfile);
		fsr = new InputStreamReader(fr, encoding);
		buffReader = new BufferedReader(fsr);
	}


	/**
	 * Reads a line of text.  A line is considered to be terminated by any one
	 * of a line feed ('\n'), a carriage return ('\r'), or a carriage return
	 * followed immediately by a linefeed.
	 *
  	 * @return A String containing the contents of the line, not including
	 *         any line-termination characters, or null if the end of the
	 *         stream has been reached
	 * @throws java.io.IOException If an I/O error occurs
	 * @see java.io.LineNumberReader#readLine()
	 */
	public String readLine() throws Exception {
		if (mode.equalsIgnoreCase("FixedLength")) {
			return readFixedLengthRecord();
		} else {
			return readTextLine();
		}
	}

	public String readFixedLengthRecord() throws Exception {
		try {
			byte[] buf = new byte[fixedLength];
			int bytesRead = fr.read(buf);
			_log.debug("Bytes Read: " + bytesRead + " out of " + fixedLength);
			if (bytesRead < 0) {
				return null;
			}
			lineNumber++;
			String res = new String(buf, encoding);
			_log.debug("Binary Record [" + res + "]");
			return res;
		} catch (IOException e) {
			throw new IOException("Trouble Reading FixedLengthRecord (" + fixedLength + ")", e);
		}
	}

	public String readTextLine() throws IOException {
		try {
			StringBuilder sBuilder = new StringBuilder();
			while (true) {
				int ch = buffReader.read();
				if (ch == -1) { // This is the end of file marker
					lineNumber++;
					if (sBuilder.length() == 0) {
						return null;
					} else {
						return sBuilder.toString();
					}
				}

				if (ch == '\n' && checkIfReallyEOL()) {
					// Want to check if its really end of line or stray binary
					// so check if next two characters are 00 for now. Need better way...
					lineNumber++;
					if (sBuilder.length() == 0) {
						return null;
					} else {
						return sBuilder.toString();
					}
				}

				// Else append character -- note this may be EOL character.
				char chAsChar = (char) ch;
				sBuilder.append(chAsChar);
			}
		} catch (Exception e) {
			throw new IOException("Trouble Reading Line", e);
		}
	}

    /**
     * FIXME: Client specific code.
     * @return
     * @throws java.io.IOException
     */
	protected boolean checkIfReallyEOL() throws IOException {
		buffReader.mark(20);
		try {
			boolean res = false;
			char[] startOfNextLine = new char[11];
			for (int i = 0; i < 11; i++) {
				int st = buffReader.read();
				if (st == -1) {
					_log.debug("Got to end of file!");
					return true;
				}
				startOfNextLine[i] = (char) st;
			}
			String val = new String(startOfNextLine);

			String trackNo = val.substring(0, 7);
			String msgTypeToken = val.substring(7, 11);

			if (msgTypeToken.equals("TCBI") || msgTypeToken.equals("TCBO")
					|| msgTypeToken.equals("OUTP") || msgTypeToken.equals("INPT")
					|| msgTypeToken.equals("CISI") || msgTypeToken.equals("CISO")) {
				return true;
			} else {
				_log.debug("Line #: " + getLineNumber() + "  Start of Line[" + val + "]");
				_log.debug("Next Line Token: " + msgTypeToken);
			}
			return res;
		} finally {
			buffReader.reset();
		}
	}

	public void close() throws IOException {
		if (buffReader != null) {
			buffReader.close();
			this.fsr.close();
		}
		this.fr.close();
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
}
