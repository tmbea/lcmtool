/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.conversions; /**
 *
 * User: Steve Franks
 * Date: Oct 25, 2009
 * Source Code Version: $Id: AlternaticeCompCoverter.java 161 2012-05-14 07:45:43Z e1040775 $
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.BitSet;

/**
 * Also see: http://search.cpan.org/~grommel/Convert-IBM390-0.27/lib/Convert/IBM390.pod
 *
 * @author Linda Fisher linda@room42.com
 * @version 1.0 31 Dec 1998
 */
public class AlternaticeCompCoverter {
	private final static Logger _log = LoggerFactory.getLogger(AlternaticeCompCoverter.class);


	private static File input_file;
	private static FileInputStream input;
	private static File output_file;
	private static FileOutputStream output;
	private static byte hexbyte;

	/**
	 * Description:   base 10 int changed to two 4 bit BitSet's
	 *
	 * @param hi the byte the BitSet's are to be created from
	 * @return BitSet[] the BitSet's created from the byte
	 * @throws
	 */
	private final static BitSet[] bitSetsFromBase10(byte hi) {
		BitSet bits[] = new BitSet[2];
		bits[0] = new BitSet(4);
		bits[1] = new BitSet(4);
		int bb = new Integer(hi).intValue();
		int m7 = bb / 128;
		bb = (bb - (m7 * 128));
		if (m7 > 0) {
			bits[0].set(3);
		}
		int m6 = bb / 64;
		bb = (bb - (m6 * 64));
		if (m6 > 0) {
			bits[0].set(2);
		}
		int m5 = bb / 32;
		bb = (bb - (m5 * 32));
		if (m5 > 0) {
			bits[0].set(1);
		}
		int m4 = bb / 16;
		bb = (bb - (m4 * 16));
		if (m4 > 0) {
			bits[0].set(0);
		}
		int m3 = bb / 8;
		bb = (bb - (m3 * 8));
		if (m3 > 0) {
			bits[1].set(3);
		}
		int m2 = bb / 4;
		bb = (bb - (m2 * 4));
		if (m2 > 0) {
			bits[1].set(2);
		}
		int m1 = bb / 2;
		bb = (bb - (m1 * 2));
		if (m1 > 0) {
			bits[1].set(1);
		}
		int m0 = bb;
		if (m0 > 0) {
			bits[1].set(0);
		}
		return (bits);
	}

	/**
	 * Description:   bit settings changed to base 10
	 *
	 * @param bs the BitSet the decimal integer is to be created from
	 * @return bb the int created from the BitSet
	 * @throws
	 */
	private final static int intBase10FromBitSet(BitSet bs) {
		// bit settings changed to base 10
		boolean m0 = bs.get(0);
		boolean m1 = bs.get(1);
		boolean m2 = bs.get(2);
		boolean m3 = bs.get(3);

		int bb = 0;
		if (m0) {
			bb = bb + 1;
		}
		if (m1) {
			bb = bb + 2;
		}
		if (m2) {
			bb = bb + 4;
		}
		if (m3) {
			bb = bb + 8;
		}
		return (bb);
	}

	/**
	 * Description:   bit settings changed to base 10
	 *
	 * @param bs the BitSet the zoned format decimal integer is to be created from
	 * @return bb the int created from the BitSet
	 * @throws
	 */
	private final static int intBase10ZonedFromBitSet(BitSet bs) {
		boolean m0 = bs.get(0);
		boolean m1 = bs.get(1);
		boolean m2 = bs.get(2);
		boolean m3 = bs.get(3);

		int bb = 0;
		if (m0) {
			bb = bb + 1;
		}
		if (m1) {
			bb = bb + 2;
		}
		if (m2) {
			bb = bb + 4;
		}
		if (m3) {
			bb = bb + 8;
		}
		if (bb < 10) {
			bb = bb + 240;
		} else if (bb == 10) {     /* positive */
			bb = bb + 240;
		} else if (bb == 11) {     /* negative */
			bb = bb + 208;
		} else if (bb == 12) {     /* positive */
			bb = bb + 240;
		} else if (bb == 13) {     /* negative */
			bb = bb + 208;
		} else if (bb == 14) {     /* positive */
			bb = bb + 240;
		} else if (bb == 15) {     /* positive */
			bb = bb + 240;
		}
		return (bb);
	}

	public static void main(String args[]) {
		try {
			input_file = new File("input.file");
			input = new FileInputStream(input_file);
			output_file = new File("output.file");
			output = new FileOutputStream(output_file);
		} catch (Exception e) {
			System.out.println("Something went wrong with the files " + e);
		}
		try {
			/*  simple 16 bit - 2 bytes packed decimal converted to zoned format decimal integer  */
			/*  123C (positive) converted to F1F2 F3  or 123D (negitive) converted to F1F2 D3     */

			// toZoned();
			//  withSign();

		} catch (Exception e) {
			System.out.println("Something went wrong with reading the file " + e);
		}
	}

	private static byte readByte() throws IOException {
		int ch = input.read();
		if (ch < 0) {
			throw new EOFException();
		}
		return (byte) (ch);

	}

	/**
	 * Description: both sets of 4 bits in an 8 bit byte converted to zoned format decimal integer
	 */
	private static void toZoned(byte hexbyte) throws IOException {

		BitSet these_bits[] = bitSetsFromBase10(hexbyte);
		int b1 = intBase10ZonedFromBitSet(these_bits[0]);
		output.write(b1);
		int b2 = intBase10ZonedFromBitSet(these_bits[1]);
		output.write((int) b2);
	}

	/**
	 * Description: left most 4 bits in an 8 bit byte converted to zoned format decimal integer
	 * right most 4 bits in an 8 bit byte converted to zoned format sign
	 */
	private static void withSign(byte hexbyte) throws IOException {

		_log.debug("Converted Sign Byte " + hexbyte);
		BitSet those_bits[] = bitSetsFromBase10(hexbyte);
		int b1 = intBase10FromBitSet(those_bits[0]);
		int b2 = intBase10FromBitSet(those_bits[1]);
		_log.debug(b1 + " sign byte is " + b2);
		if (b2 == 10) {          /* positive */
			b1 = b1 + 240;
		} else if (b2 == 11) {   /* negative */
			b1 = b1 + 208;
		} else if (b2 == 12) {   /* positive */
			b1 = b1 + 240;
		} else if (b2 == 13) {   /* negative */
			b1 = b1 + 208;
		} else if (b2 == 14) {   /* positive */
			b1 = b1 + 240;
		} else if (b2 == 15) {   /* positive */
			b1 = b1 + 240;
		}
		output.write((int) b1);
	}
}

