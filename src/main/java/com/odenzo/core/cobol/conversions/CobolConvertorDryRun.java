/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.conversions;

import com.odenzo.core.cobol.CobolDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO: Move to tests.
 * @author stevef
 * @version $Id: CobolConvertorDryRun.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class CobolConvertorDryRun {
	private static Logger _log = LoggerFactory.getLogger(CobolConvertorDryRun.class);

	public static void main(String[] args) {
		try {
			ConversionFacade convertor = new ConversionFacade("IBM Thai");
			CobolDataType type = new CobolDataType("PIC S9(4)V99 COMP-3");
			byte[] bits = convertor.convertTextToCobol(type, "-100.33");
		} catch (Exception e) {
			_log.error("Problem", e);
		}
	}


	public byte[] newRoutine(CobolDataType type, String xmlValue) {
		return null;
	}
}
