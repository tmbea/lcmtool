/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.conversions; /**
 *
 * User: stevef
 * Date: Mar 19, 2009
 * $Id: CobolDatatypesUtils.java 210 2012-05-19 11:16:20Z e0084502 $
 */


import com.odenzo.core.cobol.HexValue;
import com.odenzo.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * My wrapper class to a hodgepodge of utilities. Really should make a CobolDataType noun class that stores related info about a raw datatype
 */
public class CobolDatatypesUtils {
	private final static Logger _log = LoggerFactory.getLogger(CobolDatatypesUtils.class);

	static String decimalPointPattern = "V9\\((\\d+)\\)";       // Find V9(x)
	static String decimalRepearedPattern = "V(9+)"; // Find V99..
	static String packedDecimalPattern = "S?9(\\(\\d+\\)).+";
	public static void main(String[] args) {
		// Unit test... hahaha
		try {
			// Want to send these bytes escaped" &#0;&#0;&#0;&#0;&#0;&#0;&#16;&#12;
			//byte[] bytes = {0, 0, 0, 0, 0, 0, 16, 12}; // I think the &# stuff is hex?
			//String val = convertPackedToString(bytes, "S9(13)V99COMP-3");
			
			byte[] bytes = convertStringToSignedPacked("180", 4);
			ByteUtils.dumpBytes(_log, Charset.forName("IBM-Thai"), bytes);
			//_log.debug("Result: [" + val + "]");
		} catch (Exception e) {
			_log.error("Error", e);
		}
	}

	/*
		  * Known types:   X(n)  is character based and takes n characters/bytes
		  *                x is a single character
		  *               9(n) is character based number taking n characters
		  *               S9(13)V99 Means 13 digits and then 2 decimal points, can have COMP-3
		  *               Segment means occurance record
		  */


	static private String decimalizeString(String val, int numDecimals) {
		int len = val.length();
		String formatted = val.substring(0, len - numDecimals) + ".";
		formatted += val.substring(len - numDecimals);
		return formatted;
	}


	public static String convertPackedToString(byte[] bytes, String type) throws Exception {
		String val = CobolComp3Utils.unpackComp3(bytes);
		int numDecimals = extractNumberOfDecimalPointsFromType(type);
		String res = decimalizeString(val, numDecimals);
		return res;
	}
	
	public static byte[] convertStringToUnsignedPacked(String number){
		
		try{
			Integer.parseInt(number);
		}catch(Exception e){
			throw new IllegalArgumentException("Proper number need to passed to convert to unsigned packed.");
		}
		if ((number.length()%2)>0){
			number="0"+number;
		}
		ByteBuffer buffer = ByteBuffer.allocate(number.length()/2);
		for (int i=0; i<number.length(); i+=2){
			String byteStr = number.substring(i, i+2);
			byte firstByte = Byte.parseByte(byteStr.substring(0,1));
			byte secondByte = Byte.parseByte(byteStr.substring(1,2));
			
			firstByte = (byte) (firstByte << 4);
			
			buffer.put((byte) (firstByte|secondByte));
			
		}
		
		return buffer.array();
		
	}


	public static byte[] convertStringToSignedPacked(String number, int len) throws IOException{
		int num;
		try{
            _log.debug("Converting number to Signed Packed -  COMP-1?");
			num = Integer.parseInt(number);
		}catch(Exception e){
			throw new IllegalArgumentException("Proper number need to passed to convert to unsigned packed.");
		}
		
		
		
		String charVal = String.format("%X", num);

		
        if (charVal.length() % 2 != 0) {
            charVal = "0" + charVal;
        }

        _log.debug("Set Value to {} based on source {}", charVal, number);
        int numBytes = charVal.length() / 2;
        byte[] bytes = new byte[len/2];
        try{
        	
        
	        for (int i=charVal.length(), j=bytes.length-1; i>0; i-=2){
	        	byte[] bs =(new HexValue("0x" + charVal.substring(i-2, i))).getCobolBytes();
	        	if (bs.length == 1){
	        		bytes[j--]=bs[0];
	        	}
	        }
	        
        }catch(Exception e){
			throw new IllegalArgumentException("Proper number need to passed to convert to unsigned packed.");

        }

		return bytes;
	}
	
	private static int extractNumberOfDecimalPointsFromType(String type) {
		Pattern p = Pattern.compile(decimalPointPattern);
		Matcher m = p.matcher(type);
		if (m.find()) {
			String x = m.group(1);  // from V9(x)
			return Integer.parseInt(x);
		}

		p = Pattern.compile(decimalRepearedPattern);
		m = p.matcher(type);
		if (m.find()) {
			String x = m.group(1); // V9999 - this has the 999
			return x.length();
		}
		return 0;
	}


	

}
