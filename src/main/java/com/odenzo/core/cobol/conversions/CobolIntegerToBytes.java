/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.conversions;

import com.odenzo.core.cobol.CobolDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * My own hand rolled convertor.
 *
 * @author stevef
 * @version $Id: CobolIntegerToBytes.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class CobolIntegerToBytes {
	private static Logger _log = LoggerFactory.getLogger(CobolIntegerToBytes.class);


	/**
	 * This will handle PIC S9(n) and PIC 9(n) but no decimals please and not packed.
	 *
	 * @param type
	 * @param val
	 */
	public static byte[] convertIntegerToBytes(CobolDataType type, String val) throws Exception {
		try {
// It seems sign is always present and S9(n) is redundant form of 9(n)
			// For each digit need to make 4 bits and then a sign bit and throw them all together somewhere.
		    if (!type.isInteger()) {
                throw new IllegalArgumentException("Must not have decimal places -- "+ type);
            }
            if (type.isPacked()) {
                throw new IllegalArgumentException("This doesn't deal with COMP anything -- "+type);
            }
            Integer intVal = Integer.parseInt(val);
			boolean negative = intVal < 0;
			List<Byte> bytes = new ArrayList<Byte>();
			for (int i = 0; i < val.trim().length(); i++) {
				bytes.add(convertDigitToByte(val.charAt(i)));
			}
			// Okay, now we need to decide if we need a sign and how to compact the Bytes.
			// I think we just take the first digit and put the +/- in the high byte.
			// Note that the list now just has all the low bytes (bottom 4 of 8 bits).
			if (negative) {
				Byte signByte = bytes.get(0);
				//	signByte |= new Byte("xFF00");  // Pretend FF is 4 bits for negative, ask Prasad.
			}
			return null;
		} catch (Exception e) {
			throw new Exception("Trouble converting " + type + " Value " + val + " to bytes.", e);
		}
	}


    /**
     *
     * @param digit Single digit between 0-9
     * @return a Byte value representing the digit (top 4 bits always zero, bottom the hex value of digit)
     * @throws Exception
     */
	private static byte convertDigitToByte(Character digit) throws Exception {
		if (!Character.isDigit(digit)) {
			throw new IllegalArgumentException("Char " + digit + " was not a digit.");
		}

		String hexVal = "0x0" + digit;
		return new Byte(hexVal);

	}
}
