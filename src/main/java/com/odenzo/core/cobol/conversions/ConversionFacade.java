/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.conversions;

import com.odenzo.core.cobol.CobolDataType;
import com.odenzo.core.utils.ByteUtils;
import com.odenzo.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Facade to play around with correct Cobol conversions to/from Java and pure text datatypes.
 * Very inneficient now.
 * 
 *
 * @author stevef
 * @version $Id: ConversionFacade.java 295 2012-06-01 07:59:55Z e0084502 $
 */
public class ConversionFacade {
	private static Logger _log = LoggerFactory.getLogger(ConversionFacade.class);


    private  String mfEncoding = "IBM-Thai"; // aka CP838
    
    public ConversionFacade(String mainframeEncoding) {
        mfEncoding = mainframeEncoding;
    }
	/**
	 * This is used to take text values, and convert to bytes appropriate to the Cobol datatype.
	 * Used for populating Copybook data structures for CTG calls for one thing.
	 * It will pad numeric types with zero on left, and PIC X with, mmmm spaces on left.  Need to review that.
	 * @param type
	 * @param text
	 * @return
	 */
	public byte[] convertTextToCobol(CobolDataType type, String text) throws Exception {
		try {
			if (type.isPICX()) {
				String val = text.trim();
				if (val.length() > type.getLengthInBytes()) {
					throw new IllegalArgumentException("Value: [" + text + "] Len: " + val.length()+" too long for datatype " + type);
				}
				// Note the assumption that one character is one byte. This is also validated.
				val = StringUtils.leftAlignRightPadTruncating(val, type.getLengthInBytes());
				byte[] res = val.getBytes(mfEncoding);
				if (res.length > type.getLengthInBytes()) {
					throw new IllegalArgumentException("String too long -- I bet there are some double byte characters in it!");
				}
				return res;
			}

            if (type.isNumeric()) {
                // TODO: Safety check to make a decent padding for numbers with decimals

            }
			// Quick hack to put in PIC 9(10 with no compression.
            // TODO: THIS DOES NOT HANDLE PIC S9() COMP-1
            if (type.isPIC9()){
				if (!type.isPacked()) {
					String padded = StringUtils.zeroPadLeft(text, type.getNumDigits());
					byte[] bytes = padded.getBytes(mfEncoding);
					return bytes;
	
				}else{
					byte[] bytes = CobolDatatypesUtils.convertStringToUnsignedPacked(text);
					return bytes;
				}
            } else if (type.isPICS9())	{
                if (type.isCOMP1()) {
                    _log.warn("Doing COMP-1 COmpression -- where is it? Temporary convert S9(x) COMP as unsigned packed. As it seems to work with BRIH header datatypes.");
                    //byte[] bytes = CobolDatatypesUtils.convertStringToUnsignedPacked(text);
                    byte[] bytes = CobolDatatypesUtils.convertStringToSignedPacked(text, type.getNumDigits());
					return bytes;
                }
				if (!type.isPacked() && type.isNumeric()) { // Non-Packed Signed or Unsigned Decimal with perhaps decimal places.
					// Use CobolComp3Utils class.
				}
				
				if (type.isPacked() && type.isNumeric()){
					byte[] bytes = CobolDatatypesUtils.convertStringToSignedPacked(text, type.getNumDigits());
					return bytes;
				}
			}
		} catch (Exception e) {
			throw new Exception("Trouble Converting Cobol Type " + type, e);
		}

		throw new Exception("Text to Cobol Conversion for Type " + type + " not yet implemented.");

	}

	/**
	 * Take a series of bytes and converts to text format for logging based on the CobolDataType.
	 * Text bytes are assumed to be in UTF-8 format.
	 *
	 * @param type
	 * @param value
	 * @return
	 */
	public String convertCobolToText(CobolDataType type, byte[] value) throws Exception {
		if (type.isPICX()) {
			String res = new String(value, "UTF-8");
			if (res.length() > type.getLengthInBytes()) {
				throw new IllegalArgumentException("String Data for PIC X() was larger than " + type.getLengthInBytes());
			}
			res = String.format("%-10s", res);
			_log.debug("Padded String [" + res + "]");
			return res;
		}
		if (type.isCOMP3() && type.isInteger()) {
			String val = CobolComp3Utils.unpackComp3(value);
			return val;
		}

		if (type.isPIC9()) {
			String val = new String(value, mfEncoding);
			return val;
		}

		if (type.isPICS9() && type.isBinary()) {
			return "No Transform: " + ByteUtils.bytesToHex(value);
		}
		throw new Exception("Cannot Convert Cobol Type To String: " + type + " Value " + value);
	}


	public static String convertS9BinaryToText(CobolDataType type, byte[] value) {
		return "No Yet Implemented";
	}
}
