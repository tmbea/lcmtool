/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.xmlcopybooks;
/**
 *
 * User: Steve Franks
 * Date: Oct 30, 2009
 * Source Code Version: $Id: XmlCopybook.java 161 2012-05-14 07:45:43Z e1040775 $
 */


import com.odenzo.core.dom4jutils.Dom4JUtils;
import com.odenzo.core.utils.UniqueDictionary;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;


/**
 * This encapsulates a copybook in XML format. Ideally this will allow two way conversions between this XML to copybook and copybook to this XML with no changes.
 * If this hack isn't sufficient Google cb2xml for a more full featured solution.
 * I chose to do manually for better control and because our copybooks are simple.
 * TODO: Correct parsing should be line based for comments and start of line to period for Cobol statements.
 * <p><em>Note this is tailored explicitly to OPN Copybooks from TCB for KBank project.</em></p>
 */
public class XmlCopybook {
	private final static Logger _log = LoggerFactory.getLogger(XmlCopybook.class);
	private Document nestedDoc;
	private Document unrolledDoc;
	private Pattern fieldOccursPattern;
	private String viewName;
	private int copybookLength;
	private Document doc;
	private List<String> headerComments;
	private Pattern sectionPattern;
	private Pattern fieldPattern;
	private Pattern occursRegex;
	private String officialServiceName;
	private String longOpnName;
	private String shortOpnName; // I may be assigning view name here by mistake!

	private Boolean isRequest;

	private File sourceCopybookFile;

	/**
	 * Bad design, but can't use closures and too lazy to make nested scope.
	 * This is used during parsing to conditional increment the xref for each line in copybook.
	 * e.g. Comments and Sections may not incremenet. Only matched things
	 */
	private int internalId = 1;




	public XmlCopybook(File copybookFile) throws Exception {
		this.sourceCopybookFile = copybookFile;
		XmlCopybookParser parser = new XmlCopybookParser();
		this.nestedDoc = parser.parseCopybook(copybookFile);
		extractOpnMetaDataFromDocument(this.nestedDoc);
		this.unrolledDoc = makeUnrolledFromNestedDocument((Element) nestedDoc.selectSingleNode("//CopyBook"));
	}

	private void extractOpnMetaDataFromDocument(Document nestedDoc) throws Exception {
		try {
			Element root = nestedDoc.getRootElement();
			this.shortOpnName = root.attributeValue("opnTransaction");
			this.longOpnName = root.attributeValue("opnServiceName");
			this.officialServiceName = root.attributeValue("officialServiceName");
			this.isRequest = root.attributeValue("direction").equalsIgnoreCase("Inbound");
		} catch (Exception e) {
			_log.error("Bad Nested Doc " + Dom4JUtils.prettyString(nestedDoc));
			throw new Exception("Trouble Extracting MetaData from XMLCopybook ", e);
		}
	}


	/**
	 * This performs two tasks to unroll a nested document. First duplicate fields with occurance > 1, then unroll segment and sections > 1
	 * It bases this unrolled documnet on the passed in nested document.
	 * It then updates the offsets for all fields in the unrolled document.
	 *
	 * @param srcCopybook
	 * @return
	 * @throws Exception
	 */
	public static Document makeUnrolledFromNestedDocument(Element srcCopybook) throws Exception {
		try {
			_log.info("Unrolling XML Nested Copybook");
			Element copybook = srcCopybook.createCopy();
			Document unrolledResult = DocumentHelper.createDocument(copybook);
			copybook.addAttribute("type", "Unrolled");

			List<Element> segments = copybook.selectNodes("//Segment[@occurs>1]");
			_log.info("Found " + segments.size() + " semgents with occurs greater than one on first pass.");
			while (segments.size() > 0) {
				_log.info("Found " + segments.size() + " semgents with occurs greater than one.");
				for (Element segment : segments) {
					_log.debug("Processing Segment: \n " + Dom4JUtils.prettyString(segment));
					if (segment.attributeValue("unrolled", "false").equals("true")) {
						continue;
					}
					Element record = segment.createCopy();
					int occurances = new Integer(segment.attributeValue("occurs"));
					segment.clearContent(); // Erase contents of original Segment which are in record now anyway
					record.setName("Record");
					for (int i = 0; i < occurances; i++) {
						Element nthRecord = record.createCopy();
						nthRecord.addAttribute("index", "" + i);
						segment.add(nthRecord);
					}
					segment.setName("UnrolledSegment");
				}
				segments = copybook.selectNodes("//Segment[@occurs>1]");
			}
			// Now unroll the fields with max occurances
			List<Element> fieldOccurances = copybook.selectNodes("//Field[@occurs>1]");
			_log.debug("Found " + fieldOccurances.size() + " recurring fields");
			for (Element field : fieldOccurances) {
				_log.debug("Processing Occurs Field: \n " + field.asXML());
				Element baseField = field.createCopy();

				int occurances = new Integer(field.attributeValue("occurs"));

				for (int i = 0; i < occurances; i++) {
					Element nSeg = baseField.createCopy();
					// Not sure if we really should make a unique name here or not.
					field.add(nSeg);
					String baseIndex = nSeg.attributeValue("index");
					nSeg.addAttribute("index", baseIndex);

				}
			}
			calculateOffsets(copybook);
			assignUIDForEachField(unrolledResult);
			return unrolledResult;
		} catch (Exception e) {
			throw new Exception("Trouble Unrolling Nested Document", e);
		}
	}


	public static void assignUIDForEachField(Document doc) throws Exception {
		List<Element> fields = doc.selectNodes("//Field");

		for (Element field : fields) {
			List<Element> recordXrefs = field.selectNodes("./ancestor::Record");
			String val = field.attributeValue("xref");
			if (recordXrefs.size() == 1) {
				String parentXref = recordXrefs.get(0).attributeValue("xref");
				String parentIndex = recordXrefs.get(0).attributeValue("index");
				val = parentXref + "[" + parentIndex + "]." + val;
			} else if (recordXrefs.size() == 2) {
				//blah blah more. Use a switch/case with fallthru
				String parentXref = recordXrefs.get(1).attributeValue("xref");
				String parentIndex = recordXrefs.get(1).attributeValue("index");
				val = parentXref + "[" + parentIndex + "]." + val;
				val = recordXrefs.get(0).attributeValue("xref") + "[" + recordXrefs.get(0).attributeValue("index") + "]." + val;
			}
			if (val.length() > 10) {
				throw new Exception("Field " + field.asXML() + " resulted in too long a UID " + val);
			}
			field.addAttribute("uid", val);
		}
	}


	/**
	 * Replaces just the unrolled OPN with shortened and unque field names.
	 * Currently this is in place change.
	 *
	 * @param maxFieldLength
	 * @param unrolledDoc
	 * @throws Exception
	 */
	public Document makeFieldsUnique(int maxFieldLength, Document unrolledDoc) throws Exception {
		// Note: The Unrolled KBMF will have unique names, but some are longer than 10 chars
		try {
			UniqueDictionary dict = new UniqueDictionary(10);
			List<Element> fields = unrolledDoc.selectNodes("//Field");
			for (Element e : fields) {
				String name = e.attributeValue("name").trim();
				String shortName = dict.addWord(name);
				_log.info("Copybook Converting: [" + name + "] to [" + shortName + "]");
				e.addAttribute("name", shortName);
				_log.debug("New Elem: " + e.asXML());
			}

			_log.debug("Setting Unique Unrolled to " + Dom4JUtils.prettyString(this.getUnrolledDoc()));
			return unrolledDoc;
		} catch (Exception e) {
			throw new Exception("Trouble making Fields Unique in Server " + getServiceName(), e);
		}
	}


	private static void applyAttributeToSubFieldsAndSections(Element parent, String attributeName, String attributeValue) {
		List<Element> fields = parent.selectNodes("descendant::Field");
		List<Element> sections = parent.selectNodes("descendant::Section"); // This is absolutely necessary

		for (Element e : fields) {
			e.addAttribute(attributeName, attributeValue);

		}
		for (Element e : sections) {
			e.addAttribute(attributeName, attributeValue);
		}
	}

	/**
	 * This modifies child elements of parent (the name atteibute) and the dict
	 *
	 * @param parent
	 * @param index
	 * @param dict
	 */
	private void ensureUniqueNamesForSegment(final Element parent, int index, UniqueDictionary dict) throws Exception {
		List<Element> fields = parent.selectNodes("descendant::Field");
		List<Element> sections = parent.selectNodes("descendant::Section"); // This is absolutely necessary

		for (Element e : fields) {
			String existingName = e.attributeValue("name");
			String newName = dict.addWord(existingName);
			e.addAttribute("name", newName);
		}
	}

	public int calculateTotalSize(Document unrolled) throws Exception {
		try {
			Element root = unrolled.getRootElement();
			Element copybook = (Element) root.selectSingleNode("CopyBook[@type='Unrolled']");
			List<Element> fields = copybook.selectNodes("descendant::Field");
			int size = 0;
			for (Element f : fields) {
				size += new Integer(f.attributeValue("size"));
			}
			return size;
		} catch (Exception e) {
			throw new Exception("Trouble Calculating Size of Unrolled COpybook", e);
		}
	}

	/**
	 * Adds an offset field to each concrete field in the unrolled document, in place.
	 *
	 * @param unrolledCopybook
	 * @return
	 * @throws Exception
	 */
	private static void calculateOffsets(Element unrolledCopybook) throws Exception {
		try {


			List<Element> fields = unrolledCopybook.selectNodes("descendant::Field");
			int offset = 0;
			for (Element f : fields) {
				f.addAttribute("offset", "" + offset);
				offset += new Integer(f.attributeValue("size"));
			}

		} catch (Exception e) {
			throw new Exception("Trouble Calculating Offset of Unrolled COpybook", e);
		}
	}

	public Document getNestedDoc() {
		return nestedDoc;
	}

	public Document getUnrolledDoc() {
		return unrolledDoc;
	}


	/**
	 * This should be the short copybook name, 8 chars
	 *
	 * @return
	 */
	public String getViewName() {
		return viewName;
	}

	/**
	 * KBank Service Name, e.g. AcctLstInq001
	 *
	 * @return
	 */
	public String getServiceName() {
		return officialServiceName;
	}

	public String getLongOpnName() {
		return longOpnName;
	}

	public Boolean isRequest() {
		return isRequest;
	}

	public Boolean isResponse() {
		if (isRequest == null) {
			return null;
		}
		return !isRequest();
	}

	/**
	 * Get all the fields that are present in the physical copybook. The leaf nodes in unrolled document basically.
	 *
	 * @throws Exception
	 */
	public List<Element> extractConcreteFields() throws Exception {
		try {
			// Implemented by Finding all Field elements with occurs=1, I think the occurs is optional actually. (From unrolled)
			List<Element> fields = this.unrolledDoc.getRootElement().selectNodes("//Field[@occurs=1]");
			return fields;
		} catch (Exception e) {
			throw new Exception("Trouble Extracting Concrete Fields", e);
		}
	}

	public Element getFieldByXRef(int id) throws Exception {
		try {
			List<Element> opnFields = (List<Element>) getNestedDoc().selectNodes(String.format("//Field[@xref='%d']",
			                                                                                   id));
			if (opnFields.size() != 1) {
				throw new Exception("Illegal Number of OPN Fields with name [" + id + "] found in OPN: " + opnFields.size());
			}
			return opnFields.get(0);
		} catch (Exception e) {
			throw new Exception("Trouble Getting Field XRef " + id + " from XmlCopybook.", e);
		}
	}

	public Element getFieldByName(String name) throws Exception {
		List<Element> opnFields = (List<Element>) getNestedDoc().selectNodes(String.format("//Field[@line='%s']",
		                                                                                   name));
		if (opnFields.size() != 1) {
			_log.warn("Document Searched: " + Dom4JUtils.prettyString(getNestedDoc()));
			throw new Exception("Illegal Number of OPN Fields with name [" + name + "] found in OPN:" + opnFields.size());
		}
		return opnFields.get(0);
	}

	/**
	 * This gets all the defined concrete fields, NOT the unrolled leaf nodes though.
	 * These are sourced from the nested document.
	 *
	 * @return
	 * @throws Exception
	 */
	public List<Element> getAllBodyFields() throws Exception {
		if (isRequest()) {
			List<Element> opnFields = getNestedDoc().selectNodes("//Field[@line>13]"); // All fields passed OPN Header
			_log.debug("Returning " + opnFields.size() + " elements from OPN Request Body");
			return opnFields;
		}
		List<Element> opnFields = getNestedDoc().selectNodes("//Field[@line>20]"); // All fields passed OPN Header
		_log.debug("Returning " + opnFields.size() + " elements from OPN Response Body");
		return opnFields;
	}

	/**
	 * Gets all the fields from the unrolled document, live. Not Copied.
	 *
	 * @return
	 */
	public List<Element> getAllConcreteFields() {
		List<Element> opnFields = getUnrolledDoc().selectNodes("//Field"); // All fields passed OPN Header
		return opnFields;
	}

	public void setMetaData(String kbankService, String opnShortName, String opnViewName, boolean request) {

		this.officialServiceName = kbankService;
		this.viewName = opnViewName;
		this.shortOpnName = opnShortName;
		this.isRequest = request;
	}

	public void setRequest(Boolean request) {
		isRequest = request;
	}

	public void writeXML(File destDir) throws Exception {
		String suffix = isRequest ? "RQ" : "RS";
		destDir.mkdirs();
		File nestedFile = new File(destDir, "OPN-" + getServiceName() + "_" + suffix + ".xml");
		File unrolledFile = new File(destDir, "OPN-" + getServiceName() + "_Unrolled_" + suffix + ".xml");
		Dom4JUtils.writeToFile(getNestedDoc(), nestedFile);
		Dom4JUtils.writeToFile(getUnrolledDoc(), unrolledFile);
	}

	public void unrollDocumentAgain() throws Exception {
		this.unrolledDoc = makeUnrolledFromNestedDocument((Element) nestedDoc.selectSingleNode("//CopyBook"));
	}
}
