/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.cobol.xmlcopybooks;

/**
 *
 * User: Steve Franks
 * Date: Oct 30, 2009
 * Source Code Version: $Id: XmlCopybookParser.java 161 2012-05-14 07:45:43Z e1040775 $
 */


import com.odenzo.core.cobol.CobolDataType;
import com.odenzo.core.dom4jutils.Dom4JUtils;
import com.odenzo.core.io.RegexFileFilter;
import com.odenzo.core.utils.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This encapsulates a copybook in XML format. Ideally this will allow two way conversions between this XML to copybook and copybook to this XML with no changes.
 * If this hack isn't sufficient Google cb2xml for a more full featured solution.
 * I chose to do manually for better control and because our copybooks are simple.
 * We should go straight to nested XML Copybook and forget the flat format altogether.
 * TODO: Correct parsing should be line based for comments and start of line to period for Cobol statements.
 * <p><em>Note this is not capable of handling generic copybooks.</em></p>
 */
public class XmlCopybookParser {
    private final static Logger _log = LoggerFactory.getLogger(XmlCopybookParser.class);

    /**
     * Note: This is duplicated in a few places. Not sure where best. See CopybookMetaDataLibrary comments.
     */
    //private final static String copybookFilePattern = "^OPN-(.*?)-(.*?)-(.*?)_(R.).txt";
    private final static String copybookFilePattern = "(.*).cpy";

    private Pattern fieldOccursPattern;
    private String viewName;

    private Document doc;

    private Pattern sectionPattern;
    private Pattern fieldPattern;
    private Pattern occursRegex;
    private String serviceName;
    private String longOpnName;
    private String shortOpnName;

    private Boolean isRequest;
    private int documentedCopybookLength;
    private File sourceCopybookFile;

    public static void main(String[] args) {
        try {
            String inputDirName = "/Volumes/Copybooks";
            File baseInpDir = new File(inputDirName);
            File[] cbFiles = baseInpDir.listFiles(new RegexFileFilter(copybookFilePattern));
            for (File copybookFile : cbFiles) {
                XmlCopybookParser parser = new XmlCopybookParser();
                Document xmlCopybook = parser.parseCopybook(copybookFile);
                _log.info("XML: " + Dom4JUtils.prettyString(xmlCopybook));

            }
        } catch (Exception e) {
            _log.error("Error:" , e);
        }
    }

    /**
     * Bad design, but can't use closures and too lazy to make nested scope.
     * This is used during parsing to conditional increment the xref for each line in copybook.
     * e.g. Comments and Sections may not incremenet. Only matched things
     */
    private int internalId = 1;


    public XmlCopybookParser() throws Exception {
        initRegularExpressions();
    }

    /**
     * Parses a copybook. Note we generally have 2 kinds of copybooks
     *
     * @param copyBook Assumed UTF-8 encoded copybook, will be ASCII in practice.
     * @return
     */
    public Document parseCopybook(File copyBook) throws Exception {
        try {
            _log.debug("Parsing File: " + copyBook + " Size: " + copyBook.length());
            this.sourceCopybookFile = copyBook;

            FileInputStream fis = new FileInputStream(copyBook);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            LineNumberReader lnr = new LineNumberReader(br);

            // First read into a list of strings for flexibility.
            List<String> copybookLines = new ArrayList<String>(200);
            String line = "first";
            Element root = DocumentHelper.createElement("CopyBookMetaData");
            this.doc = DocumentHelper.createDocument(root);
            //	addOpnFileNameBasedMetaData(doc, copyBook);
            Element currentParent = root.addElement("CopyBook");
            int lineNumber = lnr.getLineNumber();
            while ((line = getNextCobolLine(lnr)) != null) {
                _log.debug("Parsing Line: " + lineNumber);
                try {
                    _log.debug("Parsing Line into Parent " + currentParent.asXML() + " == " + line);
                    line = line.trim();
                    if (line.contains("REDEFINES")) {
                        // REDEFINES in Standard Header skip this line and the next
                        lnr.readLine();
                        continue;
                    }
                    if (line.length() > 0) {
                        Element newElement = parseLine(line);
                        newElement.addAttribute("line", String.valueOf(lnr.getLineNumber()));
                        String name = newElement.attributeValue("name");
                        String elemType = newElement.getName();
                        if (!elemType.equals("Comment")) {
                            newElement.addAttribute("xref", String.valueOf(internalId++));
                        }

                        if (elemType.equals("Field")) {     // Do, fields obviously can end segments/sections too
                            int currLevel = Integer.parseInt(currentParent.attributeValue("level", "0"));
                            int elemLevel = Integer.parseInt(newElement.attributeValue("level"));
                            if (elemLevel > currLevel) {
                                currentParent.add(newElement); // Its part of current section/segment
                            } else {
                                // Since its a field it is never the new parent, but need to find the new parent.
                                // Can pop-up arbitrary number of levels unfortunately.
                                // In fact the tricky/normal case is that the Field is the same level as current parent
                                boolean needToPop = true;
                                while (needToPop) {
                                    currentParent = currentParent.getParent();
                                    int upLevel = Integer.parseInt(currentParent.attributeValue("level"));
                                    needToPop = elemLevel <= upLevel;
                                }
                                currentParent.add(newElement);
                            }
                        } else if (elemType.equals("Section") || elemType.equals("Segment")) {
                            // Check to see if push or pop 1 or pop 2 elements down/up
                            int currLevel = Integer.parseInt(currentParent.attributeValue("level", "0"));
                            int elemLevel = Integer.parseInt(newElement.attributeValue("level"));
                            if (elemLevel > currLevel) {
                                _log.debug("Elem " + newElement.asXML() + " greater than current level");
                                currentParent.add(newElement);
                                currentParent = newElement;
                            } else if (elemLevel == currLevel) { // Adding a sibling Section to current Section
                                _log.debug("Elem " + newElement.asXML() + " equal current level - making sibling");
                                currentParent.getParent().add(newElement);
                                currentParent = newElement;
                            } else { // elemLevel < currLevel, pop-up one or more
                                boolean needToPop = true;
                                while (needToPop) {
                                    currentParent = currentParent.getParent();
                                    int upLevel = Integer.parseInt(currentParent.attributeValue("level"));
                                    needToPop = elemLevel <= upLevel;
                                }
                                currentParent.add(newElement);
                                currentParent = newElement;
                            }
                        }
                    }
                } catch (Throwable e) {
                    throw new Exception("Trouble Parsing Line " + lnr.getLineNumber() + "[" + line + "]", e);
                }
            }
            _log.debug("Base XML: " + Dom4JUtils.prettyString(this.doc));
            extractMetaData(doc);
            //_log.debug("Nested Doc: " + Dom4JUtils.prettyString(doc));

            return doc;
        } catch (Exception e) {
            throw new Exception("Trouble Reading Copybook File " + copyBook, e);
        }
    }

    private String getNextCobolLine(LineNumberReader lnr) throws Exception {
        try {
            String val = lnr.readLine();
            _log.debug("Raw Line: [" + val + "]");
            if (val == null) {
                return null;
            }
            val = val.trim();
            _log.debug("Trimmed Line [" + val + "]");
            if (val.length() < 1) {
                _log.debug("Blank Line");
                return "";
            }
            if (val.startsWith("*")) {
                return val;
            }
            // If its blank or a comment then just read the one line, else read until last char is .
            // so far a cobol line only streches across two lines.
            if (val.endsWith(".")) {
                return val;
            }

            String l2 = lnr.readLine().trim(); // Better not be null or invalid copybook I guess.
            if (l2.endsWith(".")) {
                return val + " " + l2;
            }

            throw new Exception("Multiline Cobol Not Dealt With: [" + val + "]");
        } catch (Throwable e) {
            throw new Exception("Trouble Reading Next Cobol Line @ " + lnr.getLineNumber(), e);
        }
    }


    /**
     * Extract some information about the copybook from the copybook file name.
     * This is organized around OPN based file name standards, not KBMF/WSDL Copybook standards.
     *
     * @param doc
     * @param copyBook
     * @deprecated Copybooks should now be named based on their short view name, e.g. ZKADUT1.cpy
     */
    private void addOpnFileNameBasedMetaData(Document doc, File copyBook) {

        // See if the filename matches the regular expression for OPN Copybook, if so add metadata.
        _log.debug("Adding FileName Based MetaData for " + copyBook + " using pattern " + copybookFilePattern);
        Pattern fileNamePattern = Pattern.compile(copybookFilePattern);
        String fileName = copyBook.getName();
        Matcher m = fileNamePattern.matcher(fileName);
        if (m.matches()) {
            _log.info("OPN Copybook: " + fileName);
            this.serviceName = m.group(3);
            this.longOpnName = m.group(1);
            this.shortOpnName = m.group(2);
            isRequest = m.group(4).equalsIgnoreCase("RQ");
            Element root = doc.getRootElement();
            root.addAttribute("serviceName", serviceName);
            root.addAttribute("txnServiceName", longOpnName);
            root.addAttribute("txnName", shortOpnName);  // Or is this the view name?
            root.addAttribute("type", isRequest ? "request" : "response");
            _log.debug("Element: " + root.asXML());
        }
    }


    /**
     * Parses a line and adds it to the Element supplied. Really want a seperate Branch to add it to.
     *
     * @param cbLine
     */
    private Element parseLine(String cbLine) throws Exception {
        try {
            Element el = null;

            Matcher m;
            if (cbLine.startsWith("*")) { // Its a comment
                el = DocumentHelper.createElement("Comment");
                el.addCDATA(cbLine);
                // Try and extract the short copybook name and document size in bytes
                Pattern commentInfoPattern = Pattern.compile(".*?VIEW\\s*(\\w*)\\s*Size:\\s*(\\d+)");
                m = commentInfoPattern.matcher(cbLine);
                if (m.find()) {
                    viewName = m.group(1);
                    documentedCopybookLength = Integer.parseInt(m.group(2));
                    _log.info("Found View " + viewName + " of documented size " + documentedCopybookLength);
                } else {
                    _log.warn("Could not extract View Info from Comment from " + this.sourceCopybookFile);
                    _log.warn("Raw Text [" + cbLine + "]");
                }
                return el;
            }

            m = sectionPattern.matcher(cbLine);
            if (m.matches()) {
                _log.debug("Section Matched");
                el = DocumentHelper.createElement("Section");
                el.addAttribute("level", m.group(1));
                el.addAttribute("name", m.group(2));

                return el;

            }
            m = occursRegex.matcher(cbLine);
            if (m.matches()) {
                _log.debug("Occurs Matched");
                el = DocumentHelper.createElement("Segment");
                el.addAttribute("level", m.group(1));
                el.addAttribute("name", m.group(2));

                if (Integer.parseInt(m.group(3)) > 1) {
                    el.addAttribute("occurs", m.group(3));
                }


                return el;
            }

            m = fieldOccursPattern.matcher(cbLine);
            if (m.matches()) {
                el = DocumentHelper.createElement("Field");
                el.addAttribute("level", m.group(1));
                el.addAttribute("name", m.group(2));
                String datatype = m.group(3).trim();
                CobolDataType cdatatype = new CobolDataType(datatype);
                el.addAttribute("type", cdatatype.getNormalized());
                el.addAttribute("size", String.valueOf(cdatatype.getLengthInBytes()));

                if (Integer.parseInt(m.group(4)) > 1) {
                    el.addAttribute("occurs", m.group(4));
                }

                return el;
            }

            m = fieldPattern.matcher(cbLine); // Most common line but current regex means must be last
            if (m.matches()) {
                _log.debug("Field Matched");
                el = DocumentHelper.createElement("Field");
                el.addAttribute("level", m.group(1));
                el.addAttribute("name", m.group(2));
                String datatype = m.group(3).trim();
                CobolDataType cdatatype = new CobolDataType(datatype);
                el.addAttribute("type", cdatatype.getNormalized());
                el.addAttribute("size", String.valueOf(cdatatype.getLengthInBytes()));
                //	el.addAttribute("occurs", "1"); // Careful about this. Want to remove no-ops, so this should go.

                return el;
            }
        } catch (Exception e) {
            throw new Exception("Trouble Parsing Line [" + cbLine + "]", e);
        }

        throw new Exception("Copybook Line [" + cbLine + "] could not be parsed");

    }

    /**
     * Set of regular expressions we are expexting every cobol copybook line to conform to.
     */
    private void initRegularExpressions() throws Exception {
        try {
            String fieldOccursRegex = "(\\d+)\\s*(\\S+?)\\s(.*?)OCCURS\\s*(\\d+)\\s*TIMES\\.$"; // Sequence, name, untrimmed cobol type OCCURS dd TIMES.
            String sectionRegex = "(\\d+)\\s*(\\S+?)\\.$";  // Sequence and name
            String occursRegex = "(\\d+)\\s*(\\S+?)\\s+occurs\\s+(\\d+).*";  // Sequence ,name, # occurances
            String fieldRegex = "(\\d+)\\s*(\\S+?)\\s(.*?)\\.$"; // Sequence, name, untrimmed cobol type

            this.sectionPattern = Pattern.compile(sectionRegex);
            this.occursRegex = Pattern.compile(occursRegex, Pattern.CASE_INSENSITIVE);
            this.fieldPattern = Pattern.compile(fieldRegex);
            this.fieldOccursPattern = Pattern.compile(fieldOccursRegex, Pattern.CASE_INSENSITIVE);
        } catch (Exception e) {
            throw new Exception("Trouble Initializing Copybook Regular Expressions", e);
        }

    }

    /**
     * This will get the name of the OPN I think, likst AR-FSTM-LST-OPN-I  , we remove the last -I or -O
     *
     * @param doc
     * @throws Exception
     */
    private void extractMetaData(Document doc) throws Exception {
        try {
            _log.debug("Extracting MetaData");
            Element root = doc.getRootElement();
            String viewNamePath = "CopyBook/Section[@level='01']";
            // The name attribute of top OPN section is <<opn-name>>-O or -I
            Element topSection = (Element) doc.getRootElement().selectSingleNode(viewNamePath);
            String longViewName = topSection.attributeValue("name");
            _log.debug("Long View Name: " + longViewName);
            if (longViewName.matches("(.*?)-I(-V)?$")) {
                root.addAttribute("direction", "Inbound");
            } else if (longViewName.matches("(.*?)-O(-V)?$")) {
                root.addAttribute("direction", "Outbound");
            } else {
                throw new Exception("View Name not in Standard OPN Format (x-I or x-O) [" + longViewName + "]");
            }
            String opnServiceName = StringUtils.removeLastNChars(longViewName, 2);
            root.addAttribute("opnServiceName", opnServiceName);

            root.addAttribute("viewName", viewName);
            root.addAttribute("documentedViewSize", String.valueOf(documentedCopybookLength));

        } catch (Exception e) {
            throw new Exception("Trouble Extracting MetaData", e);
        }


    }
}
