/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.datastructures;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Set;

/**
 * This is a forwardMap that enforces a one-to-one relationship between keys and values.
 * Each value must be unique, but there may be multiple NULL values.
 * Each key must be unique and no null values are allowed.
 * TODO: Quick and dirty for low performance, rewrite someday.
 *
 * @author stevef
 * @version $Id: MapOneToOne.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class MapOneToOne<T, V> {
	private static Logger _log = LoggerFactory.getLogger(MapOneToOne.class);

	HashMap<T, V> forwardMap = new HashMap<T, V>();

	HashMap<V, T> reverseMap = new HashMap<V, T>();

	public void put(T key, V val) {
		if (forwardMap.containsKey(key)) {
			throw new IllegalArgumentException("Key " + key + " already exists  with value " + forwardMap.get(key));
		}
		if ((val != null) && reverseMap.containsKey(val)) {
			throw new IllegalArgumentException("Value " + val + " already exists with value " + reverseMap.get(val));
		}

		forwardMap.put(key, val);
		if (val != null) {
			reverseMap.put(val, key);
		}
	}

	public Set<T> getKeySet() {
		return forwardMap.keySet();
	}

	public V getValueForKey(T key) {
		return forwardMap.get(key);
	}

	public T getKeyForValue(V val) {
		return reverseMap.get(val);
	}
}
