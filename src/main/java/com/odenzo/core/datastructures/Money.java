/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.datastructures;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * A non-mutable Money class for general Java use. To do, change to containment (back again!)
 * This gives me fits. Don't recommend using this.
 *
 * @version $Id: Money.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class Money extends BigDecimal {

	/**
	 *
	 */
	private static final long serialVersionUID = -5118340722315665494L;

	final private static MathContext moneyMathContext = MathContext.DECIMAL64;

	public static final Money ZERO = new Money("0.00");

	protected static Money _zero = new Money("0.00");

	protected static NumberFormat _format = null;

	static {
		_format = new DecimalFormat("###0.00");
		_format.setMaximumFractionDigits(2);
		_format.setMinimumFractionDigits(2);

	}

	/**
	 * Hmm, too bad constructors do not return self pointers
	 */
	public Money() {
		super(0.00);
	}

	public Money(BigDecimal val) {
		super(val.doubleValue(), Money.moneyMathContext);
	}

	public Money(String val) {
		super(val);
	}

	public Money(double val) {
		super(val);
	}

	/**
	 * Geez, how can we do this nicely without reimplementing or extending by contains?
	 *
	 * @param val
	 * @return
	 */
	public Money add(Money val) {
		Money res = new Money(super.add(val));
		return (res);
	}

	public Money subtract(Money val) {
		Money res = new Money(super.subtract(val));
		return (res);
	}

	/**
	 * Calculate a percentage of this money.
	 *
	 * @param percent Value between 0 and 100
	 * @return this * percent / 100
	 */
	public Money percent(double percent) {
		return new Money(this.multiply(new BigDecimal(percent / 100)));
	}

	/**
	 * Formats for XML use, standard DDD.CC
	 *
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public static String formatForXML(BigDecimal val) throws Exception {
		try {
			return (_format.format(val));
		} catch (Exception e) {
			throw new Exception("Trouble Formating Money to XML: " + val, e);
		}
	}

	/**
	 * This returns the Money formatted accord to the local currency formatting preferences. e.g. $4,500.00 or
	 * Yen5676
	 *
	 * @return
	 */
	public String format() {
		return _format.format(this);

	}

	/**
	 * This returns an unformatted number of the form xxxxx.yy for the money
	 */
	public String toString() {
		// BigDecimal.doubleValue calls toString() again.... rats.
		BigDecimal proxy = super.add(BigDecimal.ZERO);
		return _format.format(proxy);

	}

	/**
	 * Return true is greatar than zero
	 *
	 * @return
	 */
	public boolean isPositive() {
		return this.doubleValue() > 0;
	}

}
