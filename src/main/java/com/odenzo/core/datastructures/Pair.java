/*
 * Copyright (c) 2009. adenzo software
 */

/**
 * $License$
 */
package com.odenzo.core.datastructures;

/**
 * Simple Pair (2-tuple)
 * @author stevef
 * @version $Id: Pair.java 254 2012-05-23 07:14:28Z e1040775 $
 */
public class Pair<T, V> {

	private T a;
	private V b;

	public Pair() {


	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        if (a != null ? !a.equals(pair.a) : pair.a != null) return false;
        if (b != null ? !b.equals(pair.b) : pair.b != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        return result;
    }

    public Pair(T a, V b) {
		this.a = a;
		this.b = b;
	}

	public T getFirst() {
		return a;
	}

	public V getSecond() {
		return b;
	}

    public String toString() {
		return "Pair (" + getFirst() + "," + getSecond() + ") #" + Integer.toHexString(hashCode());
	}
}
