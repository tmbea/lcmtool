/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.datastructures;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Vector;

/**
 * This datastructure holds n most recent items. Adding new items may cause the oldest item to be dropped off the list.
 * Basiclly a cyclic array. Created on Jul 24, 2003
 *
 * TODO: Cyclic List is in Java yet? Also make <T> for get and set etc.
 * @author stevef
 * @version $Id: RollingList.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class RollingList {
	final private static Logger _log = LoggerFactory.getLogger(RollingList.class);

	private Object[] _data = null;

	private int _currentIndex = 0;

	public RollingList(int maxListLength) {
		_data = new Object[maxListLength];
		_currentIndex = 0;
	}

	public synchronized void appendObject(Object elem) {
		_currentIndex = (_currentIndex + 1) % _data.length;
		_data[_currentIndex] = elem;
	}

	synchronized public Object getCurrent() {
		return (_data[_currentIndex]);
	}

	/**
	 * This list may contain NULL values? It should not really, not sure why it does.
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	synchronized public List getListOrderedMostRecent() {
		List res = new Vector(_data.length);
		int indx = _currentIndex;
		for (int i = 0; i < _data.length; i++) {
			if (_data[indx] != null) {
				res.add(_data[indx]);
			}

			if (indx == 0) {
				indx = _data.length;
			}
			indx--;
		}
		return (res);
	}
}
