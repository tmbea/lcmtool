/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 * $License$
 */
package com.odenzo.core.datastructures;

/**
 * A simple triplet datastructure.
 * @author stevef
 * @version $Id: Triplet.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class Triplet<T, U, V> {

    final T a;

    final U b;

    final V c;

    public Triplet(T a, U b, V c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Object get(int i) {
        switch (i) {
            case 0:
                return getFirst();
            case 1:
                return getSecond();
            case 2:
                return getThird();
            default:
                return null;

        }
    }

    public T getFirst() {
        return a;
    }

    public U getSecond() {
        return b;
    }

    public V getThird() {
        return c;
    }

    public boolean equals(Object other) {
        if ((other == null) || (!(other instanceof Triplet))) {
            return false;
        }
        Triplet otherPair = (Triplet) other;
        return (otherPair.getFirst().equals(this.getFirst())) && (otherPair.getSecond().equals(this.getSecond()))
                && (otherPair.getThird().equals(this.getThird()));
    }

    @Override
    /**
     * We want the hashcode to be the same if the first and second objects are the same (by object or value?)
     */
    public int hashCode() {
        return getFirst().hashCode() * 31 + getSecond().hashCode() + getThird().hashCode() * 53;
    }

    public String toString() {
        return new StringBuilder().append("3-Tuple (")
                    .append(getFirst()).append(",").append(getSecond()).append(",").append(getThird())
                    .append(") #").append(Integer.toHexString(hashCode())).toString();
    }
}
