/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 *
 */
package com.odenzo.core.datetime;

/**
 * This class encapsulates a calendar week. Note that several definitions for what is the first calendar week in a year.
 * This uses the method defined in Day
 *
 * @author stevef
 * @version $Id: CalendarWeek.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class CalendarWeek {

	private final Day startOfWeek;

	public CalendarWeek(int year, int cw) {
		this.startOfWeek = Day.startOfWeek(year, cw);
	}

	public CalendarWeek(Day dayInWeek) {
		this.startOfWeek = new Day(dayInWeek);
		this.startOfWeek.setToStartOfThisWeek();
	}

	public int getYear() {
		return startOfWeek.getYear();
	}

	public int getCalendarWeek() {
		return startOfWeek.getWeek();
	}
}
