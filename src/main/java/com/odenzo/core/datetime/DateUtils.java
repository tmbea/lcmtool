/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 * $License$
 */
package com.odenzo.core.datetime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Some general utilities for dealing with data and time.
 * That suck. SimpleDateFormat and Calendars are not multithread safe and
 * we are not suppose to synchronize in EJB environment.
 *
 * @author stevef
 * @version $Id: DateUtils.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class DateUtils {
	private final static Logger _log = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
		// Private constructor as this is a utitlity class that should not be instanciated.
	}

	/**
	 * Date formats are not safe for multithreaded use .... still?
     * @return ISO Date format yyyy-MM-dd
     */

	static SimpleDateFormat isoDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}

	static SimpleDateFormat dayOfWeekFormat() {
		return new SimpleDateFormat("EEEE");
	}

	static SimpleDateFormat w3cDateTimeFormat() {
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
	}

	static SimpleDateFormat isoDateTimeFormat() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z ");
	}


	/**
	 * @param date
	 * @return Date in YYYY-MM-DD format. If date null return 0000-00-00
	 */
	public static String formatIsoDate(final Date date) {
		if (date == null) {
			return "0000-00-00";
		}
		return isoDateFormat().format(date);
	}

	public static String formatIsoDate(final Calendar cal) {
		return isoDateFormat().format(cal.getTime());
	}

	public static String formatIsoTimestamp(final Calendar cal) {
		return isoDateTimeFormat().format(cal.getTime());
	}

	/**
	 * Sets the time porting of a Calendar to zero, essentially midnight at the start of the day. This means fields
	 * Hour,  MINUTE, SECOND, MILLISECOND.
	 *
	 * @param end
	 */
	public static void zeroTime(final Calendar end) {
		end.set(Calendar.HOUR_OF_DAY, 0);
		end.set(Calendar.MINUTE, 0);
		end.set(Calendar.SECOND, 0);
		end.set(Calendar.MILLISECOND, 0);
	}

    /**
     * Check to see if the Calendar params are on the same day, regardless of what time of the day, and I think what
     * time zone. E.g. Jan 2 1:00AM in New Zealand will not be the same day as Jan 1 11PM in New York (even that at same UTC time).
     * (well you know what I mean).
     * @param a
     * @param b
     * @return
     */
	public static boolean areSameDay(final Calendar a, final Calendar b) {
		boolean res = ((a.get(Calendar.DAY_OF_YEAR) == b.get(Calendar.DAY_OF_YEAR))
				&& (a.get(Calendar.YEAR) == b.get(Calendar.YEAR))
		);
		return res;
	}

	/**
	 * Return the weekday name for the date.
	 *
	 * @param cal
	 * @return
	 */
	public static String formatDayOfWeek(final Calendar cal) {
		return dayOfWeekFormat().format(cal.getTime());
	}

    /**
     * Gets W3C Timestamp in current timezone.
     * @return
     */
	public static String currentW3CTimestamp() {
		Calendar cal = Calendar.getInstance();
		final String base = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").format(cal.getTime());
        // HUH? How does this be consistent with currentW3CTimestampUTC and formatW3CTimestamp.
        // TODO: Should confirm W3C format and re-use formatW3CTimestamp!!
		String ans = base.substring(0, base.length() - 2) + ":" + base.substring(base.length() - 2);
		return ans;
	}

    /**
     * W3C Timestamp in UTC timezone.
     * @return
     */
	public static String currentW3CTimestampUTC() {
		final Calendar utcTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.ssss'Z'").format(utcTime.getTime());
	}

    /**
     *
     * @param cal
     * @return
     */
	public static String formatW3CTimestamp(final Calendar cal) {
		if (cal == null) {
			return "[No Value]";
		}
		return w3cDateTimeFormat().format(cal.getTime());
	}

	public static String formatW3CTimestamp(final Date cal) {
		return w3cDateTimeFormat().format(cal);
	}

	public static long calculateDeltaTimeInMicroseconds(final Date start, final Date end) {
		return start.getTime() - end.getTime();
	}

	/**
	 * Parses a string in format yyyy-mm-dd (ISO xxx format)
	 *
	 * @param fromDate
	 * @return
	 * @throws Exception If trouble parsing.
	 */
	public static Calendar parseISODate(final String fromDate) throws Exception {
		Calendar cal = Calendar.getInstance();


		try {
			Date time = isoDateFormat().parse(fromDate);
			cal.setTime(time);
			return cal;
		} catch (ParseException e) {
			throw new Exception("Could not parse ISO Date yyyy-MM-dd from " + fromDate, e);
		}


	}



	public static TimeZone getTokyoTZ() {
		return TimeZone.getTimeZone("Asia/Tokyo");
	}

	public static Calendar getTokyoTime() {
		return Calendar.getInstance(getTokyoTZ());
	}

	/**
	 * Tries to parse the date from a String in any format. like 2009-01-01 or 31 Dec 2010.
	 * Needs to be better documented, but really it is Fuzzy/Lenient and could be wrong as some formats ambiguous.
     * Currently this only handles basic ISO Date.
	 * <ul>
	 * <li>YYYY-MM-DD</li>
	 * <li>DD-MM-YYYY</li>
	 * </ul>
	 *
	 * @param val
	 * @return Parsed date or null if cannot parse.
	 */
	public static Calendar parseFreeFormDate(String val) {
		try {
			return parseISODate(val);
		} catch (Exception e) {
			return null;
		}
	}
}
