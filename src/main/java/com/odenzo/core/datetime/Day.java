/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 * $License$
 */
package com.odenzo.core.datetime;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This is a restriction of Gregorian Calendar that is used just to store day
 * resolution values. The time will always be set to zero (midnight) of the day.
 *
 * @author stevef
 * @version $Id: Day.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class Day extends GregorianCalendar {
	private static final Logger _log = LoggerFactory.getLogger(Day.class);

	private final static int firstDayOfWeek = Calendar.MONDAY;
	private static Map<Integer, Integer> dayIndexTable = null;

	static {
		dayIndexTable = new HashMap<Integer, Integer>();
		dayIndexTable.put(Calendar.MONDAY, 0);
		dayIndexTable.put(Calendar.TUESDAY, 1);
		dayIndexTable.put(Calendar.WEDNESDAY, 2);
		dayIndexTable.put(Calendar.THURSDAY, 3);
		dayIndexTable.put(Calendar.FRIDAY, 4);
		dayIndexTable.put(Calendar.SATURDAY, 5);
		dayIndexTable.put(Calendar.SUNDAY, 6);
	}

	public static Integer calDayToIndex(Integer calendar_day) {
		return dayIndexTable.get(calendar_day);
	}

	private final static DateFormat toStringFormat = new SimpleDateFormat("dd-MM-yyyy");

	/**
	 * Returns the current day.
	 *
	 * @return
	 */
	public static Day getInstance() {
		return new Day();
	}

	/**
	 * @return The first day of the current week.
	 */
	public static Day getStartOfThisWeek() {
		Day res = new Day();
		res.set(Calendar.DAY_OF_WEEK, Day.firstDayOfWeek);
		return res;
	}

	public static Day getEndOfThisWeek() {
		Day res = getStartOfThisWeek();
		res.add(WEEK_OF_YEAR, 1);
		return res;
	}

	public static Day getTomorrow() {
		Day res = new Day();
		res.add(Calendar.DAY_OF_YEAR, 1);
		return res;
	}

	/**
	 * Returns the first day of week in the week of the year specified
	 * by YYYY_WW, eg. 2006_03  is third week in 2006.
	 *
	 * @param date
	 * @return
	 * @throws
	 */
	public static Day parseYearAndWeek(String date) throws IllegalArgumentException {
		if ((date == null) || (date.length() < 1)) {
			return null;
		}

		int year;
		int calWeek;

		String[] y_cw = date.split("_");
		year = Integer.parseInt(y_cw[0]);
		calWeek = Integer.parseInt(y_cw[1]);
		Day startOfWeek = Day.startOfWeek(year, calWeek);
		_log.debug("Target Start of Week for Year " + year + " week " + calWeek + " is: " + startOfWeek);
		return startOfWeek;
	}


	/**
	 * @param year    Year, like 2005
	 * @param calWeek Calendar Week of Year, starts with 1 (?)
	 * @return
	 */
	public static Day startOfWeek(int year, int calWeek) {
		Day res = new Day();
		res.set(Calendar.YEAR, year);
		res.set(Calendar.WEEK_OF_YEAR, calWeek);
		res.set(Calendar.DAY_OF_WEEK, Day.firstDayOfWeek);
		return (res);
	}

	public Day(int year, int month, int day) {
		this();
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month);
		this.set(Calendar.DAY_OF_MONTH, day);

	}

	/**
	 * Makes a new day.
	 *
	 * @param year
	 * @param dayOfYear
	 */
	public Day(int year, int dayOfYear) {
		this();
		this.set(Calendar.YEAR, year);
		this.set(Calendar.DAY_OF_YEAR, dayOfYear);
	}


	/**
	 * Default constructor, all other constructors should call this.
	 */
	public Day() {
		super();
		super.setMinimalDaysInFirstWeek(3);
		super.setFirstDayOfWeek(firstDayOfWeek);
		this.clearTime();
	}

	public Day(TimeZone tz) {
		super(tz);
		super.setMinimalDaysInFirstWeek(3);
		super.setFirstDayOfWeek(firstDayOfWeek);
		this.clearTime();
	}

	public Day(Day day) {
		this();
		setTime(day.getTime());
	}

	public Day(Calendar c) {
		this();
		setTime(c.getTime());
		this.clearTime();
	}

	public Day(Date d) {
		this();
		super.setTime(d);
		this.clearTime();
	}


	/**
	 * Returns the day of the week of the entry as index, where 0 = monday, 6 = Sunday
	 * This is not the same as calling Calendar.get(Calendar.DAY_OF_WEEK
	 *
	 * @return
	 */
	public Integer getDayIndex() {
		return Day.dayIndexTable.get(this.get(Calendar.DAY_OF_WEEK));
	}


	/**
	 * CHanges this day to be the first day of the week in which the current day is in.
	 * First day of week is based on the current locale, typically Sunday or Monday
	 *
	 * @return The same instance the method was applied to for convencience
	 */
	public Day setToStartOfThisWeek() {
		this.set(Calendar.DAY_OF_WEEK, this.getFirstDayOfWeek());
		return this;
	}

	/**
	 * Sets this day to the first day of the month its in.
	 *
	 * @return The same instance the method was applied to for convencience
	 */
	public Day setToStartOfMonth() {
		this.set(Calendar.DAY_OF_MONTH, 1);
		return this;
	}

	/**
	 * Makes a new Day object corresponding to the first day of the week
	 * that this day is in.
	 *
	 * @return
	 */
	final public Day getStartingDayOfWeek() {
		return Day.startOfWeek(this.getYear(), this.getWeek());

	}

	/**
	 * Clears the time values of this day, setting hour, minute, second and millisecond to zero.
	 */
	public void clearTime() {
		this.set(Calendar.HOUR_OF_DAY, 0);
		this.set(Calendar.MINUTE, 0);
		this.set(Calendar.SECOND, 0);
		this.set(Calendar.MILLISECOND, 0);
	}

	public String toString() {
		return toStringFormat.format(getTime());
	}

	/**
	 * This does not modify the current Day, instead it calculates the next week and returns as a new day.
	 *
	 * @return
	 */
	public Day getNextWeek() {
		Day res = new Day(this);
		res.add(Calendar.DAY_OF_YEAR, 7); // Assume always 7 days in a week :)
		return res;
	}

	/**
	 * This does not modify the current Day, instead it calculates the next week and returns as a new day.
	 *
	 * @return
	 */
	public Day getPreviousWeek() {
		Day res = new Day(this);
		res.add(Calendar.DAY_OF_YEAR, -7); // Assume always 7 days in a week :)
		return res;
	}

	/**
	 * For access from JSP. This follows our defination of first week in year, not the default one.
	 *
	 * @return The Calendar week of the year this day is in.
	 */
	public int getWeek() {
		return this.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * For access from JSP.
	 *
	 * @return
	 */
	public int getYear() {
		return this.get(Calendar.YEAR);
	}

	/**
	 * A nice clone to be used from JSP
	 *
	 * @return
	 */
	public Day getCopy() {
		return new Day(this);
	}

	/**
	 * Returns the full text of the day of week for this Day.
	 * Convenience routine.
	 *
	 * @return
	 */
	public String getDayName() {
		return DateUtils.formatDayOfWeek(this);
	}

//	public int hashCode() {
//		return
//	}
}
