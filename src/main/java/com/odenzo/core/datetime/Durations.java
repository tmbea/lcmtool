/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 * $License$
 */
package com.odenzo.core.datetime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Some utilities to calculate durations between Calendar objects.
 * TODO: TestNG Test cases badly needed for this one!
 * @author stevef
 * @version $Id: Durations.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class Durations {
	private static Logger _log = LoggerFactory.getLogger(Durations.class);

	public static void main(String[] args) {
		// JIT/hotspot warmup:
		for (int r = 0; r < 3000; ++r) {
			System.nanoTime();
		}
		long time = System.nanoTime(), time_prev = time;
		for (int i = 0; i < 5; ++i) {
			// Busy wait until system time changes:
			while (time == time_prev) {
				time = System.nanoTime();
			}
			System.out.println("delta = " + (time - time_prev) + " ms");
			time_prev = time;
		}
		System.out.println("Val: " + format(12345566));
	}

	/**
	 * Returns the number of minutes between start and end. Negative values if end is before start.
	 *
	 * @param start
	 * @param end
	 * @return   Different in minutes between start and end.
	 */
	static public long minute(Calendar start, Calendar end) {
		long startMs = start.getTimeInMillis();
		long endMs = start.getTimeInMillis();

		long durationMs = endMs - startMs;
		if (durationMs == 0) {
			return 0;
		}
		long durationInMin = durationMs / (1000 * 60);
		return durationInMin;
	}

	/**
	 * This method formats durations using the days and lower fields of the ISO format pattern, such as P7D6TH5M4.321S.
	 * I should just steal Apache Commons, but that gives me grief sometimes.
	 *
	 * @param durationInMillisecondsParam e.g. from Date.getTime()
	 * @return Formatted string in form of PTxxDyyHzzSqqqqq
	 */
	static public String format(long durationInMillisecondsParam) {
		long durationInMilliseconds = durationInMillisecondsParam;
		long numDays = durationInMilliseconds / 86400000;
		durationInMilliseconds -= (numDays * 86400000);
		long numHours = durationInMilliseconds / 3600000;
		durationInMilliseconds -= (numHours * 3600000);
		long numMins = durationInMilliseconds / 60000;
		durationInMilliseconds -= (numMins * 60000);
		long numSecs = durationInMilliseconds / 1000;
		durationInMilliseconds -= (numSecs * 1000);
		System.nanoTime();
		long days = TimeUnit.DAYS.convert(durationInMillisecondsParam, TimeUnit.MILLISECONDS);
		durationInMillisecondsParam -= TimeUnit.DAYS.toMillis(days);
		long hours = TimeUnit.HOURS.convert(durationInMillisecondsParam, TimeUnit.MILLISECONDS);
		durationInMillisecondsParam -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MINUTES.convert(durationInMillisecondsParam, TimeUnit.MILLISECONDS);
		durationInMillisecondsParam -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.SECONDS.convert(durationInMillisecondsParam, TimeUnit.MILLISECONDS);
		durationInMillisecondsParam -= TimeUnit.SECONDS.toMillis(seconds);
		String res = String.format("%dD%dH%dM%dS%04d", numDays, numHours, numMins, numSecs, durationInMilliseconds);
		_log.debug("Manual: \t" + res);
		String auto = String.format("%dD%dH%dM%dS%04d", days, hours, minutes, seconds, durationInMillisecondsParam);
		_log.debug("Auto: \t" + auto);
		return res;
	}

    /**
     * Converts nanoseconds to microseconds (or milliseconds). I wrote this because I am sloppy.
     * @param nanoSec
     * @return Milliseconds, or whatever a duration in nanosecond divided by one million is.
     */
	public static long convertNanoToMSec(long nanoSec) {
		return nanoSec / 1000000;
	}


	public static String format(long numUnits, TimeUnit unit) {
		long days = TimeUnit.DAYS.convert(numUnits, unit);
		numUnits -= TimeUnit.DAYS.toMillis(days);
		long hours = TimeUnit.HOURS.convert(numUnits, unit);
		numUnits -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MINUTES.convert(numUnits, unit);
		numUnits -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.SECONDS.convert(numUnits, unit);
		numUnits -= TimeUnit.SECONDS.toMillis(seconds);
		long nanoSeconds = TimeUnit.NANOSECONDS.convert(numUnits, unit);
		StringBuffer formatter = new StringBuffer("P");

		formatter.append("%dD");
		formatter.append("%02dH");
		formatter.append("%02dM");
		formatter.append("%02d.%010dS");  // always do seconds and 10 digits subsecond precision

		String auto = String.format(formatter.toString(), days, hours, minutes, seconds, numUnits);
		return auto;
	}

    /**
     * People want stuff printed in seconds even if .02314 style.
     * @param nanoSeconds
     * @return
     */
	public static String formatNanoSecondsAsSeconds(long nanoSeconds) {
		double seconds = nanoSeconds / 1000000000d;
		String auto = String.format("%10.10f seconds", seconds);
		return auto;
	}
}
