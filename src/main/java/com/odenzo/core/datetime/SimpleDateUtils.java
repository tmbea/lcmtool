/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * Silly things to do with dates that I can re-use.
 *
 * @author stevef
 * @version $Id: SimpleDateUtils.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class SimpleDateUtils {

	/**
	 * Converts a Date object to a String using the given dateFormat
	 *
	 * @param date       Standard Java Date object. Not null please!
	 * @param dateFormat Date Format in java.text.SimpleDateFormat format.
	 * @return Formatted results
	 * @throws Exception Usually only if the date format is wrong or date is null.
	 */
	final public static String formatDate(Date date, String dateFormat) throws Exception {
		try {
			DateFormat format = new SimpleDateFormat(dateFormat);
			String res = format.format(date);
			return (res);
		} catch (Exception e) {
			throw new Exception("Trouble Formatting Date with pattern " + dateFormat + " date=" + date, e);
		}
	}

	/**
	 * Extracts a Date object from given text using dateFormat supplied. date format description.
	 *
	 * @param date       The text from which to extract the date.
	 * @param dateFormat Date format in the form required by SimpleDateFormat (1.4.2 class)
	 * @return A valid Date object
	 * @throws Exception Thrown if dateformat is invalid, or if could not match the date with dateformat.
	 */
	final public static Date parseDate(String date, String dateFormat) throws Exception {
		try {
			DateFormat format = new SimpleDateFormat(dateFormat);
			Date res = format.parse(date);
			if (res == null) {
				throw new Exception("Parsing Failed - date didn't match format");
			}
			return (res);
		} catch (Exception e) {
			throw new Exception("Trouble Parsing Date, Format: " + dateFormat + " - Src: " + date, e);
		}
	}

	final public static Date parseDate(String date, String dateFormat, TimeZone tz) throws Exception {
		try {

			DateFormat format = new SimpleDateFormat(dateFormat);
			format.setTimeZone(tz);
			Date res = format.parse(date);
			if (res == null) {
				throw new Exception("Parsing Failed - date didn't match format");
			}
			return (res);
		} catch (Exception e) {
			throw new Exception("Trouble Parsing Date, Format: " + dateFormat + " - Src: " + date, e);
		}
	}

}
