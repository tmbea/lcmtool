/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/**
 * $License$
 */
package com.odenzo.core.datetime;

import java.math.BigDecimal;

/**
 * This class represents a duration of time, as in hours/minutes. Currently it is somewhat limited, only representing
 * minute values with no accuracy lower than that.
 *
 * @author stevef
 * @version $Id: TimeDuration.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class TimeDuration {

    /**
     * Shared instance of zero, do not write to this.
     */
    public final static TimeDuration ZERO = new TimeDuration();

    /**
     * TimeDuration is currently stored to minutes precision.
     */
    private int minutes;

    /**
     * A duration with zero value
     */
    public TimeDuration() {
        this(0);
    }

    /**
     * Iniitalize object with number of minutes
     *
     * @param minutes
     */
    public TimeDuration(int minutes) {
        setMinutes(minutes);
    }

    /**
     * Well, a clone would do for now but....
     *
     * @param deepCopy
     */
    public TimeDuration(TimeDuration deepCopy) {
        if (deepCopy != null) {
            this.minutes = deepCopy.minutes;
        }
    }

    /**
     * @param minutes
     * @return
     */
    public void setMinutes(int minutes) {
        this.minutes = minutes;

    }

    public void addMinutes(int minutes) {
        this.minutes += minutes;
    }

    /**
     * Adds the given duration to this, if period is null no-op
     *
     * @param period
     */
    public void add(TimeDuration period) {
        if (period != null) {
            this.minutes += period.getMinutes();
        }
    }

    /**
     *
     * @param hours Hours truncated down to nearest minute. (1.23 Hours = FLOOR(1.23 * 60))
     */
    public void add(BigDecimal hours) {
        this.minutes += (60 * hours.doubleValue());
    }

    /**
     * Return the <em>TOTAL</em> time in minutes (not just the minutes portion)
     *
     * @return
     */
    public int getMinutes() {
        return this.minutes;
    }

    /**
     *
     * @return THe duration in hours (to nearest minute accuracy).
     */
    public double getHours() {
        if (this.minutes == 0) {
            return 0;
        } else {
            return ((double) this.minutes) / 60.0;
        }
    }

    /**
     * Equals compares the minutes value, if same return true
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TimeDuration == false) {
            return false;
        }
        TimeDuration other = (TimeDuration) obj;
        return (other.getMinutes() == this.getMinutes());

    }

    /**
     * @return True if the duration is zero minutes, false otherwise.
     */
    public boolean isNotZero() {
        return (this.minutes != 0);
    }

    public boolean isZero() {
        return (this.minutes == 0);
    }

    public String toString() {
        return " " + getMinutes() + " mins";
    }

    public String toHoursString() {
        double h = getHours();
        return "" + h + " hours";
    }

    /**
     * This is a utility routine that returns the minimum of two TimeDuration objects passed in. It does not create a
     * new TimeDuration objects. If both are null null if returned. If only one null, then the other is returned.
     *
     * @param opA
     * @param opB
     * @return
     */
    public static TimeDuration min(TimeDuration opA, TimeDuration opB) {
        if (opA == null) {
            if (opB == null) {
                return null;
            } else {
                return opB;
            }
        } else if (opB == null) {
            return opA;
        }

        // Both are not null, so compare them
        if (opA.minutes < opB.minutes) {
            return opA;
        } else {
            return opB;
        }

    }

    /**
     * This creates a new time duration and sets it to the value duration - minApproved. If minApproved is null a new
     * object of duration duration is returned.
     *
     * @param duration
     * @param minApproved
     * @return
     */
    public static TimeDuration substract(TimeDuration duration, TimeDuration minApproved) {
        TimeDuration res = new TimeDuration(duration);
        if (minApproved != null) {
            res.minutes -= minApproved.minutes;
        }
        return (res);
    }


}
