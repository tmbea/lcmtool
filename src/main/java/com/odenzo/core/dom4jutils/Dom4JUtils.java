/*
 * Copyright (c) 2009. adenzo software
 */

package com.odenzo.core.dom4jutils;

import com.odenzo.core.datetime.DateUtils;
import com.odenzo.core.utils.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;

/**
 * A collection of DOM4J utils that are valid up to DOM4j  1.6.1
 * This is really a set of reminders and a decoupling a bit from DOM4J 1.6.1 in case change to build-in
 * or DOM4J 2.0 (sometime around 2020?!)
 *
 * @author Steve Franks
 * @version $Id: Dom4JUtils.java 240 2012-05-21 06:45:08Z e1040775 $
 */
public class Dom4JUtils {
	private static Logger _log = LoggerFactory.getLogger(Dom4JUtils.class);

	public static Document parse(File f) throws Exception {
		try {
			return parse(f.toURI().toURL());
		} catch (Exception e) {
			throw new Exception("Trouble Parsing XML File " + f, e);
		}
	}

	/**
	 * Makes a deep copy of a document and its nodes.
	 *
	 * @param d Source document (read-only)
	 * @return Deep clone / copy of the source document d
	 */
	public static Document cloneDocument(Document d) {
		return DocumentHelper.createDocument(d.getRootElement().createCopy());
	}

	/**
	 * Parses an XML document from the given URL, also sets the encoding to UTF-8.
	 * Sometimes that makes a difference (File url), for HTTP probably not.
	 *
	 * @param url
	 * @return
	 * @throws org.dom4j.DocumentException
	 */
	public static Document parse(URL url) throws DocumentException {
		SAXReader reader = new SAXReader();
		reader.setEncoding("UTF-8");
		//	_log.info("Reading XML " + url + " using encoding " + reader.getEncoding());
		return reader.read(url);
	}

	/**
	 * Writes the XML document to given file in pretty format (UTF-8 encoding).
	 *
	 * @param document
	 * @param f
	 * @throws Exception
	 */
	public static void writeToFile(Document document, File f) throws Exception {

		try {

			if (document == null) {
				throw new Exception("The Document was NULL!");
			}
			// lets write to a file
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setEncoding("UTF-8");
			XMLWriter writer = new XMLWriter(new FileWriter(f), format);
			writer.write(document);
			writer.close();


			// Pretty print the document to System.out

			//   writer = new XMLWriter(System.out, format);
			//  writer.write(document);

			// Compact format to System.out
			//  format = OutputFormat.createCompactFormat();
			//  writer = new XMLWriter(System.out, format);
//            writer.write(document);
		} catch (Exception e) {
			throw new Exception("Trouble Writing DOM4J Document to File " + f, e);
		}
	}

	/**
	 * Just because I always forget about this.
	 *
	 * @param doc
	 * @return
	 */
	public static String documentToString(Document doc) {
		return doc.asXML();
	}

	/**
	 * @param res
	 * @return
	 * @throws org.dom4j.DocumentException
	 */
	public static Document parse(String res) throws DocumentException {
		return DocumentHelper.parseText(res);
	}

	public static String compactString(Element e) throws IOException {
		if (e == null) {
			return " [[ No Element - Element was null ]]";
		}
		OutputFormat format = OutputFormat.createPrettyPrint();
		StringWriter sw = new StringWriter();
		XMLWriter writer = new XMLWriter(sw, format);
		writer.write(e);

		return sw.toString();
	}

	/**
	 * Pretty print the XML document to a String
	 *
	 * @param doc
	 * @return
	 * @throws java.io.IOException
	 */
	public static String prettyString(Document doc) throws IOException {
		try {
			// Pretty print the document to System.out
			if (doc == null) {
				return " [[ No Document - Document was null ]]";
			}
			OutputFormat format = OutputFormat.createPrettyPrint();
			StringWriter sw = new StringWriter();
			XMLWriter writer = new XMLWriter(sw, format);
			writer.write(doc);

			return sw.toString();

			//// Compact format to System.out
			//format = OutputFormat.createCompactFormat();
			//PrintStream out = new OutputStream();

			//writer = new XMLWriter(System.out, format);
			//writer.write(document);// TODO: Enable some pretty printing for logging
			//return documentToString(doc);
		} catch (IOException e) {
			throw new IOException("Trouble Printing XML Document " + doc, e);
		}
	}

	/**
	 * Pretty print just the given element (and its sub-nodes) to a String.
	 *
	 * @param elem
	 * @return Element and children as text.
	 * @throws Exception
	 */
	public static String prettyString(Element elem) throws Exception {


		try {
// Pretty print the document to System.out
			if (elem == null) {
				return " [[ No Element - Element was null ]]";
			}
			OutputFormat format = OutputFormat.createPrettyPrint();
			StringWriter sw = new StringWriter();
			XMLWriter writer = new XMLWriter(sw, format);
			writer.write(elem);

			return sw.toString();

			//// Compact format to System.out
			//format = OutputFormat.createCompactFormat();
			//PrintStream out = new OutputStream();

			//writer = new XMLWriter(System.out, format);
			//writer.write(document);// TODO: Enable some pretty printing for logging
			//return documentToString(doc);
		} catch (IOException e) {
			throw new IOException("Trouble Pretting Printing XML Element " + elem, e);
		}
	}


	/**
	 * Makes a document given the root element and tacks on a generatedOn attribute to root element.
	 *
	 * @param root
	 * @param addStandardAttr
	 * @return
	 */
	public static Document makeDocumentWithElement(Element root, boolean addStandardAttr) {
		Element e = (Element) root;
		if (addStandardAttr) {
			e.addAttribute("generatedOn", DateUtils.currentW3CTimestamp());
		}
		return DocumentHelper.createDocument(e);
	}


	/**
	 * Some times things want XML withouth the <! prolog on top. Go figure.
	 *
	 * @param doc
	 * @return
	 * @throws java.io.IOException
	 */
	public static String prettyStringNoProlog(Document doc) throws IOException {
		// Pretty print the document to System.out
		if (doc == null) {
			return " [[ No Document - Document was null ]]";
		}
		OutputFormat format = OutputFormat.createPrettyPrint();
		//  format.setExpandEmptyElements(true);
		format.setOmitEncoding(true);
		format.setSuppressDeclaration(true);

		StringWriter sw = new StringWriter();
		XMLWriter writer = new XMLWriter(sw, format);
		writer.write(doc);
		return sw.toString();
	}

	/**
	 * A utility to routine to add a sibling Element factored out because sooner or later DOM4J will have this routine.
	 * (Or I can actually optimize this poor excuse for code).
	 *
	 * @param doc
	 * @param elementToAddAfter
	 * @param newSiblingElement
	 */
	public static void addSibling(Document doc, Element elementToAddAfter, Element newSiblingElement) {
		List<Element> parentNodes = elementToAddAfter.getParent().elements();
		int indx = parentNodes.indexOf(elementToAddAfter);// Now insert at correct position. fieldList.add(index++, newField);
		parentNodes.add(indx + 1, newSiblingElement);
	}

	/**
	 * DOM4J doesn't have simple way to add element containing value iff value is not null. This hacks it.
	 *
	 * @param parent      Element to add new element if text value would not be null or all whitespace.
	 * @param newElemName
	 * @param value       Value to add, coerced to text if needed via toString()
	 * @return parent element passed in.
	 */
	public static Element addElement(Element parent, String newElemName, Object value) {
		if (parent == null) {
			throw new IllegalArgumentException("Parent Element Cannot Be Null");
		}
		if (value == null) {
			return parent;
		}
		String val;
		if (value instanceof String) {
			val = (String) value;
		} else {
			val = value.toString();
		}
		if (!StringUtils.isEmpty(val)) {
			parent.addElement(newElemName).setText(val);

		}
		return parent;
	}
}
