/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.dom4jutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import java.util.Iterator;

/**
 * This is to allow XPath queries to find stuff in the default namespace.
 *
 * @author stevef
 * @version $Id: PersonalNamespaceContext.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class PersonalNamespaceContext {
	private static Logger _log = LoggerFactory.getLogger(PersonalNamespaceContext.class);

	public String getNamespaceURI(String prefix, String uri) {
		if (prefix == null) {
			throw new NullPointerException("Null prefix");
		} else if ("pre".equals(prefix)) {
			return "http://www.example.com/books";
		} else if ("xml".equals(prefix)) return XMLConstants.XML_NS_URI;
		return XMLConstants.NULL_NS_URI;
	}

	// This method isn't necessary for XPath processing.

	public String getPrefix(String uri) {
		throw new UnsupportedOperationException();
	}

	// This method isn't necessary for XPath processing either.

	public Iterator getPrefixes(String uri) {
		throw new UnsupportedOperationException();
	}
}
