/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Some utilities for reading and writing comma seperated value files.
 *
 * @version $Id: CSVFileUtils.java 161 2012-05-14 07:45:43Z e1040775 $
 */

public class CSVFileUtils {

	private final static Logger _log = LoggerFactory.getLogger(CSVFileUtils.class);

	public static <T> void writeCSVFile(File file, List<List<T>> rowsOfColumns) throws IOException {

		FileWriter fw = new FileWriter(file, false);
		for (List<T> row : rowsOfColumns) {
			writeCSV_Row(fw, row);
		}
		fw.close();

	}

	public static <T> void writeCSV_Row(FileWriter fw, List<T> row) throws IOException {
		for (T col : row) {
			if (col != null) {
				fw.write(col.toString());
			}
			fw.append(',');   // This will tack on extra column at end, but who cares
		}
		fw.append("\n");
	}

	/**
	 * Read CSV file as UTF-8, eats the header (cause it may have , in it - bozoes)
	 *
	 * @param f
	 * @param maxColumns
	 * @return
	 */
	public static List<String[]> readCSVFile(File f, int maxColumns) throws Exception {

		try {
			FileInputStream fis = new FileInputStream(f);
			InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
			LineNumberReader lnr = new LineNumberReader(isr, 1024);


			List<String[]> list = new ArrayList<String[]>();


			String row = lnr.readLine();					 // eat the header, assume 2+ lines
			row = lnr.readLine().trim();
			while (row != null) {
				//  System.out.println("Read Row: " + lnr.getLineNumber() + ":" + row);
				String[] fields = row.split(",", maxColumns);
				list.add(fields);
				row = lnr.readLine();


			}
			return list;
		} catch (Exception e) {
			throw new Exception("Trouble Reading CSV File " + f, e);
		}

	}
}
