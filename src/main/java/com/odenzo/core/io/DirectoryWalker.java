/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

/*
 * $License$
 */
package com.odenzo.core.io;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that walks a directory tree and calls worker process in a depth-first manner. That is, leaf nodes of the tree
 * get callback first, then bread first, then go back up.
 *
 * TODO: This was hacked into the utility classes and needs to be generified a bit.
 *
 * @author stevef
 * @version $Id: DirectoryWalker.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class DirectoryWalker {


	DirectoryWalkerCallback callback = null;

	private List<String> ignoredDirectoryNames = new ArrayList<String>();

	public DirectoryWalker(DirectoryWalkerCallback callback) {
		this.callback = callback;
		ignoredDirectoryNames.add("Ignored".toUpperCase());
		ignoredDirectoryNames.add("Paused".toUpperCase());
	}

	/**
	 * Filter to find all non-hidden subdirectories in a directory
	 */
	private final class SubDirsFilter implements FileFilter {
		public boolean accept(File dir) {
			return (dir.isDirectory() && (dir.isHidden() == false) && (!ignoredDirectoryNames.contains(dir.getName().toUpperCase())));
		}
	}

	public void walkTree(File topDir) throws Exception {
		if (!topDir.exists()) {
			throw new Exception("The File/Dir " + topDir + " did not exist.");
		}
		File[] subDirs = topDir.listFiles(new SubDirsFilter());

		for (int i = 0; i < subDirs.length; i++) {
			walkTree(subDirs[i]);
		}

		callback.processDir(topDir);
	}
}
