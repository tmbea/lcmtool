/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.io;

import java.io.File;

/**
 * Used as interface for DirectoryWalker to do ??
 *
 * @author stevef
 * @version $Id: DirectoryWalkerCallback.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public interface DirectoryWalkerCallback {
	public void processDir(File dir) throws Exception;
}
