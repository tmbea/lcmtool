/*
 * Copyright (c) 2009. adenzo software
 */

package com.odenzo.core.io; /**
 *
 * User: Steve Franks
 * Date: May 3, 2009
 * Source Code Version: $Id: FileUtils.java 211 2012-05-19 11:27:18Z e1040775 $
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * File name and file I/O related utilities.
 *
 * @version $Id: FileUtils.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class FileUtils {
	private final static Logger _log = LoggerFactory.getLogger(FileUtils.class);



	/**
	 * This does not recurse, just finds exactly one file in directory that matces regex. If more than one or zero then an exception is thrown.XS
	 *
	 * @param inDir
	 * @param regex
	 * @return
	 * @throws Exception
	 */
	public static File findSingleFileByRegex(File inDir, String regex) throws Exception {
		try {
			File[] results = inDir.listFiles(new RegexFileFilter(regex));
			if (results.length == 1) {
				return results[0];
			}
			if (results.length > 1) {
				throw new Exception("Found " + results.length + " but expecting only one matching file");
			} else {
				throw new Exception("No matching file found.");
			}
		} catch (Exception e) {
			throw new Exception("Trouble finding direct file by regex " + regex + " in directory " + inDir, e);
		}
	}

	/**
	 * Tries to find a file by name in a list of Java File objects. Does so ignoring case sensitivity.
	 *
	 * @return
	 * @throws Exception
	 */
	public static List<File> findFilesByName(String fileName, List<File> files) throws Exception {
		List<File> res = new ArrayList<File>();
		for (File f : files) {

			if (fileName.equalsIgnoreCase(f.getName())) {
				res.add(f);
			}
		}
		if (res.size() == 0) {
			_log.warn("Could not find File [" + fileName + "] not found");
		} else if (res.size() > 1) {
			_log.warn("Expected exactly one file, found  " + res.size() + " that matched [" + fileName + "] -> " + res);
		}
		return res;
	}


	/**
	 * Have trouble copying read only files to a directory, and then trying to copy over duplicate.
	 * So, need this handy dandy to let the current user have read/write access to a file.
	 * This is a Unix/Mac command.
	 *
	 * @param f
	 */
	public static void setPermissionsToReadWrite(File f) {
		f.setReadable(true, true);
		f.setWritable(true, true);
	}

	/**
	 * This copies a file and makes the file world readable/writable.
	 *
	 * @param srcFile
	 * @param destDir
	 * @throws Exception
	 * @throws InterruptedException
	 */
	public static void cpFile(File srcFile, File destDir) throws Exception {

		try {
			_log.debug("Copying [" + srcFile + "] \t\t to [" + destDir + "]");
			File workingDir = srcFile.getParentFile();

			File destFile = new File(destDir, srcFile.getName());
			if (destFile.exists()) {
				_log.debug("Destination File Exists -- may have trouble overwriting.");
			}


			if (!workingDir.exists()) {
				_log.error("Working Dir Java thinks doesn't exist! [" + workingDir + "]");
			}
			if (!destDir.exists()) {
				_log.error("Destination Direcory doesn't exist! [" + destDir + "]");
			}

			if (!srcFile.exists()) {
				_log.error("Source File Doesn't Exist!");
			}
			if (!srcFile.canRead()) {
				_log.error("Cannot read source file!");
			}
			if (!destDir.canWrite()) {
				_log.error("Cannot write to destination directory");
			}
			//workingDir = new File(workingDir.getCanonicalPath().replace(" ", "\\ "));
			Process process = Runtime.getRuntime().exec(new String[]{"cp", srcFile.getName(), destDir.getCanonicalPath()},
			                                            null,
			                                            workingDir);
			int exitValue = process.waitFor();
			_log.info("Exit Value = " + exitValue);
			if (exitValue != 0) {
				InputStream errorStream = process.getErrorStream();
				String err = readInputStreamToString(errorStream);
				_log.error("StdErr: " + err);

				throw new Exception("Trouble Copying " + srcFile + " to " + destDir);
			}
			destFile.setReadable(true, true);
			destFile.setWritable(true, true);
		} catch (Exception e) {
			throw new Exception("Trouble Copying File " + srcFile + " to " + destDir, e);
		}
	}


	public static void cpRenameFile(File srcFile, File destFile) throws IOException {
		Runtime.getRuntime().exec("cp " + srcFile.getCanonicalFile() + " " + destFile.getCanonicalPath());

	}

	public static void cpFileToDirRenaming(File srcFile, File destDir, String newName) throws IOException, Exception {
		cpFile(srcFile, destDir);
		File destFile = new File(destDir, srcFile.getName());
		File renamedFile = new File(destDir, newName);
		if (!destFile.renameTo(renamedFile)) {
			throw new Exception("Trouble Renaming " + srcFile + " to " + renamedFile);
		}
	}

	public static String readInputStreamToString(InputStream stream) throws IOException {
		InputStreamReader isr = new InputStreamReader(stream);
		BufferedReader br = new BufferedReader(isr);
		String line = null;
		String res = "";
		while ((line = br.readLine()) != null) {
			res += line;
            res += "\n";
		}
		return res;
	}

	static public void copyFiles(List<File> files, File destDir) throws Exception {
		try {
			_log.info("Copying " + files.size() + " files to " + destDir.getCanonicalPath());
			if (!destDir.exists()) {
				_log.info("Making Directory " + destDir.getCanonicalPath() + " because it didn't exist.");
				destDir.mkdirs();
			}
			if (!destDir.isDirectory()) {
				throw new Exception("Dest is not a Dir: " + destDir.getCanonicalPath());
			}


			for (File f : files) {
				FileUtils.cpFile(f, destDir);
			}
		} catch (Exception e) {
			throw new Exception("Trouble", e);
		}
	}

	public static void moveFile(File first, File destDir) throws Exception {
		cpFile(first, destDir);
		first.delete();
	}


	/**
	 * Generate a simple timestamp to append to output file names to make then unqie.
	 * Format is _yyyy-MM-dd_HHmm
	 *
	 * @return
	 */
	public static String timestampForFilename() {
		SimpleDateFormat sdf = new SimpleDateFormat("_yyyy-MM-dd_HHmm");
		return sdf.format(new Date());

	}

	public static File makeOutputFile(File inDir, String baseName, String ext) throws IOException {
		if (inDir.mkdirs()) {
			_log.warn("Had to make output directory " + inDir.getCanonicalPath());
		}
		String name = baseName + timestampForFilename() + "." + ext;
		return new File(inDir, name);

	}


	public static void writeBytesToFile(File outFile, byte[] data, boolean append) throws Exception {
		try {
			FileOutputStream fos = new FileOutputStream(outFile, append);
			fos.write(data);
			fos.close();
		} catch (Exception e) {
			throw new Exception("Trouble writing bytes to file " + outFile, e);
		}
	}

	/**
	 * Finds the last . and removes that and all trailing characters after that from the String.
	 *
	 * @param name
	 * @return
	 */
	public static String removeExtension(String name) {
		int dotIndex = name.lastIndexOf('.');
		if (dotIndex < 1) {
			return name;
		}
		return name.substring(0, dotIndex);
	}

	/**
	 * Recurses down rootDir looking for all nested files whose name matches the given Java regular expression.
	 *
	 * @param rootDir
	 * @param regex
	 * @return
	 * @throws Exception
	 */
	public static List<File> findNestedFiles(File rootDir, String regex) throws Exception {

		final List<File> res = new ArrayList<File>();
		final FileFilter filter = new RegexFileFilter(regex);
		DirectoryWalkerCallback cb = new DirectoryWalkerCallback() {
			@Override
			public void processDir(File dir) throws Exception {
				File[] files = dir.listFiles(filter);
				Collections.addAll(res, files);
			}
		};

		DirectoryWalker walker = new DirectoryWalker(cb);
		walker.walkTree(rootDir);
		return res;

	}

	/**
	 * Looks for files in the rootDir WITHOUT recurding into subdirectory for a list of files that match the Java
	 * regular expression.
	 *
	 * @param rootDir
	 * @param regex
	 * @return
	 * @throws Exception
	 */
	public static List<File> findChildFiles(File rootDir, String regex) throws Exception {
		final FileFilter filter = new RegexFileFilter(regex);
		final File[] fArray = rootDir.listFiles(filter);
		if (fArray == null) {
			return new ArrayList<File>();
		}
		final List<File> res = Arrays.asList(fArray);
		return res;

	}


	/**
	 * Find the relative path of file with respect to baseDir such that <pre>new File(baseDir, relPath) == file</pre>
	 *
	 * @param file    A file contained withing the baseDIr directory tree.
	 * @param baseDir
	 * @return
	 */
	public static String getPathRelativeTo(File file, File baseDir) throws Exception {
		try {
			String basePath = baseDir.getCanonicalPath();
			String filePath = file.getCanonicalPath();
			if (!filePath.startsWith(basePath)) {
				throw new Exception("It Appears the File is not in given directory tree: " + basePath + " not contains " + filePath);
			}
			return filePath.substring(basePath.length());
		} catch (Exception e) {
			throw new Exception("Trouble Getting Relative Path of " + file + " to " + baseDir, e);
		}
	}

	/**
	 * Listsall directorries that are not hidden.
	 *
	 * @param serviceDir
	 * @return
	 */
	public static List<File> listDirs(File serviceDir) {
		File[] res = serviceDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && !pathname.isHidden();
			}
		});
		return Arrays.asList(res);
	}

	/**
	 * Listsall directorries that are not hidden and match the regex.
	 *
	 * @param serviceDir
	 * @return
	 */
	public static List<File> listDirs(File serviceDir, final String regex) {
		File[] res = serviceDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				boolean isDir = pathname.isDirectory() && !pathname.isHidden();
				boolean matches = pathname.getName().matches(regex);
				return isDir && matches;
			}
		});
		return Arrays.asList(res);
	}


	
}
