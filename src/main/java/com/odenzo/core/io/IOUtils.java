/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.io;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * TODO: This is confused with FileUtils, refactor nicely.
 * The discriminator is wether the routine operates on an existing file or not and is not some query shit on the file name
 * or file attributes.
 * <p/>
 * $Id: IOUtils.java 211 2012-05-19 11:27:18Z e1040775 $ * * A collection of simple I/O utilities, centered around disk
 * access
 */

public class IOUtils {
    static final Logger _log = LoggerFactory.getLogger(IOUtils.class);


    /**
     * Creates a directory, making intermediaries with nice error reporting that standard mkdirs();
     * @param dir
     * @throws java.io.IOException
     */
    public static void createDir(File dir) throws IOException {
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new IOException("Could not create directory "+ dir);
            }
        }
    }

    /**
     *
     * @param theFile
     * @param encoding
     * @return The number of characters in the given file (encoding sensitive).
     * @throws Exception
     */
    public static int countCharactersInFile(File theFile, String encoding) throws Exception {
        // TODO: Better way (no memory usage) or worse way just load as String and to a .length
        // Look at the NIO stuff to see if better way now.
        int charCount = 0;
        long byteLength = 0;
        try {
            byteLength = theFile.length();
            FileInputStream fis = new FileInputStream(theFile);
            InputStreamReader isr = new InputStreamReader(fis, encoding);
            BufferedReader br = new BufferedReader(isr);
            while (br.read() != -1) {
                charCount++;
            }
            return (charCount);
        } catch (Throwable e) {
            throw new Exception("Trouble Getting Character Count of file " + theFile + " after reading "
                    + charCount + " chars of file with byte count of " + byteLength + " using encoding " + encoding, e);
        }
    }

    /**
     * Reads a file into a string, converting to Unix line ending
     *
     * @param theFile
     * @param encoding
     * @return
     * @throws Exception
     */
    public static String loadFile(File theFile, String encoding) throws Exception {
        try {
            _log.debug("Loading File as String: " + theFile.getAbsolutePath());
            _log.debug("Using Encoding: " + encoding);
            char kEOL = '\n';
            FileInputStream fis = new FileInputStream(theFile);
            InputStreamReader isr = new InputStreamReader(fis, encoding);
            BufferedReader br = new BufferedReader(isr);

            StringBuilder res = new StringBuilder(1000);
            String aLine;
            while ((aLine = br.readLine()) != null) {
                res.append(aLine).append(kEOL);
            }
            return (res.toString());

        } catch (Exception e) {
            throw new Exception("Error Loading File as String: File " + theFile.getAbsolutePath(), e);
        }
    }

    /**
     * Writes a string to a file in UTF-8 encoding
     *
     * @param theFile
     * @param data
     * @throws Exception General exception if problems
     */
    public static void writeToFile(File theFile, String data) throws Exception {
        try {
            FileOutputStream fos = new FileOutputStream(theFile);
            OutputStreamWriter os = new OutputStreamWriter(fos, "utf-8");
            os.write(data);
            os.flush();
            os.close();
        } catch (Exception e) {
            throw new Exception("Error writing to file" + theFile, e);
        }
    }

    /**
     * Returns the part of the file name past the last '.' character
     *
     * @param theFile
     * @return
     * @throws  Exception General exception if problems
     */
    public static String getExtension(File theFile) throws Exception {
        try {
            String name = theFile.getName();
            int lastDot = name.lastIndexOf('.');
            String extension = name.substring(lastDot + 1);
            return (extension);
        } catch (Exception e) {
            throw new Exception("Trouble Finding Extension of File", e);
        }
    }

    /**
     * Returns the name of the files sans file extension (from the last dot onwards)
     *
     * @param theFile
     * @return
     * @throws Exception
     */
    public static String getFileNameNoExtension(File theFile) throws Exception {
        try {
            String name = theFile.getName();
            int lastDot = name.lastIndexOf('.');
            String fileNameNoExt = name.substring(0, lastDot);
            return (fileNameNoExt);
        } catch (Exception e) {
            throw new Exception("Trouble Finding Extension of File", e);
        }
    }

    /**
     * Attempts to copy srcFile to destFile .
     * There was a reason I wrote this but I forget why now. I rememebr File.renameTo() gave sporadic errors.
     * ANyway, should look at NIO.
     *
     * @param srcFile
     * @param destFile
     * @throws
     */
    public static void copyFile(File srcFile, File destFile) throws Exception {
        try {

            FileInputStream fis = new FileInputStream(srcFile);
            BufferedInputStream bis = new BufferedInputStream(fis);

            FileOutputStream fos = new FileOutputStream(destFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            byte[] buffer = new byte[512];
            while (true) {
                int count = bis.read(buffer);
                if (count == -1) {
                    break;
                }
                bos.write(buffer, 0, count);
            }
            bis.close();
            bos.close();
            fis.close();
            fos.close();
            /**
             * // Java 1.4 nio package try { // Create channel on the source FileChannel srcChannel = new
             * FileInputStream("srcFilename").getChannel(); // Create channel on the destination FileChannel dstChannel =
             * new FileOutputStream("dstFilename").getChannel(); // Copy file contents from source to destination
             * dstChannel.transferFrom(srcChannel, 0, srcChannel.size()); // Close the channels srcChannel.close();
             * dstChannel.close(); } catch (IOException e) { }
             */
        } catch (Exception e) {
            throw new Exception("Trouble copying " + srcFile + " to " + destFile, e);
        }
    }

    /**
     * Searches through the entire baseDir tree and gets a list of files matching the Java regex, as applied to dir.listFiles
     *
     * @return List of all files in the directory tree that match the regex.
     */
    public static List<File> findAllMatchingFiles(File baseDir, String regex) throws Exception {

        final List<File> files = new ArrayList<File>(100);
        final FileFilter filter = new RegexFileFilter(regex);
        DirectoryWalkerCallback cb = new DirectoryWalkerCallback() {

            public void processDir(File dir) throws Exception {
                File[] dirList = dir.listFiles(filter);
                files.addAll(Arrays.asList(dirList));
            }
        };
        DirectoryWalker walker = new DirectoryWalker(cb);
        walker.walkTree(baseDir);
        return files;
    }

    /**
     * For a valid java package, e.g. com.foo.bar this fill create a File object relative to baseDir corresponding to it.
     * baseDir/com/foo/bar.   It does not actually create the directory.
     * @param baseDir
     * @param javaPackage
     * @return
     * @throws java.io.IOException
     */
    public static File makeFileDirForJavaPackage(File baseDir, String javaPackage) throws IOException {
        _log.debug("Java Package: {}", javaPackage);
        String[] dirs = javaPackage.split("\\.");
        _log.debug("Split into Dirs {}", dirs);
        File res = new File(baseDir.getCanonicalPath());
        for (String p : dirs) {
            res = new File(res, p);
        }
        return res;
    }
}
