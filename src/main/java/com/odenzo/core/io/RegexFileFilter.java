/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.io;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Java IO package uses FileFilter in many places. This has some utility routines to make FileFilter objects easier.
 *
 * @version $Id: RegexFileFilter.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class RegexFileFilter implements FileFilter {
	final Logger _log = LoggerFactory.getLogger(RegexFileFilter.class);

    /**
     * A Filter to just return directories that ARE NOT HIDDEN. This is kinda Unixy
     * because .svn directories and stuff I usually don't want to traverse and they are hidden.
     *
     * @return A pre-mase Directory Filer
     */
	public static RegexFileFilter directoryFilter() {
		return new RegexFileFilter("") {
			@Override
			public boolean accept(File dir) {
				return (dir.isDirectory() && (!dir.isHidden()));
			}
		};
	}

	private Pattern regexPattern = null;

	/**
	 * Filter files based on their name and extension matching the Sun style regex. The regex IS NOT applied to the
	 * directory path.
	 *
	 * @param sunRegex Regex to apply to the filename only. (File.getName())
	 */
	public RegexFileFilter(Pattern sunRegex) {
		regexPattern = sunRegex;
	}

	/**
	 * Filter files base don their name and extension matching the Sun style regex. The regex IS NOT applied to the
	 * directory path.
	 *
	 * @param sunRegex Regex to apply to filename only.
	 */
	public RegexFileFilter(String sunRegex) {
		regexPattern = Pattern.compile(sunRegex);
	}

	public boolean accept(File file) {
		_log.debug("Testing File: " + file.getAbsolutePath());
		String name = file.getName();
		Matcher m = regexPattern.matcher(name);
		boolean accept = m.matches();
		_log.debug("File Name: " + name + " Accepted: " + accept + " on regex " + regexPattern.pattern());
		return (accept);
	}

	public String getRegex() {
		return regexPattern.pattern();
	}

	public String toString() {
		return "RegexFileFilter with Pattern " + getRegex();
	}
}
