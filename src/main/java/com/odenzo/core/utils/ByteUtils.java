/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * General byte utilities to compensate for Java sucking.
 * This still sucks, needs a cleanup or two.
 * @author stevef
 * @version $Id: ByteUtils.java 298 2012-06-06 04:24:03Z e0084502 $
 */
public class ByteUtils {
    private static Logger _log = LoggerFactory.getLogger(ByteUtils.class);

    public static Character[] hexDigits;

    public static List<Character> hexList;

    static {
        hexDigits = new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        hexList = Arrays.asList(hexDigits);

    }


    /**
     * Kind of strange, takes the first (low) eight bits of the int and outputs two hex
     * digits with no 0x at the front.
     *
     * @param val This is integer with value between 0 and 255 because I hate signed bytes.
     * @return Two hex characters representing the val. No spaces around at all.
     */
    public static String byteToHexString(int val) {
    	val = val & 0xFF;
        if (val < 0 || val > 255) {
            throw new IllegalArgumentException("Value " + val + " was not between 0 and 255 (inclusive) as a byte val should be");
        }
        byte cast = (byte) val;
        String res = String.format("%02X", val);
        _log.debug("Value {} yielded {}", val, res);
        return res;
    }


    /**
     * Convert byte array value into the hex value string
     * for tracability purpose   Spaces between each 2 hex characters and space at end of line.
     * TODO: No use having this and byteToHexString style.
     *
     * @param byteArray input array
     * @return string value containing hex decimal
     */
    public static String printHexValue(byte[] byteArray) {

        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < byteArray.length; i++) {
            String str;
            str = String.format("%02X ", byteArray[i]);
            buffer.append(str);

        }
        buffer.append(" ");
        return buffer.toString();
    }


    /**
     * Takes a byte value and converts to Hex in 0xFF stytle but without the Ox.
     * Check to see if String.format is better/
     *
     * @param b Byte value to format. Note that bytes are signedi in Java. Go figure.
     * @return Two character string, one character for top 4 bits, one for bottom 4 regardless of if zero or not.
     */
    public static String byteToHex(Byte b) {

        return byteToHexString(b & 0xff);

    }

    /**
     * Utility routine for creating a string from an array of bytes per @see byteToHex (no 0x).
     * A space is placed between each byte. No line ending.
     *
     * @param bs An array of bytes to convert to String.
     * @return
     */
    public static String bytesToHex(byte[] bs) {
        StringBuilder sb = new StringBuilder(bs.length * 3);
        for (byte b : bs) {
            sb.append(byteToHex(b));
            sb.append(" ");

            // Could add a line ending but...
        }
        return sb.toString();
    }
    
    
    /**
     * Utility routine for creating a string from an array of bytes per @see byteToHex (no 0x).
     * A space is placed between each byte. No line ending.
     * 
     * This is slow transformatio from byte[] to ByteBuffer
     *
     * @param bs An ByteBuffers containing bytes to convert to String.
     * @return
     */
    public static String bytesToHex(ByteBuffer bs) {
    	
        StringBuilder sb = new StringBuilder(bs.limit() * 3);
        bs.rewind();
        while (bs.hasRemaining()) {
            sb.append(byteToHex(bs.get()));
            sb.append(" ");
        }
        return sb.toString();
    }
    

    /**
     * Utility routine for creating a string from an Collection of bytes per @see byteToHex (no 0x).
     * A space is placed between each byte. No line ending.
     *
     * @param inp An collection of bytes to convert to String.
     * @return
     */
    public static String bytesToHex(Collection<Byte> inp) {
        StringBuilder sb = new StringBuilder(inp.size() * 3);
        for (Byte b : inp) {
            sb.append(byteToHex(b));
            sb.append(" ");
        }
        return sb.toString();
    }


    /**
     * Given a byte return a string with 8 corresponding ones and zeroes. (Two complement represensation for most data types (except car)
     *
     * @param b Will be converted to string, e.g. 0000:1111, bitwise from 2-cpomplement encoding. e.g. 0xFF = -1 in byte.
     * @return 8 character String consisting of 0 and 1.
     */
    public static String byteToBitString(Byte b) {
        StringBuilder bitres = new StringBuilder(8);

        int mask = 128; // 8th bit on
        for (int i = 0; i < 8; i++) {
            bitres.append((b & mask)==0?'0':'1');
            mask = mask >> 1;
            if (i == 3) {
                bitres.append(':');
            }
        }
        return bitres.toString();
    }


    /**
     * TODO: More crap code to fix.  Or avoid using and delete even better.
     * <p/>
     * Convert a List of Byte objects to an array of built-in byte
     *
     * @param inp Input to convert
     * @return Unchanged converted bytes from input
     */
    public static Byte[] listToArray(List<Byte> inp) {
        return inp.toArray(new Byte[1]);
    }

    /**
     * Utility to make a string of 1 and 0 from an array of bytes.
     *
     * @param suspectBytes
     * @return
     */
    public static String bytesToBinary(byte[] suspectBytes) {
        StringBuilder res = new StringBuilder(suspectBytes.length * 9);
        for (byte b : suspectBytes) {
            res.append(byteToBitString(b)).append(" ");
        }
        return res.toString();
    }


    /**
     * A general hack utility, tries to print out array of bytes in general readable form on the
     * assumption that it is mostly text, and we know nothing about the format.
     * Throws to theLog at debug level.
     * TODO: Have option to make it look like IBM TSO Dump
     *
     * @param theLog
     * @param encoding Assumed encoding of the byte[]
     */
    public static void dumpBytes(Logger theLog, Charset encoding, ByteBuffer data){
    	ByteBuffer buffer = data.duplicate();
    	byte[] bytes = new byte[buffer.limit()];
    	buffer.get(bytes);
    	dumpBytes(theLog, encoding, bytes);
    }
    
    public static void dumpBytes(Logger theLog, Charset encoding, byte[] data) {
        int byteNum = 0; // Assume 1 char per byte encoding.
        int offset = 0;
        int linesize = 16;
        int numLines = data.length / linesize + (data.length % linesize > 0 ? 1 : 0);
        String str = new String(data, encoding);
        for (int lineNum = 0; lineNum < numLines; lineNum++) {
            CharSequence aLine = str.subSequence(offset, Math.min(data.length, offset + linesize));
            StringBuilder charOut = new StringBuilder();
            StringBuilder hexOut = new StringBuilder();

            // sigh... maybe a StringUtil.isPrintable(char c) for reuse. This is 1/2 right probably.

            for (int cNum = 0; cNum < aLine.length(); cNum++) {
                Character c = aLine.charAt(cNum);
                hexOut.append(ByteUtils.byteToHex(data[byteNum])).append(':');
                if (Character.isLetterOrDigit(c)) {
                    // Log as is.
                    charOut.append(c);
                } else if (!Character.isValidCodePoint(c) || (!Character.isDefined(c))) {
                    charOut.append('.');
                } else if (Character.isSpaceChar(c) || Character.isHighSurrogate(c) || Character.isLowSurrogate(c) || Character.isISOControl(c)) {

                    charOut.append('.');
                }
                byteNum++;
                if ((cNum + 1) % 4 == 0) {
                    charOut.append(' ');
                    hexOut.append(' ');
                }

            }
            String lineDesc = String.format("Start %04d  - End %04d\t", offset, offset + linesize - 1);
            theLog.debug(lineDesc + "Text: [{}]   \t Hex[{}]", charOut, hexOut);
            offset += linesize;

        }
    }

}
