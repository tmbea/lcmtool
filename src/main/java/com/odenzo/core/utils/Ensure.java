/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.utils;



/**
 * This class provides an assert like mechanism. If the operation fails a Exception is thrown *
 *
 * @version $Id: Ensure.java 161 2012-05-14 07:45:43Z e1040775 $
 */
public class Ensure {

	/**
	 * Ensures the given string is an integer value. If so, returns the value, if not throws * a Exception with the
	 * msg and appends Must be Integer value, Original Value: {intStringVal} * Does the parsing in base 10
	 *
	 * @param msg
	 * @param intStringVal
	 * @return
	 * @throws
	 */
	public static int isInteger(String msg, String intStringVal) throws IllegalArgumentException {
		try {
			int val = Integer.parseInt(intStringVal, 10);
			return (val);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(msg + " must be integer value, Original Value:[" + intStringVal + "]", e);
		}
	}

    /**
     * No need to comment how bad this is. Not sure my original use case :-)
     * @param msg
     * @param val
     * @param validValues
     * @throws Exception
     */
	public static void isFromList(String msg, String val, String[] validValues) throws Exception {
		String validList = " was not one of the following values: ";
		for (int i = 0; i < validValues.length; i++) {
			if (val.equals(validValues[i])) {
				return;
			} else {
				validList = validList + " : " + validValues[i];
			}
		}
		throw new Exception(msg + validList);
	}
}
