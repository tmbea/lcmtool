package com.odenzo.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.*;
import java.io.IOException;

/**
 * Quick and dirty JNDI dumper. This is J2SE code, not JEE code. DOn't pollute.
 *
 * @author stevef
 * @version $Id: JNDIDumper.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class JNDIDumper {
    private static Logger _log = LoggerFactory.getLogger(JNDIDumper.class);


    /* Could do this in property files instead.
    ### JBossNS properties
java.naming.factory.initial=org.jnp.interfaces.NamingContextFactory
java.naming.provider.url=jnp://localhost:1099
java.naming.factory.url.pkgs=org.jboss.naming:org.jnp.interfaces
/*
     */

    /**
     * Note: This also sets the System Properties INITIAL_CONTEXT_FACTORY and PROVIDER_URL so has side effects.
     *
     * @param providerUrl e.g. file:///tmp
     * @return Initial context.
     * @throws javax.naming.NamingException
     */
    public static InitialContext setupJavaSEInitialContext(String providerUrl) throws NamingException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        System.setProperty(Context.PROVIDER_URL, "file:///tmp");
        InitialContext ic = new InitialContext();
        return ic;
    }

    private static final String INDENT = "\t";
    private String eol;

    public JNDIDumper() {
        this.eol = JVMUtils.getSystemEOL();
    }

    /**
     * Dumps the default JNDI context (InitialContext) to a String for logging.
     *
     * @return Developer dump of JNDI Context
     * @throws Exception
     */
    public String dumpDefaultContext() throws Exception {
        InitialContext context = new InitialContext();
        return dumpContext(context);
    }

    public String dumpContext(Context context) throws Exception {
        StringBuilder builder = new StringBuilder();
        printNode(context, 1, builder);
        return builder.toString();
    }

    /**
     * Recursively prints the contents of a naming context and all subcontexts to the StringBuilder
     *
     * @param context     JNDI Context being dumped;
     * @param indentLevel Current indentation level. Start with 1 for intial call.
     * @param out         StringBuilder in which the developer dump is printed to.
     */
    private void printNode(Context context, int indentLevel, StringBuilder out) throws NamingException, IOException {
        for (NamingEnumeration names = context.listBindings(""); names.hasMore(); ) {
            Binding binding = (Binding) names.next();
            //out.print(binding.getName());
            printBinding(binding, indentLevel, out);
            Object obj = binding.getObject();
            if (obj instanceof Context) {
                printNode((Context) obj, indentLevel + 1, out);
            }
        }
    }

    /**
     * @param binding
     * @param indentLevel
     * @param out
     * @throws java.io.IOException
     */
    private void printBinding(Binding binding, int indentLevel, StringBuilder out) throws IOException {
        out.append(padding(indentLevel)).append("+ '");
        out.append(binding.getName()).append(eol);
        //buffer.append(binding.getName()).append("' is a ");
        //buffer.append(binding.getClassName()).append("<br/>");

    }


    /**
     * Returns a string that will be used for padding/indenting any bindings
     * we will render. We can't really rely on JavaSE 5 String.format() here as
     * this must also work for 1.4 installations (yes, there's still plenty of
     * them around).
     * TODO: Should just use StringUtils instead probably.
     *
     * @param indentLevel
     */

    private String padding(int indentLevel) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < indentLevel; i++) {
            buffer.append(INDENT);
        }
        return buffer.toString();
    }
}
