package com.odenzo.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;

/**
 * Collectiopn of Java Environmental Utilities, for JDK6+
 *
 * @author stevef
 */
public class JVMUtils {
	private static Logger _log = LoggerFactory.getLogger(JVMUtils.class);


    /**
     * Each JVM has default end of line. Best to stick to Unix (\n) I think.
     * But when want the local line ending please use this.
     * @return Line ending as defined per the JVM line.separator property.
     */
	public static String getSystemEOL() {
		return System.getProperty("line.separator"); // This is mandatory property;
	}


	/**
	 * Gets the classpath and nicely (?) formats as one long multiline string for logging.
     * @return
	 */
	public static String dumpClasspath() {

		final String pathSeperator = System.getProperty("path.separator");
		final String linePad = getSystemEOL() + "\t\t";
		String classpath = System.getProperty("java.class.path");
		String[] paths = classpath.split(pathSeperator);
		StringBuilder formattedPaths = new StringBuilder("Current Java Classpath: ");

		for (String s : paths) {
			formattedPaths.append(linePad).append(s);
		}
		_log.debug("ClassPath: {}", formattedPaths);
		return formattedPaths.toString();
	}


    /**
     * Logs all the JVM known encoding by key and character set at info level.
     */
    public static void dumpKnownEncodings() {

        SortedMap<String, Charset> map = Charset.availableCharsets();
        for (Map.Entry<String,Charset> encoding : map.entrySet() ) {
            _log.info("Encoding [{}] with CharSet {}", encoding.getKey(), encoding.getValue());
            
        }
    }

    /**
     * Dumps ALL the system properties into a String for logging.
     * Note: This is just a crude developer hack.
     * @return String containing all system properties. Currently unsorted and unfiltered.
     */
    static public String dumpAllProperties() {
        final StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        Properties props = System.getProperties();
        props.list(pw);
        pw.flush();
        return writer.toString();
    }

    /**
     * TODO: NOT IMPLEMENTED
     */
    static public void dumpPropertiesNoFonts() {

    }

    /**
     * TODO: NOT IMPLEMENTED
     */
    static public void dumpPropertiesOnlyFonts() {

    }

    /**
     * Prints all the standard properties which must be there
     * TODO: NOT IMPLEMENTED
     */
    static public void dumpPropertiesStandard() {

    }
}
