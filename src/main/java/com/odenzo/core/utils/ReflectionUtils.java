package com.odenzo.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * Reflection and invocation utilities.
 * TODO: Merge in full odenzo and adenzo code base here sometime as needed.
 * @version $Id$
 */
public class ReflectionUtils {
     private final static Logger _log = LoggerFactory.getLogger(ReflectionUtils.class);


    /**
     * This is a wrapper to set a property of an arbitrary object. Not sure the best way to do it as get
     * permission errors sometimes.
     * TODO: This should really be in a general Java utilities file. Used for fancy parsing framework (not in FIS version)
     *
     * @param destObject
     * @param name
     * @param value
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public static void setProperty(Object destObject, String name, Object value) throws Exception {
        try {
            Class clazz = destObject.getClass();
            Method setter = clazz.getMethod(name, value.getClass());
            setter.invoke(destObject, value);
        } catch (Exception e) {
            throw new Exception("Troubling Invoking Method [" + name + "] on object " + destObject + " with value " + value);
        }
    }

    /**
     * @param clazz  Class to look for setter on (or its superclasses)
     * @param method The full setter method, e.g. setYourName
     * @param param  The single class the setter takes as a parameter, e.g. Class.for(java.lang.String)
     */
    public static Method getSetterMethodForProperty(Class clazz, String method, Class param) throws NoSuchMethodException {
        return clazz.getMethod(method, param);
    }

    public static Method findSetterMethodForProperty(Class clazz, String method, Class param) throws NoSuchMethodException {
        if (method == null) {
            return null;
        }
        return clazz.getMethod(method, param);
    }
}
