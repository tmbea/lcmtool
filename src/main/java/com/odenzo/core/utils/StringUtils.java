/*
 * Copyright (c) 2009. adenzo software
 */

package com.odenzo.core.utils;/**
 *
 * User: Steve Franks
 * Date: Apr 12, 2009
 * Source Code Version: $Id: StringUtils.java 265 2012-05-26 18:36:53Z e1040775 $
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;


/**
 * Various StringUtil that rely on no external libraries.
 * TODO: Needs a review for duplicates and some optimization.
 * @author Steve Franks
 * @version $Id: StringUtils.java 265 2012-05-26 18:36:53Z e1040775 $
 */
public class StringUtils {
    private final static Logger _log = LoggerFactory.getLogger(StringUtils.class);

    final static private String zeroBuffer = "0000000000000000000";

    static String spaces;

    static {
        spaces = "     ";
        for (int i = 0; i < 10; i++) {
            spaces += spaces;
        }
    }

    /**
     * For when white spaces should equal NULL.
     *
     * @param val String to check.
     * @return If the given val is only whitespace then return null, else the return original string.
     */
    public static String nullIfBlank(String val) {
        if (val == null) {
            return val;
        }
        if (val.trim().length() < 1) {
            return null;
        }
        return val;
    }

    /**
     * Returns true is the string is null or composed only of whitespaces.
     *
     * @param val A possible null String var
     * @return true is val is null or consists solely of whitespace, else false.
     */
    public static boolean isEmpty(String val) {
        if (val == null) {
            return true;
        }
        return val.trim().length() < 1;
    }

    /**
     * Lame routine to remove the token string from end of given string if it exists.
     * Oddly I use this alot.
     *
     * @param data  Original String to modify, this is null safe.
     * @param token Trailing token to check for and remove if present.
     * @return Original data unless ends with token, in which case data with token removed.
     */
    public static String removeTrailingToken(String data, String token) {
        if (data == null) {
            return null;
        }
        if (data.endsWith(token)) {
            return data.substring(0, data.length() - token.length());
        } else {
            return data;
        }
    }

    /**
     * Find the number of occurances of substring in val. A regular expression may be better way to do but it works...
     *
     * @param val       The string to search for substring in.
     * @param substring Substring.
     * @return Number of times Substring occurs (non-overlapping) in the original string.
     */
    public static int countOccurancesOfString(String val, String substring) {
        int count = 0;
        int offset = 0;
        int loc = 0;
        while ((loc = val.indexOf(substring, offset)) > 0) {
            count++;
            offset = loc + substring.length();
        }
        return count;
    }

    /**
     * Literal check of number of times a char appears in the string (regex special characters cause no problems).
     *
     * @param val String to check for character occurance in.
     * @param c   Character to find occurances of in val.
     * @return Number of times c occurs in val. If val null then returns zero.
     */
    public static int countOccurances(String val, char c) {
        if (val == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < val.length(); i++) {
            if (val.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    /**
     * Takes the word and if size less than length right pads with spaces.
     * Note, this will not truncate the original word to given length if word is longer than length.
     *
     * @param word   The original word. This is not trimmed of spaces on left or right.
     * @param length The size of the padded result desired.
     * @return Original word with a number of spaces appended to make the original lenght.
     */
    public static String leftAlignRightPad(String word, int length) {

        if (word.length() < length) {
            return String.format("%-" + length + "s", word);
        }
        return word;
    }

    /**
     * Trims given work and places in field of length with right padding using pad character.
     * Not, this does not truncate long words to fit into field.
     *
     * @param word
     * @param length
     * @param pad
     * @return
     */
    public static String leftAlignRightPad(String word, int length, char pad) {
        String t = word.trim();
        if (word.length() < length) {
            String format = "%-" + pad + length + 's';
            _log.debug("Format [" + format + "]");
            return String.format(format, word);
        }
        return word;
    }

    /**
     * Trims given work and places in field of length with right padding using pad character.
     * Not, this does not truncate long words to fit into field.
     *
     * @param word
     * @param length
     * @return
     */
    public static String leftAlignRightZeroPad(String word, int length) throws Exception {
        String t = word.trim();
        if (t.length() < length) {
            String format = "%-" + length + 's';
            _log.debug("Format [" + format + "]");
            String spacePad = String.format(format, word);
            String res = spacePad.replace(' ', '0');
            if (res.length() != length) {
                throw new Exception("leftAlignRightZeroPad produced wrong length [" + res + "] from [" + word + "] should have been " + length);
            }
            return res;
        }
        return word;
    }

    public static boolean isNotEmpty(String val) {
        return !isEmpty(val);
    }

    /**
     * Takes the original word and removes N characters from the right side.
     * Error if numCharToDelete is greater than originWord.length
     *
     * @param originWord
     * @param numCharToDelete
     * @return Value or originWord minus characters on the right hand side.
     */
    public static String removeLastNChars(String originWord, int numCharToDelete) {
        return originWord.substring(0, originWord.length() - numCharToDelete);
    }

    /**
     * Right aligns the text and zero pads to the number of digits specified.
     * TODO: Switch to String.format() method.
     *
     * @param text
     * @param numDigits
     * @return
     */
    public static String zeroPadLeft(String text, int numDigits) throws Exception {
        if (text.length() > numDigits) {
            throw new Exception("Text [" + text + "] too long. Cannot Pad to Total Length: " + numDigits);
        }
        if (text.length() == numDigits) {
            return text; // No padding needed. Text is immutable, no worries.
        }

        // Need to pad some number of zeros on the left if get here.

        int zerosToAdd = numDigits - text.length();

        if (zerosToAdd <= StringUtils.zeroBuffer.length()) {
            String res = zeroBuffer.substring(0, zerosToAdd) + text;
            if (res.length() != numDigits) {
                throw new Exception("Programming Error in zeroPadLeft!!");
            }
            return res;  // TODO: Unit Test
        }

        // Slow way for very long padding. Probably never actually used.
        String zeros = "";
        for (int i = 0; i < zerosToAdd; i++) {
            zeros = zeros + "0";
        }
        String res = zeros + text;
        _log.debug("LeftZeroPadded [" + text + "] to [" + res + "] of total length " + numDigits);
        if (res.length() != numDigits) {
            throw new Exception("Programming Error in zeroPadLeft!!");
        }
        return res;
    }

    /**
     * Truncates the string to the maximum length supplied. If current lengthis less just returns same string
     * else a substring.
     *
     * @param src
     * @param toLength
     * @return
     */
    public static String truncate(String src, int toLength) {
        if (src.length() < toLength) {
            return src;
        }
        return src.substring(0, toLength);
    }


    /**
     * If n is less than one empty string is returned.
     *
     * @param n
     * @return
     */
    static String getNSpaces(int n) {
        if (n < 1) {
            return "";
        }
        if (n <= spaces.length()) {
            return spaces.substring(0, n);
        }
        throw new IllegalArgumentException("Can only get maximum of " +spaces.length() + " spaces. N of " + n + " too big");
    }

    /**
     * This left zero pads the src string to total length of limit and returns new string.
     *
     * @param strbuf
     * @param limit
     * @return Strbuf with zeros prepended to match the limit.
     * @throws java.io.IOException
     */
    static String getZero(String strbuf, int limit) throws Exception {

        try {
            int cnt = strbuf.length();
            int len = limit - cnt;
            String leftPad = zeroBuffer.substring(0, len);
            return leftPad + strbuf;
        } catch (Exception e) {
            throw new Exception("Error Padding Zeros ", e);
        }

    }

    /**
     * Left pads with spaces
     *
     * @param strbuf
     * @param limit
     * @return
     * @throws java.io.IOException
     */
    static String appendSpace(String strbuf, int limit) throws IOException {

        try {
            int cnt = strbuf.length();
            if (cnt > 10) {
                return strbuf.substring(0, 10);
            } else {
                int len = limit - cnt;
                String buf = "";
                for (int i = 0; i < len; i++) {
                    buf += " ";
                }
                return strbuf + buf;
            }
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * This truncates strbuf to the limit and appends a space if its greater than limit, else return strbuf.
     *
     * @param strbuf
     * @param limit
     * @return
     * @throws Exception
     */
    static String limitFieldName(String strbuf, int limit) throws Exception {

        String strLimit = "";
        try {
            int cnt = strbuf.length();
            if (cnt > limit) {
                strLimit = strbuf.substring(0, limit) + " ";
            } else {
                strLimit = strbuf;
            }
            return strLimit;
        } catch (Exception e) {
            throw new Exception("Trouble limitFieldName to " + limit + " for String " + strbuf, e);
        }
    }


    /**
     * Removes all double quotes from the original input string and trims the results.
     * @param inp
     * @return
     */
    public static String stripQuotes(String inp) {
        return inp.replace("\"", "").trim();
    }

    /**
     * Removes all trailing commas. If whitespace between commas this will stop at whitespace currently.
     * @param in
     * @return
     */
    public static String stripTrailingCommas(String in) {
        while (in.endsWith(",")) {
            in = in.substring(0, in.length() - 1);
        }
        return in;
    }

    /**
     * Converts String in format nnn% to a range of 0-1
     * Treats null and empty as zero.
     *
     * @param percentToken
     * @return
     */
    public static double convertPercent(String percentToken) {
        if (percentToken == null) {
            return 0;
        }
        String val = percentToken.replace('%', ' ').trim();

        if (val.length() < 1) {
            return 0;
        }
        Double res = Double.valueOf(val) / 100.0;
        return res;
    }

    /**
     * Hmm, TODO: This is too specialized and should be removed I thnk.
     * @param fields
     * @param num
     * @return
     */
    public static String loadField(String[] fields, int num) {
        if (fields.length > num) {
            return stripQuotes(fields[num]);
        } else {
            return null;
        }
    }


    public static String stripTrailingColon(String in) {
        while (in.endsWith(":")) {
            in = in.substring(0, in.length() - 1);
        }
        return in;
    }

    public static String leftAlignRightPadTruncating(String val, int lenInChars) {
        val = val.trim();
        val = truncate(val, lenInChars);
        val = leftAlignRightPad(val, lenInChars);
        return val;
    }

	/**
	 *
	 * @param vals List of possibly null values.
	 * @return true if <em>ANY</em> of the values are not empty per isEmpty
	 */
	public static boolean areAllEmpty(String ... vals) {
		for (String s: vals) {
			if (!StringUtils.isEmpty(s)) {
				return false;
			}
		}
		return  true;
	}

    /**
     * Return concatenated string from given string list
     *
     * @param stringList
     * @return
     */
    public static String uniqueValueList(List<String> stringList){
        String outputString = "";
        for (String s: stringList){
            outputString = outputString+s;
        }
        return outputString;
    }

}
