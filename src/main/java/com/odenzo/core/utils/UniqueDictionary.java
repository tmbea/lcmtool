/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class maintains a dictionary of unique tokens, if you try and add a token that already exists it creates a
 * new token that is unique and returns that.
 * <p>Primarily used to make unique field names in Cobol copybooks.</p>
 * @version $Id: UniqueDictionary.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class UniqueDictionary {
	private final static Logger _log = LoggerFactory.getLogger(UniqueDictionary.class);

	/**
	 * If null allow arbitrary length field names, no padding or truncation.
	 */
	private Integer fieldLen;

	/**
	 * Key is unique field, Value is the original word.
	 */
	private HashMap<String, String> fieldToWord;

	/**
	 * All words are padded or converted (truncated) to this length, because TCBIS requires it.
	 *
	 * @param wordLength
	 */
	public UniqueDictionary(int wordLength) {
		this.fieldLen = wordLength;
		fieldToWord = new HashMap<String, String>();
	}


	/**
	 * Note this will trim all leading spaces off the word and make unique.
	 *
	 * @param word Non-Unique word to add to dictionary. A unique version will be created and added
	 * @return Unique word derived from input word and added to dictionary.
	 */
	public String addWord(String word) throws IllegalStateException {
		try {
			String value = word.trim();
			if (value.length() > fieldLen) {
				value = value.substring(0, fieldLen);
			}
			String field = StringUtils.leftAlignRightPad(value, fieldLen);
			while (fieldToWord.containsKey(field)) {

				field = cycleUniqueWord(field);
			}
			fieldToWord.put(field, value);
			return field;
		} catch (Exception e) {
			throw new IllegalStateException("Trouble Adding Word [" + word + "] to unique dictionary", e);
		}
	}


	/**
     * Adds trailing values to the original word by cycling the trailing value until the resulting token
     * is not present in the unique dictionary. Will replace spaces with tokens first.
	 * @param origWord An already left aligned, right space padded word of length exactly fieldLen.
	 * @return  Unqiue word (of same length as original?) that is not currently in the unique dictionary.
	 * @throws Exception
	 */
	public String cycleUniqueWord(String origWord) throws Exception {
		try {
			_log.debug("Cycle Unique Word [" + origWord + "] of Length: " + origWord.length());
			if (origWord.length() != fieldLen) {
				throw new Exception("Cycling Unique Word Requires Word of Length " + fieldLen + " not " + origWord.length());
			}

			String res = null;

			// Remove all trailing space and replace with 00000
			String word = origWord.trim();
			word = StringUtils.leftAlignRightZeroPad(word, fieldLen);
			_log.debug("Zero Padded Word [" + word + "] length " + word.length() + " to field length " + fieldLen);
			_log.debug("No Empty Spaces to Fill - So iterating the numeric part at the end or last char to 0");
			// Cycle the last character, converting to zero if not already a digit
			Character lastChar = word.charAt(word.length() - 1);
			_log.debug("Got Last Char: " + lastChar);
			if (!Character.isDigit(lastChar)) {
				res = word.substring(0, word.length() - 1) + '0';
				_log.debug("Converted Last Char To Digit: [" + word + "] --> [" + res + "]");
			} else {
				_log.debug("Last Character is digit, so actually can just regex to get series of last digits.");
				Pattern regex = Pattern.compile("(.*?)(\\d*)");
				Matcher m = regex.matcher(word);
				if (!m.matches()) {
					throw new Exception("Regular Expression " + regex.pattern() + " could not match [" + word + "]");
				}
				String base = m.group(1);
				String digits = m.group(2);
				_log.debug("Found Digits [" + digits + "]");
				int newDigits = Integer.parseInt(digits) + 1;
				// If digits is all 9 then need special case
				if (StringUtils.countOccurances(digits, '9') == digits.length()) {
					res = base.substring(0, base.length() - 1) + newDigits;
				} else {
					res = String.format("%s%0" + digits.length() + "d", base, newDigits);
				}
			}


			if (res.length() != fieldLen) {
				throw new Exception("Cycleing Unique word produced Result of Wrong Length: " + res.length() + " instead of "
						+ fieldLen + " Inp: [" + origWord + "] Out: [" + res + "]");
			}
			if (word.equals(res)) {
				throw new Exception("Cycling Unique Word Produced No Change [" + word + "]");
			}
			return res;
		} catch (Exception e) {
			throw new Exception("Trouble Cycleing Unique word", e);
		}
	}
}



