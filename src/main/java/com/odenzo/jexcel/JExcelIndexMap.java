/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.jexcel;

import com.odenzo.core.utils.ReflectionUtils;
import com.odenzo.core.utils.StringUtils;
import jxl.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Maps Excel column index to properties of a model object for more automated and simplified parsing.
 *
 * @author stevef
 * @version $Id: JExcelIndexMap.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class JExcelIndexMap<T> {
	private static Logger _log = LoggerFactory.getLogger(JExcelIndexMap.class);

    /**
     * Dangler, thinking of using prototypes and cloning rather than newInstance()
     */
	private T prototype;
	private Class clazz ;
	private HashMap<Integer, Method> methodMap = new HashMap<Integer,Method>();
	private HashMap<Integer, Class> typeMap = new HashMap<Integer,Class>();

    /**
     *
     * @param clazz The class name of the model object to which a row is mapped to. Must have default constructor.
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
	public JExcelIndexMap(Class clazz) throws IllegalAccessException, InstantiationException {
		this.clazz = clazz;
		prototype = (T) clazz.newInstance();
		// _log.info("T Class is: " + T.class);
	}

    /**
     *
     * @param columnNumber
     * @param setterMethod Setter method is method name as String. Setter has exactly one parameter.
     * @param paramClass The class of the setter parameters.
     * @throws NoSuchMethodException
     */
	public void addMap(Integer columnNumber, String setterMethod, Class paramClass) throws NoSuchMethodException {
		methodMap.put(columnNumber, ReflectionUtils.findSetterMethodForProperty(clazz, setterMethod, paramClass));
		typeMap.put(columnNumber, paramClass);
	}


    /**
     * THe Set of columns (by index) which are mapped to the model object.
     * @return  Set of index mapped to model object.
     */
	public Set<Integer> getColumnSet() {
		return methodMap.keySet();
	}



	/**
	 * This is designed to be overridden in case a series of Objects need to be created for each row.
     * This handles the simple case where one column is mapped to a setter and all columns to same model object.
	 * @param cells cells stored in a Map where key is column index.
	 * @return List of model objects, this returns only one item in list, subclasses or extensions may return more.
	 * @throws Exception
	 */
	public List<Object> parseRow(HashMap<Integer, Cell> cells) throws Exception {
		T res = this.createModelObject();
		for (Integer col : cells.keySet()) {
			setColumn(res, col, cells.get(col));
		}
		ArrayList<T> resList = new ArrayList<T>();
		resList.add(res);
		return (List<Object>) resList;
	}

    /**
     * Constructs an instance of the model class using the default constructor.
     * @return a new instance of the model class.
     * @throws Exception
     */
	public T createModelObject() throws Exception {
		return (T)clazz.newInstance();
	}

	/**
	 * Utility routine, that tries to extract the cell contents and apply via setter to model object.
     * TODO: Should be protected not public.
     * This will automatically try and cast, you can subclass and override this for custom casting.
	 *
     * @param model Model object which will have its setter called.
	 * @param col Column, should be the same as cell column (TODO: Remove me). Used to find setX(param) param class.
	 * @param cell
	 */
	public void setColumn(T model, Integer col, Cell cell) throws Exception {
		Class type = typeMap.get(col);
		String textVal = JExcelUtils.getCellContentTrim(cell);
		Object convertedVal = null;
		if (StringUtils.isEmpty(textVal)) {
			return;
		}
		if (type == String.class) {
			convertedVal =textVal;
		} else	if (type == Integer.class)   {
			convertedVal= Integer.parseInt(textVal);
		} else if (type == Character.class) {
			if (textVal.length() > 1) {
				throw new Exception("Character type too short for String ["+textVal+"]");
			}
			convertedVal =  (Character)textVal.charAt(0);
		} else {
			throw new Exception("Unsupport Type " + type);
		}
		if (convertedVal != null) {
			setColumn(model, col, convertedVal);
		}
	}

    /**
     * Another setColumn, that is used by the first public setColumn. TODO: Needs cleanup
     * @param model
     * @param colIndex
     * @param val
     * @throws java.lang.reflect.InvocationTargetException
     * @throws IllegalAccessException
     */
	protected void setColumn(Object model, Integer colIndex, Object val) throws InvocationTargetException, IllegalAccessException {
		Method method = methodMap.get(colIndex);
		if (method!= null) {
			method.invoke(model, val);
		}
	}

}
