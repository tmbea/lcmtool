/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.jexcel;


import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.format.CellFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * General Excel parser framework code, not currently in use.
 * @see com.odenzo.jexcel.JExcelIndexMap which is used in conjuction with this class. (Header Map missing now?)
 *
 * Source Code Version: $Id: JExcelParserByIndex.java 211 2012-05-19 11:27:18Z e1040775 $
 */
public class JExcelParserByIndex<T extends Object> {
	private final static Logger _log = LoggerFactory.getLogger(JExcelParserByIndex.class);

	private Sheet sheet;
	private int headerRow = 0;
	private JExcelIndexMap<T> columnMapping;
	private Set<Integer> columns;

	public JExcelParserByIndex(Sheet sheet, JExcelIndexMap<T> map) {
		this.sheet = sheet;
		this.columnMapping = map;
		this.columns = map.getColumnSet();
	}

	/**
	 * Set the row that the column headers are on. Expects headers on this row, and parses data below this row.
	 *
	 * @param index
	 */
	public void setHeaderRowIndex(int index) {
		headerRow = 0;
	}

	public int getFirstDataRow() {
		return headerRow + 1;
	}

	public int getNumberOfRows() {
		return sheet.getRows();
	}

	/**
	 * Parses the expected row range, creating value object instances for each row (if possible) and adding to collection.
	 *
	 * @return A collection of Entity objects that need to be in database. These may already be in, so need to add or update.
	 * @throws Exception
	 * @throws IllegalAccessException
	 */
	public Collection<Object> parse() throws Exception {
		try {
			_log.info("Parsing Total Number of Rows: " + getNumberOfRows());
			List<Object> res = new ArrayList<Object>(getNumberOfRows());
			for (int i = headerRow + 1; i < getNumberOfRows(); i++) {
				try {
					_log.warn("Parsing Row " + i);
					List<Object> resultObjs = parseRowUsingIndexMap(i);
					if (resultObjs != null) {
						res.addAll(resultObjs);
					}
				} catch (Exception e) {
					throw new Exception("Trouble Parsing Row " + i, e);
				}
			}
			return res;
		} catch (Exception e) {
			throw new Exception("Trouble Parsing", e);
		}
	}


	/**
	 * Parses a row from excel and sets value object based on column index to field mapping.
	 *
	 * @param row
	 * @return
	 * @throws Exception
	 * @throws IllegalAccessException
	 */
	private List<Object> parseRowUsingIndexMap(int row) throws Exception, IllegalAccessException {
		try {
			Cell[] cells = sheet.getRow(row);
			_log.info("Got Cells for Row " + row + " total of "+ cells.length);


			HashMap<Integer, Cell> relevantCells = new HashMap<Integer,Cell>();
			_log.info("RelevalColumns: " +columns);
			for (Integer cellIndx : columns) {
				Cell cell = cells[cellIndx-1];
				CellType type = cell.getType();
				CellFormat format = cell.getCellFormat();
				String cellValue =  JExcelUtils.cleanCellContents(cell);
				Class cellClass = cell.getClass();
				_log.warn("Cell Column " + cellIndx + " type " + type + " format " + format + " class  " + cellClass + " Vaule: " + cellValue);
				relevantCells.put(cellIndx, cell);
			}
			List<Object> modelObjects = (List<Object>) columnMapping.parseRow(relevantCells);
			return modelObjects;

		} catch (Exception e) {
			throw new Exception("Trouble Parsing Row " + row + " Using Index Map", e);
		}
	}





}
