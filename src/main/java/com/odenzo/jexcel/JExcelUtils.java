/*
 * Copyright (c) 2009. adenzo software
 */

package com.odenzo.jexcel;


import com.odenzo.core.datetime.DateUtils;
import com.odenzo.core.utils.StringUtils;
import jxl.*;
import jxl.format.PageOrientation;
import jxl.write.*;
import jxl.write.Number;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * A collection of helper utilities to use with Excel spreadsheets and the JXL library.
 * Note that JXL only does .xls files now, not .xlsx
 * $Id: JExcelUtils.java 263 2012-05-26 08:02:59Z e1040775 $
 */


public class JExcelUtils {
    private final static Logger _log = LoggerFactory.getLogger(JExcelUtils.class);

    /**
     * Treats the name as a Base26 "number", this column label "AA" is converted to its column index value
     *
     * @param name, Like A, B, AB etc...
     * @return A=0, B= 1, AA=27 ...
     */
    public static int convertColumnNameToIndex(String name) throws Exception {
        String b26 = name.trim().toUpperCase();

        if (b26.length() == 1) {
            return b26.charAt(0) - 'A';  // Hmm, what is definition of char math !
        } else if (b26.length() == 2) {
            return   ((b26.charAt(0) - 'A' + 1) * 26)
                            + (b26.charAt(1) - 'A');
        } else {
            throw new Exception("Can only convert 2 character Excel column names to index: " + name);
        }

    }

    /**
     * An ill advised routine that sets the Excel view items to what I personally like.
     *
     * @param sheet The excel worksheet to apply the settings to.
     */
    public static void applyStandardSheetSettings(WritableSheet sheet) {
        SheetSettings setting = sheet.getSettings();

        setting.setOrientation(PageOrientation.LANDSCAPE);   // Most sheets have many columns, set printing to landscape
        setting.setScaleFactor(120);          // Zoom to 120%
        //setting.setNormalMagnification(120); // Zoom to 120%

        autosizeColumns(sheet);        // Resize columns to fit their largest member.
        setting.setVerticalFreeze(1);  // Freeze the top header row

        // Note: Automatically turning on Data Filter seem to break the spreadsheet 1/2 the time.

    }

    /**
     * Usually I have headings in row 1 and columns down. This routine is for when I tried to use side headings
     * in column A. It didn't work too well. Maybe JXL bug that maybe fixed next time I try to use it.
     *
     * @param sheet
     */
    public static void applyStandardSideHeadingSheetSettings(WritableSheet sheet) {
        SheetSettings setting = sheet.getSettings();

        setting.setOrientation(PageOrientation.LANDSCAPE);
        setting.setScaleFactor(120);
        setting.setNormalMagnification(120);
        // autosizeColumns(sheet);
        //setting.setHorizontalFreeze(1);


    }

    /**
     * Generic (and slow) way to get the text content of a cell, all trimmed.
     *
     * @param sheet
     * @param col
     * @param row
     * @return Always the text representation of a cell, even if Date format etc.
     */
    public static String getCellContentsTrim(Sheet sheet, int col, int row) {
        Cell c = sheet.getCell(col, row);

        String content = getCellContentTrim(c);
        _log.info("Cell " + row + " , " + col + " is " + c + " with content " + content);
        return content;
    }

    /**
     * Given an individual cell, return its content or null if cell is empty or doesn't exist.
     *
     * @param c
     * @return null if cell is null or empty otherwise trimmed content as text.
     */
    public static String getCellContentTrim(Cell c) {
        if (c == null) {
            return null;
        }
        String val = c.getContents();
        if (val == null) {
            return null;
        }
        val = val.trim();
        return val;
    }

    /**
     * Sets each column in sheet to be autosized, such that its width is the maximum of its contents.
     * No mimimum or margin is set currently.
     *
     * @param sheet
     */
    public static void autosizeColumns(WritableSheet sheet) {
        for (int colIndx = 0; colIndx < sheet.getColumns(); colIndx++) {
            CellView colView = sheet.getColumnView(colIndx);
            colView.setAutosize(true); // This seems to have no effect on Mac Excel
            sheet.setColumnView(colIndx, colView);
        }
    }

    /**
     * Add the sorter stuff to top row of sheet. This has unpredictable results. and has been nuetralized.
     *
     * @param sheet
     */
    public static void enableDataAutoFilter(WritableSheet sheet) {
        CellView rowView = sheet.getRowView(1);

    }

    /**
     * Hide a column  in the given sheet.
     *
     * @param sheet
     * @param colIndx
     */
    public static void hideColumn(WritableSheet sheet, int colIndx) {
        CellView colView = sheet.getColumnView(colIndx);
        colView.setHidden(true);
        sheet.setColumnView(colIndx, colView);
    }

    /**
     * Just adds the Cells to a Sheet.
     *
     * @param cells A Collection of WritableCells, each of which has their own row,col values.
     * @param sheet
     * @throws WriteException
     */
    public static void addCellsToSheet(Collection<WritableCell> cells, WritableSheet sheet) throws WriteException {
        for (WritableCell c : cells) {
            sheet.addCell(c);
        }
    }

    /**
     * Various ways of formatting percentages in a cell. This is munger that can be used to get percent
     * from a cell in case I need to normalize based on the actual cell format later.
     *
     * @param cell Cell to extract the percentage value from.
     * @return Value between 0 and 1 to represent actual percentage. May return null is cell is empty.
     * @throws Exception Any error.
     */
    public static double extractPercentFromCell(Cell cell) throws Exception {
        try {
            String hlrdPercentText = cell.getContents();
            double hlrdPercent = 0;
            if (hlrdPercentText != null && hlrdPercentText.length() > 1) {
                hlrdPercentText = hlrdPercentText.replace("%", "").trim();
                hlrdPercent = Double.parseDouble(hlrdPercentText);
                hlrdPercent = hlrdPercent / 100.0;
            }
            return hlrdPercent;
        } catch (Exception e) {
            throw new Exception("Trouble Parsing Cell " + cell, e);
        }
    }

    /**
     * Efficient means of extacting cell contents (as text) from a row of cells
     *
     * @param rowData  Row of cells, via getRow(..) normally.
     * @param colIndex Column index to extract.
     * @return null if column doesn't exist or Cell contents is empty or blank.
     */
    public static String extract(Cell[] rowData, int colIndex) throws Exception {
        try {
            if (rowData.length <= colIndex) {
                return null;
            }
            Cell cell = rowData[colIndex];
            String trimmed = cell.getContents().trim();
            if (trimmed.length() < 1) {
                return null;
            }
            // May need to do some cleanup here as this is happening but not sure why.
            return trimmed;
        } catch (ArrayIndexOutOfBoundsException boundsErr) {
            throw new Exception("Extracting " + colIndex + " from rowData of size " + rowData.length, boundsErr);
        }
    }

    /**
     * Extract cell contents (as text) from a row of cells
     *
     * @param rowData Row of cells, via getRow(..) normally.
     * @param colName Column name is like A, B, C, ...AA, AB etc. Not the column header content.
     * @return null if column doesn't exist or Cell contents is empty or blank.
     */
    public static String extract(Cell[] rowData, String colName) throws Exception {
        int index = JExcelUtils.convertColumnNameToIndex(colName);
        return extract(rowData, index);
    }

    /**
     * Does a top-down left-right search for Cell with given content value.
     * Starts in first column and goes down column, then second column etc..
     * Note that it checks if contentValue is contained in the cell, not equal exactly to cell contents.
     * TODO: Should probably take in a regular expression in a different version of this routine for flixebility at speed cost.
     *
     * @param sheet        Sheet to search for value in.
     * @param contentValue Checks if the trimmed cell data (as String) <b>contains</b> this value. Not equals.
     * @return The cell if found else null.
     */
    static public Cell findFirstCellWithContentValue(Sheet sheet, String contentValue) {
        int numCols = sheet.getColumns();
        int numRows = sheet.getRows();
        for (int c = 0; c < numCols; c++) {
            for (int r = 0; r < numRows; r++) {
                Cell cell = sheet.getCell(c, r);
                if (cell != null) {
                    String val = cell.getContents();
                    if ((val != null) && (val.trim().contains(contentValue))) {
                        return cell;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Counts the number of cells that are not empty or null. Will skip over empty cells.
     *
     * @param cells Cells, typically representing a row in the sheet.
     * @return Number of non-empty cells.
     */
    public static int countNonEmptyCells(Cell[] cells) {
        if (cells == null) {
            return 0;
        }
        int count = 0;
        for (Cell c : cells) {
            if (c != null && !StringUtils.isEmpty(c.getContents())) {
                count++;
            }
        }
        return count;
    }

    /**
     * Finds the last cell with data (content) in a column by searching for the first empty or null cell.
     *
     * @param sheet Worksheet to check
     * @param col   Column which is traversed vertically until the FIRST empty cell is found.
     * @return
     */
    public static Cell findLastFilledCellInColumn(Sheet sheet, int col) {
        Cell last = null;
        for (int row = 0; row < sheet.getRows(); row++) {
            Cell cell = sheet.getCell(col, row);
            if (cell != null && !StringUtils.isEmpty(cell.getContents())) {
                last = cell;
            }
        }
        return last;
    }


    /**
     * Silly utility to add a label value pair horizontally starting at the specified row, col
     *
     * @param sheet
     * @param row
     * @param col
     * @param label
     * @param value
     * @throws WriteException
     */
    public static void addLabelValuePair(WritableSheet sheet, int row, int col, String label, String value) throws WriteException {
        sheet.addCell(new Label(col++, row, label));
        sheet.addCell(new Label(col, row, value));
    }


    /**
     * Extracts Gregorian date from Cell, works well if formatted as Date otherwise tries to do a free form parse of text.
     * Not so smart when it is a formula (not tested, confirm this).
     *
     * @param dateCell
     * @return Date value in the cell or null if cannot parse it (or ambiguous)
     */
    public static Calendar extractDateFromCell(Cell dateCell) {
        Calendar cal = Calendar.getInstance();
        CellType cellType = dateCell.getType();
        _log.debug("Cell Type; " + cellType);
        if (dateCell instanceof DateCell) {
            DateCell dc = (DateCell) dateCell;
            Date lastRevDate = dc.getDate();
            _log.info("Last Rev Date: " + lastRevDate);
            cal.setTime(lastRevDate);
            return cal;
        }

        if (CellType.DATE_FORMULA == cellType) {
            _log.warn("How To Handle Date Formula? " + dateCell.getClass());
            return cal;
        }
        _log.warn("Contents: " + dateCell.getContents() + " and class " + dateCell.getClass());
        _log.warn("Excel Type: {} and Format: {}", dateCell.getType(), dateCell.getCellFormat());
        // Well, if all else fails, we take as a String and pass to our Swiss Army name data parser.
        String val = dateCell.getContents().trim();
        return DateUtils.parseFreeFormDate(val);

    }

    /**
     * Tidies up the contents of a cell and returns it. Blanks to null. Funny characters removed.
     *
     * @param rawData
     * @return Tidied up cell contents or null if no data or blank.
     */
    static String cleanCellContents(Cell rawData) {
        if (rawData == null) {
            _log.warn("Cell was NULL, this never happens");
            return null; // "[Null Excel Cell Object]";
        }

        final String data = rawData.getContents();
        if (data == null) {
            return null; // "[NULL Cell Contents]"; // Again, this never seems to happen with JExcel version.
        }
        // Excel reports have a lot of 65533 characters (Hex FFFD unicode replacement)
        String trimmed = data.replace("\uFFFD", "");
        trimmed = trimmed.trim();
        if (trimmed.length() < 1) {
            return null;
        }
        // May need to do some cleanup here
        return trimmed;
    }

    /**
     * Checks to make sure at least 1 non empty cell.
     *
     * @param cells a row of cells to be tested for content.
     * @return True to treat as an empty row (and not parse)
     */
    public static boolean emptyRowTest(Cell[] cells) {
        return cells.length < 1 || countNonEmptyCells(cells) < 1;
    }


    /**
     * Utility routine to load an excel file, must be .xls format.
     *
     * @param f File pointing to .xls style Excel file.
     * @return Readable workbook.
     * @throws Exception
     */
    public static Workbook loadExcelFile(File f) throws Exception {
        try {
            Workbook workbook = Workbook.getWorkbook(f);
            return workbook;
        } catch (Exception e) {
            throw new Exception("Could not load workbook from file " + f, e);
        }
    }

    /**
     * This returned a list with list index equal to column index and contents the header for the column.
     * Note that it does not ensure the headers are unique and it stops at first empty header cell.
     *
     * @param headerRow THe row in which to look for headers to extract
     * @param sheet     THe worksheet to extract from.
     * @return List of trimmed strings for each header cell until an empty header is found.
     */
    public static List<String> extractHeaderNames(int headerRow, Sheet sheet) {
        Cell[] row = sheet.getRow(headerRow);
        ArrayList<String> headers = new ArrayList<String>();
        for (Cell c : row) {
            String hdr = JExcelUtils.cleanCellContents(c);
            if (StringUtils.isEmpty(hdr)) {
                _log.info("Stopping Header Parsing at " + headers.size() + " out of " + row.length + " header cells");
                return headers;
            }
            headers.add(hdr);
        }
        return headers;
    }

    /**
     * Writes a series of Strings as headers into the given sheet. Maybe should be able to pass in a style too.
     *
     * @param headers
     * @param headerRow
     * @param sheet
     * @throws Exception
     */
    public static void addHeaders(Collection<String> headers, int headerRow, WritableSheet sheet) throws Exception {

        int row = headerRow;
        int col = 0;

        WritableCellFormat headerFormat = new StandardExcelFormats().getHeader();
        for (String header : headers) {
            WritableCell cell = new Label(col, row, header);
            cell.setCellFormat(headerFormat);
            sheet.addCell(cell);
            col++;
        }
    }

    /**
     * Adds a number cell according to the basic stylings for number cells set in StandardFormats instance.
     *
     * @param sheet
     * @param i
     * @param row
     * @param appId
     * @param formats
     */
    public static void addNumberCell(WritableSheet sheet, int col, int row, long val, StandardExcelFormats formats) throws WriteException {
        final Number cell = new Number(col, row, val);
        cell.setCellFormat(formats.getInteger());
        sheet.addCell(cell);
    }

    /**
     * Adds a number cell according to the basic stylings for number cells set in StandardFormats instance.
     *
     * @param sheet
     * @param i
     * @param row
     * @param appId
     * @param formats
     */
    public static void addNumberCell(WritableSheet sheet, int col, int row, double val, StandardExcelFormats formats) throws WriteException {
        final Number cell = new Number(col, row, val);
        //cell.setCellFormat(formats.get`Integer());
        sheet.addCell(cell);
    }

    /**
     * @param sheet
     * @param col
     * @param row
     * @param label
     * @param formats
     * @throws WriteException
     */
    public static void addLabelCell(WritableSheet sheet, int col, int row, String label, StandardExcelFormats formats) throws WriteException {
        sheet.addCell(new Label(col, row, label, formats.getText()));
    }


    public static WritableWorkbook createWorkbook(File file) throws IOException {
        return Workbook.createWorkbook (file);

    }
}
