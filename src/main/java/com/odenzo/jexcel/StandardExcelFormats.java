/*
 * Copyright (c) 2012. odenzo software under the Apache License
 */

package com.odenzo.jexcel;

import jxl.biff.DisplayFormat;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 **
 * @version $Id: StandardExcelFormats.java 218 2012-05-20 07:33:19Z e1040775 $
 * A bunch of JExcel cell formats for use when writing Excel spreadsheets.
 * Instances of formats should only be used in one Worksheet.
 * Do not re-use one instasnce of this class across worksheets.
 */
public class StandardExcelFormats {
	private final static Logger _log = LoggerFactory.getLogger(StandardExcelFormats.class);
	private WritableCellFormat header;
	private WritableCellFormat text;
	private WritableCellFormat integer;
	private WritableCellFormat percent;

	private WritableCellFormat textWarn;
	private WritableCellFormat textError;
	private WritableCellFormat sideHeader;
    private WritableCellFormat yesNo;


    public StandardExcelFormats() throws WriteException {
		init();
	}

	private void init() throws WriteException {
		WritableFont headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
		header = new WritableCellFormat(headerFont);

		header.setAlignment(Alignment.CENTRE);
		header.setBackground(Colour.GREY_25_PERCENT);

		sideHeader = new WritableCellFormat(headerFont);
		sideHeader.setAlignment(Alignment.RIGHT);

		WritableFont bodyFont = new WritableFont(WritableFont.ARIAL, 10);
		text = new WritableCellFormat(bodyFont);


        yesNo = new WritableCellFormat(bodyFont);
		integer = new WritableCellFormat(bodyFont, NumberFormats.INTEGER);
		percent = new WritableCellFormat(bodyFont, NumberFormats.PERCENT_INTEGER);

		textWarn = addWarningDecoration(text);
		textError = addErrorDecoration(text);
	}

    public WritableCellFormat getYesNo() {
        return yesNo;
    }

    public WritableCellFormat getSideHeader() {
		return sideHeader;
	}

	public void setSideHeader(WritableCellFormat sideHeader) {
		this.sideHeader = sideHeader;
	}

	public WritableCellFormat getTextWarn() {
		return textWarn;
	}

	public void setTextWarn(WritableCellFormat textWarn) {
		this.textWarn = textWarn;
	}

	public WritableCellFormat getTextError() {
		return textError;
	}

	public void setTextError(WritableCellFormat textError) {
		this.textError = textError;
	}

	public WritableCellFormat getHeader() {
		return header;
	}

	public void setHeader(WritableCellFormat header) {
		this.header = header;
	}

	public WritableCellFormat getText() {
		return text;
	}

	public void setText(WritableCellFormat text) {
		this.text = text;
	}

	public WritableCellFormat getInteger() {
		return integer;
	}

	public void setInteger(WritableCellFormat integer) {
		this.integer = integer;
	}

	public WritableCellFormat getPercent() {
		return percent;
	}

	public void setPercent(WritableCellFormat percent) {
		this.percent = percent;
	}

	public WritableCellFormat addWarningDecoration(WritableCellFormat format) throws WriteException {
		WritableCellFormat res = new WritableCellFormat(format);
		res.setBackground(Colour.LIGHT_ORANGE);
		return res;
	}

	public WritableCellFormat addErrorDecoration(WritableCellFormat format) throws WriteException {
		WritableCellFormat res = new WritableCellFormat(format);
		res.setBackground(Colour.GOLD);
		return res;
	}
}
