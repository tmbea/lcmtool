package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 1/31/14
 * Time: 11:00 PM
 * To change this template use File | Settings | File Templates.
 */


@Entity
@Table(name="APP_PLATFORM_VERSION")
public class ApplicationPlatformVersion extends TracableEntity {
    @TableGenerator(name = "APP_PLATFORM_VERSION")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    protected String appId;

    protected String platform;

    protected String version;

    protected String platformDeploymentPortfolioKey;



    protected String hostName;


    protected String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPlatformDeploymentPortfolioKey() {
        return platformDeploymentPortfolioKey;
    }

    public void setPlatformDeploymentPortfolioKey(String platformDeploymentPortfolioKey) {
        this.platformDeploymentPortfolioKey = platformDeploymentPortfolioKey;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
