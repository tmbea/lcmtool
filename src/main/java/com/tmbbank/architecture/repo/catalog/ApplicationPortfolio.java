package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.lang.model.element.Name;
import javax.persistence.*;


/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="APP_PORTFOLIO")
public class ApplicationPortfolio extends TracableEntity {
    @TableGenerator (name = "APP_PORTFOLIO")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    /*

    Application ID (ISA)
No
Plat form

Application System
Application System (Name from ISA)
BU/SU Group
Responsible By
Vendor name or       In-house
Product Name
TMB Installed Version
Currently on MA Yes/No
End of Sale Date
End of Support Date
GA Version
Next Release Date
Implementation Date MM-YY
Run on What Type of HW
Database Type
"Source Code Avail
Y or N"
Languages used
Operational Yes / No
Product Description
Initial Project Date
Comments

     */
    String appId;
    String number;
    String platform;


    String appName;
    String appNameCORM;
    String buSuGroup;
    String responsibleBy;
    String vendorName;
    String productName;
    String tmbInstalledVersion;
    String currentlyOnMA;
    String endOfSaleDate;
    String endOfSupportDate;
    String gaVersion;
    String nextReleaseDate;
    String implementationDateMMYY;
    String runOnWhatTypeOfHW;
    String databaseType;
    String sourceCodeAvailability;
    String languageUsed;
    String operational;
    String productDescription;
    String initialProjectDate;



    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppNameCORM() {
        return appNameCORM;
    }

    public void setAppNameCORM(String appNameCORM) {
        this.appNameCORM = appNameCORM;
    }

    public String getBuSuGroup() {
        return buSuGroup;
    }

    public void setBuSuGroup(String buSuGroup) {
        this.buSuGroup = buSuGroup;
    }

    public String getResponsibleBy() {
        return responsibleBy;
    }

    public void setResponsibleBy(String responsibleBy) {
        this.responsibleBy = responsibleBy;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTmbInstalledVersion() {
        return tmbInstalledVersion;
    }

    public void setTmbInstalledVersion(String tmbInstalledVersion) {
        this.tmbInstalledVersion = tmbInstalledVersion;
    }

    public String getCurrentlyOnMA() {
        return currentlyOnMA;
    }

    public void setCurrentlyOnMA(String currentlyOnMA) {
        this.currentlyOnMA = currentlyOnMA;
    }

    public String getEndOfSaleDate() {
        return endOfSaleDate;
    }

    public void setEndOfSaleDate(String endOfSaleDate) {
        this.endOfSaleDate = endOfSaleDate;
    }

    public String getEndOfSupportDate() {
        return endOfSupportDate;
    }

    public void setEndOfSupportDate(String endOfSupportDate) {
        this.endOfSupportDate = endOfSupportDate;
    }

    public String getGaVersion() {
        return gaVersion;
    }

    public void setGaVersion(String gaVersion) {
        this.gaVersion = gaVersion;
    }

    public String getNextReleaseDate() {
        return nextReleaseDate;
    }

    public void setNextReleaseDate(String nextReleaseDate) {
        this.nextReleaseDate = nextReleaseDate;
    }

    public String getImplementationDateMMYY() {
        return implementationDateMMYY;
    }

    public void setImplementationDateMMYY(String implementationDateMMYY) {
        this.implementationDateMMYY = implementationDateMMYY;
    }

    public String getRunOnWhatTypeOfHW() {
        return runOnWhatTypeOfHW;
    }

    public void setRunOnWhatTypeOfHW(String runOnWhatTypeOfHW) {
        this.runOnWhatTypeOfHW = runOnWhatTypeOfHW;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }

    public String getSourceCodeAvailability() {
        return sourceCodeAvailability;
    }

    public void setSourceCodeAvailability(String sourceCodeAvailability) {
        this.sourceCodeAvailability = sourceCodeAvailability;
    }

    public String getLanguageUsed() {
        return languageUsed;
    }

    public void setLanguageUsed(String languageUsed) {
        this.languageUsed = languageUsed;
    }

    public String getOperational() {
        return operational;
    }

    public void setOperational(String operational) {
        this.operational = operational;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getInitialProjectDate() {
        return initialProjectDate;
    }

    public void setInitialProjectDate(String initialProjectDate) {
        this.initialProjectDate = initialProjectDate;
    }


}
