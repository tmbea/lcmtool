package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 1/31/14
 * Time: 10:27 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="CANONICAL_APPLICATION")
public class CanonicalApplication extends TracableEntity {
    @TableGenerator (name = "CANONICAL_APPLICATION")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    protected String appName;
    protected String appId;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
