package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/3/14
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="CANONICAL_SOFTWARE")
public class CanonicalSoftware  extends TracableEntity {
    @TableGenerator(name = "CANONICAL_APPLICATION")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;


    protected String platform;
    protected String version;

    protected String canonicalSoftwareName;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCanonicalSoftwareName() {
        return canonicalSoftwareName;
    }

    public void setCanonicalSoftwareName(String canonicalSoftwareName) {
        this.canonicalSoftwareName = canonicalSoftwareName;
    }
}
