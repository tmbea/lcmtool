package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="APP_PORTFOLIO_ISA")
public class ISAApplicationPortfolio extends TracableEntity {
    @TableGenerator(name = "APP_PORTFOLIO")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;
    /*
ISAApplicationPortfolio is the list of applications maintained by
    CORM department. The list is the master list and contains cannonical
    name of applications. CORM will be the department who generate the application
    id and assigned to new application when requested.

App. ID
Application Name
C
I
A
Criticality
Application Description
System Owner
Application Type
Application Support Contact
Application Support Contact Department
ITI Support contact
ITI Support Contact Department
ITO Support contact
ITO Support Contact Department
"Public
 Facing"
"ITG
Supported"
User Management Department
Chief
Application Staus
Remark


     */


    String appId;
    String appName;
    String cRating;
    String iRating;
    String aRating;

    String criticality;
    String applicationDescription;
    String systemOwner;


    String applicationType;
    String applicationSupportContact;

    String applicationSupportContactDepartment;
    String itiSupportcontact;
    String itiSupportContactDepartment;
    String itoSupportContact;
    String itoSupportContactDepartment;
    String publicFacing;
    String itgSupported;
    String userManagementDepartment;
    String chief;
    String applicationStatus;
    String remark;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getcRating() {
        return cRating;
    }

    public void setcRating(String cRating) {
        this.cRating = cRating;
    }

    public String getiRating() {
        return iRating;
    }

    public void setiRating(String iRating) {
        this.iRating = iRating;
    }

    public String getaRating() {
        return aRating;
    }

    public void setaRating(String aRating) {
        this.aRating = aRating;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getApplicationDescription() {
        return applicationDescription;
    }

    public void setApplicationDescription(String applicationDescription) {
        this.applicationDescription = applicationDescription;
    }

    public String getSystemOwner() {
        return systemOwner;
    }

    public void setSystemOwner(String systemOwner) {
        this.systemOwner = systemOwner;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getApplicationSupportContact() {
        return applicationSupportContact;
    }

    public void setApplicationSupportContact(String applicationSupportContact) {
        this.applicationSupportContact = applicationSupportContact;
    }

    public String getApplicationSupportContactDepartment() {
        return applicationSupportContactDepartment;
    }

    public void setApplicationSupportContactDepartment(String applicationSupportContactDepartment) {
        this.applicationSupportContactDepartment = applicationSupportContactDepartment;
    }

    public String getItiSupportcontact() {
        return itiSupportcontact;
    }

    public void setItiSupportcontact(String itiSupportcontact) {
        this.itiSupportcontact = itiSupportcontact;
    }

    public String getItiSupportContactDepartment() {
        return itiSupportContactDepartment;
    }

    public void setItiSupportContactDepartment(String itiSupportContactDepartment) {
        this.itiSupportContactDepartment = itiSupportContactDepartment;
    }

    public String getItoSupportContact() {
        return itoSupportContact;
    }

    public void setItoSupportContact(String itoSupportContact) {
        this.itoSupportContact = itoSupportContact;
    }

    public String getItoSupportContactDepartment() {
        return itoSupportContactDepartment;
    }

    public void setItoSupportContactDepartment(String itoSupportContactDepartment) {
        this.itoSupportContactDepartment = itoSupportContactDepartment;
    }

    public String getPublicFacing() {
        return publicFacing;
    }

    public void setPublicFacing(String publicFacing) {
        this.publicFacing = publicFacing;
    }

    public String getItgSupported() {
        return itgSupported;
    }

    public void setItgSupported(String itgSupported) {
        this.itgSupported = itgSupported;
    }

    public String getUserManagementDepartment() {
        return userManagementDepartment;
    }

    public void setUserManagementDepartment(String userManagementDepartment) {
        this.userManagementDepartment = userManagementDepartment;
    }

    public String getChief() {
        return chief;
    }

    public void setChief(String chief) {
        this.chief = chief;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
