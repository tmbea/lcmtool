package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class LifecycleSupport extends TracableEntity {

    String productsReleased;
    String lifecycleStartDate;
    String mainstreamSupportEndDate;
    String extendedSupportEndDate;
    String notes;

    public String getProductsReleased() {
        return productsReleased;
    }

    public void setProductsReleased(String productsReleased) {
        this.productsReleased = productsReleased;
    }

    public String getLifecycleStartDate() {
        return lifecycleStartDate;
    }

    public void setLifecycleStartDate(String lifecycleStartDate) {
        this.lifecycleStartDate = lifecycleStartDate;
    }

    public String getMainstreamSupportEndDate() {
        return mainstreamSupportEndDate;
    }

    public void setMainstreamSupportEndDate(String mainstreamSupportEndDate) {
        this.mainstreamSupportEndDate = mainstreamSupportEndDate;
    }

    public String getExtendedSupportEndDate() {
        return extendedSupportEndDate;
    }

    public void setExtendedSupportEndDate(String extendedSupportEndDate) {
        this.extendedSupportEndDate = extendedSupportEndDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
