package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 10:08 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="PLATFORM_DEPLOYMENT_PORTFOLIO")
public class PlatformDeploymentPortfolio extends TracableEntity {
    @TableGenerator(name = "PLATFORM_DEPLOYMENT_PORTFOLIO")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;
    /* Platform deployment portfolio contains information of
    platforms and their physical deployment (servers) with the following attributes


    App Id
    No.
    Application No.
    No.App
    App Name
    Application Tier
    Application System
    Server / Computer / Host Name
    Location Site
    Environment
    Brand / Model Type
    IP Address
    Operating System


    Type of Servers
    Service Role
    All System Software List


    System Owner
    Internet Access
    LDAP Connect
    SAN Usage
    SAN Replication
    Backup Agent
    Monitoring Agent
    URL
    Remark

    */

    String appId;
    String Number;
    String applicationNumber;
    String numberApp;
    String appName;
    String applicationTier;
    String applicationSystem;
    String serverComputerHostName;
    String locationSite;
    String environment;
    String brandModelType;
    String ipAddress;
    String operatingSystem;
    String operatingSystemVersion;


    String typeOfServers;
    String serviceRole;
    String systemSoftwareList;

    String systemSoftwareVersion;


    String systemOwner;
    String internetAccess;
    String ldapConnect;
    String sanUsage;
    String sanReplication;
    String backupAgent;
    String monitoringAgent;
    String url;
    String remark;
    String infraGroup;

    boolean isDeleted;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getNumberApp() {
        return numberApp;
    }

    public void setNumberApp(String numberApp) {
        this.numberApp = numberApp;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getApplicationTier() {
        return applicationTier;
    }

    public void setApplicationTier(String applicationTier) {
        this.applicationTier = applicationTier;
    }

    public String getApplicationSystem() {
        return applicationSystem;
    }

    public void setApplicationSystem(String applicationSystem) {
        this.applicationSystem = applicationSystem;
    }

    public String getServerComputerHostName() {
        return serverComputerHostName;
    }

    public void setServerComputerHostName(String serverComputerHostName) {
        this.serverComputerHostName = serverComputerHostName;
    }

    public String getLocationSite() {
        return locationSite;
    }

    public void setLocationSite(String locationSite) {
        this.locationSite = locationSite;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getBrandModelType() {
        return brandModelType;
    }

    public void setBrandModelType(String brandModelType) {
        this.brandModelType = brandModelType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getTypeOfServers() {
        return typeOfServers;
    }

    public void setTypeOfServers(String typeOfServers) {
        this.typeOfServers = typeOfServers;
    }

    public String getServiceRole() {
        return serviceRole;
    }

    public void setServiceRole(String serviceRole) {
        this.serviceRole = serviceRole;
    }

    public String getSystemSoftwareList() {
        return systemSoftwareList;
    }

    public void setSystemSoftwareList(String systemSoftwareList) {
        this.systemSoftwareList = systemSoftwareList;
    }

    public String getSystemOwner() {
        return systemOwner;
    }

    public void setSystemOwner(String systemOwner) {
        this.systemOwner = systemOwner;
    }

    public String getInternetAccess() {
        return internetAccess;
    }

    public void setInternetAccess(String internetAccess) {
        this.internetAccess = internetAccess;
    }

    public String getLdapConnect() {
        return ldapConnect;
    }

    public void setLdapConnect(String ldapConnect) {
        this.ldapConnect = ldapConnect;
    }

    public String getSanUsage() {
        return sanUsage;
    }

    public void setSanUsage(String sanUsage) {
        this.sanUsage = sanUsage;
    }

    public String getSanReplication() {
        return sanReplication;
    }

    public void setSanReplication(String sanReplication) {
        this.sanReplication = sanReplication;
    }

    public String getBackupAgent() {
        return backupAgent;
    }

    public void setBackupAgent(String backupAgent) {
        this.backupAgent = backupAgent;
    }

    public String getMonitoringAgent() {
        return monitoringAgent;
    }

    public void setMonitoringAgent(String monitoringAgent) {
        this.monitoringAgent = monitoringAgent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSystemSoftwareVersion() {
        return systemSoftwareVersion;
    }

    public void setSystemSoftwareVersion(String systemSoftwareVersion) {
        this.systemSoftwareVersion = systemSoftwareVersion;
    }

    public String getOperatingSystemVersion() {
        return operatingSystemVersion;
    }

    public void setOperatingSystemVersion(String operatingSystemVersion) {
        this.operatingSystemVersion = operatingSystemVersion;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getInfraGroup() {
        return infraGroup;
    }

    public void setInfraGroup(String infraGroup) {
        this.infraGroup = infraGroup;
    }
}
