package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 1:03 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "LIFECYCLE_SUPPORT")
public class PlatformPortfolio extends TracableEntity {
    public String a;

    @Id
    public short b;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public short getB() {
        return b;
    }

    public void setB(short b) {
        this.b = b;
    }
}
