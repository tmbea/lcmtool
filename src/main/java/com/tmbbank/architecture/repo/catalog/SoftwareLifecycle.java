package com.tmbbank.architecture.repo.catalog;

import com.tmbbank.architecture.repo.common.TracableEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/3/14
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="SOFTWARE_LIFECYCLE")
public class SoftwareLifecycle extends TracableEntity {
    @TableGenerator(name = "SOFTWARE_LIFECYCLE")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    protected String platform;
    protected Date endOfSupportDt;
    protected String platformType;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Date getEndOfSupportDt() {
        return endOfSupportDt;
    }

    public void setEndOfSupportDt(Date endOfSupportDt) {
        this.endOfSupportDt = endOfSupportDt;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }
}
