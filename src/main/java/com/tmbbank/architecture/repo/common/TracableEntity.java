package com.tmbbank.architecture.repo.common;

import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/7/14
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
@MappedSuperclass
public class TracableEntity {
    protected String updatedBy = "Unknown";
    protected String createdBy = "Unknown";
    protected Timestamp updatedTimestamp = new Timestamp(new Date().getTime());
    protected Timestamp createdTimestamp = new Timestamp(new Date().getTime());


    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }
}
