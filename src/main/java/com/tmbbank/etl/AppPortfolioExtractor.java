package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.ApplicationPortfolio;
import jxl.Cell;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class AppPortfolioExtractor extends BaseExtractor<ApplicationPortfolio>{
    //protected String DEFAULT_FILENAME = "TMBAppList_2013 v0.16.xls";
   //
    public static void main(String[] args)throws Exception{
        AppPortfolioExtractor instance = new AppPortfolioExtractor();
        instance.loadRepository(null);
        instance.persist();

    }
    public void mapEntity(Object o, Cell[] cells) throws Exception {
        ApplicationPortfolio port = (ApplicationPortfolio)o;
        port.setAppId("");
        //port.set\U\1\E\2\(""\);
        //\s+String\s+(.*);

        int i = 0;
        port.setAppId(JExcelUtils.extract(cells, i++));

        port.setNumber(JExcelUtils.extract(cells, i++));

        port.setPlatform(JExcelUtils.extract(cells, i++));
        ++i;
        port.setAppName(JExcelUtils.extract(cells, i++));

        port.setAppNameCORM(JExcelUtils.extract(cells, i++));

        port.setBuSuGroup(JExcelUtils.extract(cells, i++));

        port.setResponsibleBy(JExcelUtils.extract(cells, i++));

        port.setVendorName(JExcelUtils.extract(cells, i++));

        port.setProductName(JExcelUtils.extract(cells, i++));

        port.setTmbInstalledVersion(JExcelUtils.extract(cells, i++));

        port.setCurrentlyOnMA(JExcelUtils.extract(cells, i++));

        port.setEndOfSaleDate(JExcelUtils.extract(cells, i++));

        port.setEndOfSupportDate(JExcelUtils.extract(cells, i++));

        port.setGaVersion(JExcelUtils.extract(cells, i++));

        port.setNextReleaseDate(JExcelUtils.extract(cells, i++));

        ++i;
        ++i;

        port.setImplementationDateMMYY(JExcelUtils.extract(cells, i++));

        port.setRunOnWhatTypeOfHW(JExcelUtils.extract(cells, i++));

        port.setDatabaseType(JExcelUtils.extract(cells, i++));

        port.setSourceCodeAvailability(JExcelUtils.extract(cells, i++));

        port.setLanguageUsed(JExcelUtils.extract(cells, i++));
        ++i;
        ++i;
        ++i;

        port.setOperational(JExcelUtils.extract(cells, i++));

        port.setProductDescription(JExcelUtils.extract(cells, i++));

        port.setInitialProjectDate(JExcelUtils.extract(cells, i++));

    }

    @Override
    public Object createContent() {
        return new ApplicationPortfolio();
    }


    @Override
    public int getStartRow() {
        return 4;
    }


}
