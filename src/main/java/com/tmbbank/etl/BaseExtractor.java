package com.tmbbank.etl;

import com.odenzo.core.io.RegexFileFilter;
import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.ApplicationPortfolio;
import com.tmbbank.settings.CommonSettings;
import com.tmbbank.util.jpa.JPAUtils;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseExtractor<E> implements ExcelMapper{
    public String DEFAULT_FOLDER = "C:\\Users\\Tooy\\Dropbox\\Personal\\workspaces\\TMB";
    public String DEFAULT_DATASOURCE = "TMB_ARCH_REPO";
    protected String DEFAULT_FILENAME = "";


    protected CommonSettings commonSettings;

    private static Logger _log = LoggerFactory.getLogger(BaseExtractor.class);
    private E prototypeObject;
    //protected long startRow;
    protected List<E> list;

    public List<E> getList() {
        return list;
    }

    public void setList(List<E> list) {
        this.list = list;
    }

    public void loadRepository(Properties property) {
        File sourceDir;
        sourceDir = new File(DEFAULT_FOLDER);
        String serviceSpecPattern = getFileName();

        try {
            File[] filesToProcess = sourceDir.listFiles(new RegexFileFilter(serviceSpecPattern));
            for (File aFile : filesToProcess) {
                parse(aFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
            _log.error("Could not load service repository information.");
        }
    }

    protected void intercept(List<E> list){

    }

    private void parse(File f) throws Exception, BiffException {
        _log.info("Parsing File: {}", f);
        FileInputStream is = new FileInputStream(f);
        Workbook workbook = Workbook.getWorkbook(is);

        Sheet sheet = workbook.getSheet(0);
        if (sheet == null) {
            throw new Exception("Could Not get Sheet 1");
        }
        List<E> entities = parseSheet(sheet);
        list = entities;
        intercept(entities);



    }


    public void mapEntity(List<E> e, Cell[] cells)throws Exception{
        E o = e.get(0);
        mapEntity((E)o, cells);
    }

    protected List<E> parseSheet(Sheet sheet) throws Exception {

        List<E> list =  new ArrayList<E>();
        int numRow = sheet.getRows();
        _log.info("Total Number of Row {}", numRow);

        for (int row = getStartRow(); row < numRow; row++) {
            Cell[] cells = sheet.getRow(row);
            _log.debug("Processing row {} found # cells {}", row, cells.length);
            if (JExcelUtils.emptyRowTest(cells)) {
                _log.info("Found Row {} was empty so stopping parsing", row);
                break;
            }
            E app = (E)createContent();
            List<E> subList = new ArrayList<E>();
            subList.add(app);

            mapEntity(subList, cells);

            _log.debug("Row {} Produced {}", row, app);
            list.addAll(subList);


        }
        this.list = list;
        return list;
    }



    public void persist()throws Exception{
        JPAUtils.persistObjects(list, DEFAULT_DATASOURCE);
    }


    public CommonSettings getCommonSettings() {
        return commonSettings;
    }

    public void setCommonSettings(CommonSettings commonSettings) {
        this.commonSettings = commonSettings;
    }

    public String getFileName(){
        return DEFAULT_FILENAME;
    }

    public void setFileName(String f){
        DEFAULT_FILENAME = f;
    }

    public String getDefaultDataSource(){
        return DEFAULT_DATASOURCE;
    }
    public void setDefaultDataSource(String ds){
        DEFAULT_DATASOURCE = ds;
    }

    public String getDefaultWorkingDirectory(){
        return DEFAULT_FOLDER;
    }

    public void setDefaultWorkingDirectory(String defaultWorkingDirectory){
        DEFAULT_FOLDER = defaultWorkingDirectory;
    }
}

