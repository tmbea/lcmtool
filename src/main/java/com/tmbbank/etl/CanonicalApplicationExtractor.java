package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.CanonicalApplication;
import jxl.Cell;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 1:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class CanonicalApplicationExtractor extends BaseExtractor<CanonicalApplication>{

    //protected String DEFAULT_FILENAME = "Cannonical Application Name.xls";

    //protected String DEFAULT_FILENAME = "Canonical Application Name.xls";

    public static void main(String[] args)throws Exception{
        CanonicalApplicationExtractor instance = new CanonicalApplicationExtractor();
        instance.loadRepository(null);
        instance.persist();

    }

    /*
    protected void intercept(List<PlatformDeploymentPortfolio> list){
        String appName = "";
        for(PlatformDeploymentPortfolio port: list){
            if (port.getAppName()!=null && !port.getAppName().equals("")){
                appName = port.getAppName();
            }
            port.setAppName(appName);

        }
    }
    */

    @Override
    public void mapEntity(Object o, Cell[] cells) throws Exception {
        CanonicalApplication canonicalApplication = (CanonicalApplication)o;


        int i = 0;

        canonicalApplication.setAppName(JExcelUtils.extract(cells, i++));
        canonicalApplication.setAppId(JExcelUtils.extract(cells, i++));


    }

    @Override
    public Object createContent() {
        return new CanonicalApplication();
    }


    @Override
    public int getStartRow() {
        return 1;
    }

    public void loadRepository(Properties properties){
        super.loadRepository(properties);



    }

}
