package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.CanonicalApplication;
import com.tmbbank.architecture.repo.catalog.CanonicalSoftware;
import jxl.Cell;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 1:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class CanonicalSoftwareExtractor extends BaseExtractor<CanonicalSoftware>{

    //protected String DEFAULT_FILENAME = "Cannonical Application Name.xls";

    //protected String DEFAULT_FILENAME = "Canonical Software Version.xls";

    public static void main(String[] args)throws Exception{
        CanonicalSoftwareExtractor instance = new CanonicalSoftwareExtractor();
        instance.loadRepository(null);
        instance.persist();

    }

    /*
    protected void intercept(List<PlatformDeploymentPortfolio> list){
        String appName = "";
        for(PlatformDeploymentPortfolio port: list){
            if (port.getAppName()!=null && !port.getAppName().equals("")){
                appName = port.getAppName();
            }
            port.setAppName(appName);

        }
    }
    */

    @Override
    public void mapEntity(Object o, Cell[] cells) throws Exception {
        CanonicalSoftware canonicalSoftware = (CanonicalSoftware)o;
        int i = 0;

        canonicalSoftware.setPlatform(JExcelUtils.extract(cells, i++));
        String version = JExcelUtils.extract(cells, i++);
        if (version == null) version = "";
        canonicalSoftware.setVersion(version);
        canonicalSoftware.setCanonicalSoftwareName(JExcelUtils.extract(cells, i++));


    }

    @Override
    public Object createContent() {
        return new CanonicalSoftware();
    }

    @Override
    public String getFileName() {
        return DEFAULT_FILENAME;
    }

    @Override
    public int getStartRow() {
        return 1;
    }

    public void loadRepository(Properties properties){
        super.loadRepository(properties);



    }

}
