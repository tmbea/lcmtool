package com.tmbbank.etl;

import com.odenzo.core.utils.StringUtils;
import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.*;
import com.tmbbank.util.jpa.JPAUtils;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.format.Border;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 1:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class DeploymentPlatformExtractor extends BaseExtractor<PlatformDeploymentPortfolio>{
    protected static Logger log = LoggerFactory.getLogger(DeploymentPlatformExtractor.class);
    //protected String DEFAULT_FILENAME = "TMBInfraList_2013 v0.11.xls";
    protected HashMap<String, String> canonicalApplications;
    protected HashMap<String, String> isaApplications;
    protected CanonicalApplicationExtractor canonicalApplicationExtractor;
    protected ISAAppPortfolioExtractor isaAppPortfolioExtractor;
    protected List<ApplicationPlatformVersion> applicationPlatformVersions;
    //protected String DEFAULT_FILENAME = "Data Center Application List as of 4 Feb 2014 by Tui.xls";
    protected Sheet currentSheet;

    public static void main(String[] args)throws Exception{

        DeploymentPlatformExtractor instance = new DeploymentPlatformExtractor();
        instance.loadRepository(null);
        instance.persist();



    }
    public void persist()throws Exception{
        super.persist();
        //JPAUtils.persistObjects(list, DEFAULT_DATASOURCE);
        JPAUtils.persistObjects(applicationPlatformVersions, DEFAULT_DATASOURCE);
    }

    private void interceptApplicationPlatforms(PlatformDeploymentPortfolio platformDeploymentPortfolio, List<ApplicationPlatformVersion> applicationPlatformVersions){
        String platforms = platformDeploymentPortfolio.getSystemSoftwareList();
        String versions = platformDeploymentPortfolio.getSystemSoftwareVersion();
        StringTokenizer platformsTokenizer = null;
        StringTokenizer versionsTokenizer = null;

        String appId = "";
        appId = isaApplications.get(platformDeploymentPortfolio.getApplicationSystem());
        if (appId == null){
            appId = canonicalApplications.get(platformDeploymentPortfolio.getApplicationSystem());

        }
        platformDeploymentPortfolio.setAppId(appId);


        if (platforms != null){
            platformsTokenizer = new StringTokenizer(platforms, "\r\n");
            if(versions!=null)
                versionsTokenizer = new StringTokenizer(versions, "\r\n");
            else
                versionsTokenizer = new StringTokenizer("", "\r\n");


            while(platformsTokenizer.hasMoreTokens()){
                String platform = platformsTokenizer.nextToken();
                String version = "";
                if (versionsTokenizer.hasMoreTokens()){
                    version = versionsTokenizer.nextToken();
                }
                ApplicationPlatformVersion apv = new ApplicationPlatformVersion();

                apv.setAppId(appId);

                apv.setPlatform(platform);
                apv.setVersion(version);
                apv.setNumber(platformDeploymentPortfolio.getNumber());

                applicationPlatformVersions.add(apv);
            }
        }

    }

    protected void intercept(List<PlatformDeploymentPortfolio> list){
        // canonicalApplicationExtractor = new CanonicalApplicationExtractor();
        if (canonicalApplicationExtractor.getList()==null)
            canonicalApplicationExtractor.loadRepository(null);
        this.canonicalApplications = toCanonicalAppNameHashMap(canonicalApplicationExtractor.getList());


        if (isaAppPortfolioExtractor.getList()==null)
            isaAppPortfolioExtractor.loadRepository(null);
        this.isaApplications = toISAAppNameHashMap(isaAppPortfolioExtractor.getList());

        applicationPlatformVersions = new ArrayList<ApplicationPlatformVersion>();
        //String appName = "";
        for(PlatformDeploymentPortfolio port: list){
            //if (port.getApplicationSystem()!=null && !port.getApplicationSystem().equals("")){
            //    appName = port.getApplicationSystem();
            //}else{
            //    port.setApplicationSystem(appName);
            //}
            if (!port.isDeleted())
                interceptApplicationPlatforms(port, applicationPlatformVersions);


        }
    }
    @Override
    public void mapEntity(List<PlatformDeploymentPortfolio> list, Cell[] cells)throws Exception{
        List<String> applicationsInSaveServers = getApplicationInSameServers(cells[0].getRow(), currentSheet);
        String infraGroup = StringUtils.uniqueValueList(applicationsInSaveServers);
        list.clear();

        for (String app: applicationsInSaveServers){
            PlatformDeploymentPortfolio platformDeploymentPortfolio = new PlatformDeploymentPortfolio();
            platformDeploymentPortfolio.setApplicationSystem(app);
            mapEntity(platformDeploymentPortfolio, cells);
            platformDeploymentPortfolio.setInfraGroup(infraGroup);
            list.add(platformDeploymentPortfolio);
        }
    }
    /**
     *
     0	No.
     1	Application No.
     2	Application Tier
     3	Responsibility
     4	Application System
     5	Server / Computer / Host Name
     6	IP Address
     7	Location Site
     8	Environment
     9	Brand / Model Type
     10	CPU
     11	Core
     12	Memory (GB)
     13	H/W Serial No.
     14
     15	Operating System
     16
     17
     18	Type of Servers
     19	Service Role
     20	All System Software List
     21
     22
     23	System Owner
     24	Internet Access
     25	LDAP Connect
     26	SAN Usage
     27	SAN Replication
     28	Backup Agent
     29	Monitoring Agent
     30	URL
     31	Remark
     *
     */
    @Override
    public void mapEntity(Object o, Cell[] cells) throws Exception {

        PlatformDeploymentPortfolio port = (PlatformDeploymentPortfolio)o;
        //port.set\U\1\E\2\(""\);
        //\s+String\s+(.*);

        int i = 0;
        port.setNumber(JExcelUtils.extract(cells, 0));
        port.setAppId(JExcelUtils.extract(cells, 1));
        port.setApplicationNumber(JExcelUtils.extract(cells, 1));
        //port.setNumberApp(JExcelUtils.extract(cells, i++));
        //port.setAppName(JExcelUtils.extract(cells, i++));
        port.setApplicationTier(JExcelUtils.extract(cells, 2));
        //port.setApplicationSystem(app);
        i++;
        i++;
        if (cells[0].getCellFormat().getFont().isStruckout() ){
            port.setDeleted(true);
        }
        port.setServerComputerHostName(JExcelUtils.extract(cells, 5));
        port.setIpAddress(JExcelUtils.extract(cells, 6));

        port.setLocationSite(JExcelUtils.extract(cells, 7));
        port.setEnvironment(JExcelUtils.extract(cells, 8));
        port.setBrandModelType(JExcelUtils.extract(cells, 9));
        i+=4;
        port.setOperatingSystem(JExcelUtils.extract(cells, 14));
        port.setOperatingSystemVersion(JExcelUtils.extract(cells, 15));
        port.setTypeOfServers(JExcelUtils.extract(cells, 18));
        i+=2;
        port.setServiceRole(JExcelUtils.extract(cells, 19));
        port.setSystemSoftwareList(JExcelUtils.extract(cells, 20));
        port.setSystemSoftwareVersion(JExcelUtils.extract(cells, 21));
        i++;
        port.setSystemOwner(JExcelUtils.extract(cells, 23));
        port.setInternetAccess(JExcelUtils.extract(cells, 24));
        port.setLdapConnect(JExcelUtils.extract(cells, 25));
        port.setSanUsage(JExcelUtils.extract(cells, 26));
        port.setSanReplication(JExcelUtils.extract(cells, 27));
        port.setBackupAgent(JExcelUtils.extract(cells, 28));
        port.setMonitoringAgent(JExcelUtils.extract(cells, 29));
        port.setUrl(JExcelUtils.extract(cells, 30));
        port.setRemark(JExcelUtils.extract(cells, 31));

    }

    @Override
    public Object createContent() {
        return new PlatformDeploymentPortfolio();
    }



    @Override
    public int getStartRow() {
        return 1;
    }

    public void loadRepository(Properties properties){
        super.loadRepository(properties);





    }

    private HashMap<String, String> toCanonicalAppNameHashMap(List<CanonicalApplication> list){
        HashMap<String, String> hashMap = new LinkedHashMap<String, String>();
        for (CanonicalApplication canonicalApplication: list){
            hashMap.put(canonicalApplication.getAppName(), canonicalApplication.getAppId());
        }
        return hashMap;

    }


    private HashMap<String, String> toISAAppNameHashMap(List<ISAApplicationPortfolio> list){
        HashMap<String, String> hashMap = new LinkedHashMap<String, String>();
        for (ISAApplicationPortfolio isaApplicationPortfolio: list){
            hashMap.put(isaApplicationPortfolio.getAppName(), isaApplicationPortfolio.getAppId());
        }
        return hashMap;

    }


    protected List<String> getApplicationInSameServers(int i, Sheet sht) throws Exception {
        List<String> list = new ArrayList<String>();
        int topRange = -1;
        int bottomRange = -1;
        for (int r=i;r>0;r--){
            Cell[] cells = sht.getRow(r);
            Cell[] upperCells = sht.getRow(r-1);
            Cell[] lowerCells;
            if (sht.getRows()>(r+1)){
                lowerCells = sht.getRow(r+1);
            }else{
                lowerCells = sht.getRow(r);
            }


            if (!upperCells[1].getCellFormat().getBorderLine(Border.BOTTOM).getDescription().equals("none")){
                topRange = r;
            }

            if (!cells[1].getCellFormat().getBorderLine(Border.TOP).getDescription().equals("none")){
                topRange = r;
            }

            if (topRange>=0){
                break;
            }
        }
        for (int r=i;r<sht.getRows();r++){

            Cell[] cells = sht.getRow(r);
            Cell[] upperCells = sht.getRow(r-1);
            Cell[] lowerCells;
            if (sht.getRows()>(r+1)){
                lowerCells = sht.getRow(r+1);
            }else{
                lowerCells = sht.getRow(r);
            }
            if ((lowerCells[1].getCellFormat()!=null)&&(!lowerCells[1].getCellFormat().getBorderLine(Border.TOP).getDescription().equals("none"))) {
                bottomRange = r;
            }
            if (!cells[1].getCellFormat().getBorderLine(Border.BOTTOM).getDescription().equals("none")){
                bottomRange = r;
            }

            //try{
            //    lowerCells[1].getCellFormat().getBorderLine(Border.TOP).getDescription().equals("none");
            //}catch(NullPointerException e){
            //    e.printStackTrace();
            //}



            if (bottomRange>=0)
                break;

        }
        if (topRange>=0 && bottomRange>=0){
            log.debug("extract topRange: {}", topRange);
            log.debug("extract bottomRange: {}", bottomRange);
            for (int j=topRange; j<=bottomRange; j++){
                Cell[] tmpCells = sht.getRow(j);
                String value = JExcelUtils.extract(tmpCells, 4);
                if (value != null )
                    list.add(value);
            }
        }

        return list;
    }

    public CanonicalApplicationExtractor getCanonicalApplicationExtractor() {
        return canonicalApplicationExtractor;
    }

    public void setCanonicalApplicationExtractor(CanonicalApplicationExtractor canonicalApplicationExtractor) {
        this.canonicalApplicationExtractor = canonicalApplicationExtractor;
    }

    public ISAAppPortfolioExtractor getIsaAppPortfolioExtractor() {
        return isaAppPortfolioExtractor;
    }

    public void setIsaAppPortfolioExtractor(ISAAppPortfolioExtractor isaAppPortfolioExtractor) {
        this.isaAppPortfolioExtractor = isaAppPortfolioExtractor;
    }

    protected List<PlatformDeploymentPortfolio> parseSheet(Sheet sheet) throws Exception {
        currentSheet = sheet;
        return super.parseSheet(sheet);
    }

}