package com.tmbbank.etl;

import jxl.Cell;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/13/13
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ExcelMapper<E> {
    public void mapEntity(E e, Cell[] cells) throws Exception;
    public E createContent();
    public String getFileName();
    public int getStartRow();

}
