package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import jxl.Cell;

import com.tmbbank.architecture.repo.catalog.ApplicationPortfolio;


/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/10/13
 * Time: 10:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericExtractor{


    public static void main(String[] args)throws Exception{
        BaseExtractor<ApplicationPortfolio> instance = new BaseExtractor<ApplicationPortfolio>(){
            protected String DEFAULT_FILENAME = "TMBAppList_2013 v0.11.xls";
            @Override
            public void mapEntity(Object o, Cell[] cells) throws Exception {
                  ApplicationPortfolio port = (ApplicationPortfolio)o;
                  port.setAppId("");
                //port.set\U\1\E\2\(""\);
                //\s+String\s+(.*);

                int i = 0;
                port.setAppId(JExcelUtils.extract(cells, i++));

                port.setNumber(JExcelUtils.extract(cells, i++));

                port.setPlatform(JExcelUtils.extract(cells, i++));
                ++i;
                port.setAppName(JExcelUtils.extract(cells, i++));
                ++i;
                port.setAppNameCORM(JExcelUtils.extract(cells, i++));

                port.setBuSuGroup(JExcelUtils.extract(cells, i++));

                port.setResponsibleBy(JExcelUtils.extract(cells, i++));

                port.setVendorName(JExcelUtils.extract(cells, i++));

                port.setProductName(JExcelUtils.extract(cells, i++));

                port.setTmbInstalledVersion(JExcelUtils.extract(cells, i++));

                port.setCurrentlyOnMA(JExcelUtils.extract(cells, i++));

                port.setEndOfSaleDate(JExcelUtils.extract(cells, i++));

                port.setEndOfSupportDate(JExcelUtils.extract(cells, i++));

                port.setGaVersion(JExcelUtils.extract(cells, i++));

                port.setNextReleaseDate(JExcelUtils.extract(cells, i++));

                port.setImplementationDateMMYY(JExcelUtils.extract(cells, i++));

                port.setRunOnWhatTypeOfHW(JExcelUtils.extract(cells, i++));

                port.setDatabaseType(JExcelUtils.extract(cells, i++));

                port.setSourceCodeAvailability(JExcelUtils.extract(cells, i++));

                port.setLanguageUsed(JExcelUtils.extract(cells, i++));

                port.setOperational(JExcelUtils.extract(cells, i++));

                port.setProductDescription(JExcelUtils.extract(cells, i++));

                port.setInitialProjectDate(JExcelUtils.extract(cells, i++));



            }

            @Override
            public Object createContent() {
                return new ApplicationPortfolio();
            }

            @Override
            public String getFileName() {
                return "TMBAppList_2013 v0.11.xls";
            }

            @Override
            public int getStartRow() {
                return 3;
            }
        };
        instance.loadRepository(null);

    }

}
