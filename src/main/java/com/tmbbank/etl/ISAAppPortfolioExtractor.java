package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.ApplicationPortfolio;
import com.tmbbank.architecture.repo.catalog.ISAApplicationPortfolio;
import jxl.Cell;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class ISAAppPortfolioExtractor extends BaseExtractor<ISAApplicationPortfolio> {
    public static void main(String[] args)throws Exception{
        BaseExtractor<ISAApplicationPortfolio> instance = new ISAAppPortfolioExtractor();

        instance.loadRepository(null);
        instance.persist();

    }
    //protected String DEFAULT_FILENAME = "TMB CMDB Application List v2.9_20131226.xls";
    @Override
    public void mapEntity(Object o, Cell[] cells) throws Exception {
        ISAApplicationPortfolio port = (ISAApplicationPortfolio)o;
        port.setAppId("");
        //port.set\U\1\E\2\(JExcelUtils.extract\(cells, i++\)\);
        //\s+String\s+(.*);

        int i = 0;


        port.setAppId(JExcelUtils.extract(cells, i++));
        port.setAppName(JExcelUtils.extract(cells, i++));
        port.setcRating(JExcelUtils.extract(cells, i++));
        port.setiRating(JExcelUtils.extract(cells, i++));
        port.setaRating(JExcelUtils.extract(cells, i++));

        port.setCriticality(JExcelUtils.extract(cells, i++));
        i++;
        port.setApplicationDescription(JExcelUtils.extract(cells, i++));
        port.setSystemOwner(JExcelUtils.extract(cells, i++));


        port.setApplicationType(JExcelUtils.extract(cells, i++));
        port.setApplicationSupportContact(JExcelUtils.extract(cells, i++));

        port.setApplicationSupportContactDepartment(JExcelUtils.extract(cells, i++));
        port.setItiSupportcontact(JExcelUtils.extract(cells, i++));
        port.setItiSupportContactDepartment(JExcelUtils.extract(cells, i++));
        port.setItoSupportContact(JExcelUtils.extract(cells, i++));
        port.setItoSupportContactDepartment(JExcelUtils.extract(cells, i++));
        port.setPublicFacing(JExcelUtils.extract(cells, i++));
        i++;
        port.setItgSupported(JExcelUtils.extract(cells, i++));
        port.setUserManagementDepartment(JExcelUtils.extract(cells, i++));
        port.setChief(JExcelUtils.extract(cells, i++));
        port.setApplicationStatus(JExcelUtils.extract(cells, i++));
        port.setRemark(JExcelUtils.extract(cells, i++));


    }

    @Override
    public Object createContent() {
        return new ISAApplicationPortfolio();
    }

    @Override
    public String getFileName() {
        return DEFAULT_FILENAME;
    }

    @Override
    public int getStartRow() {
        return 5;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
