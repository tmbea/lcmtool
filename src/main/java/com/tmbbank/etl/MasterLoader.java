package com.tmbbank.etl;

import com.tmbbank.settings.CommonSettings;
import com.tmbbank.util.jpa.JPAUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.transaction.Transaction;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/5/14
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class MasterLoader {
    private static Logger log = LoggerFactory.getLogger(MasterLoader.class);
    private CommonSettings commonSettings;
    private List<BaseExtractor> extractors;

    public void cleanAllTable()throws Exception{
        EntityManager em = JPAUtils.getEntityManager(commonSettings.getDataSourceName());
        //JPAUtils.deleteObjects("delete from CanonicalSoftware", commonSettings.getDataSourceName());
        EntityTransaction trx = em.getTransaction();
        trx.begin();
        Query query;
        //query = em.createQuery("DELETE FROM com.tmbbank.architecture.repo.catalog.CanonicalSoftware");
        query = em.createNativeQuery("delete from canonical_software");
        query.executeUpdate();
        query = em.createNativeQuery("delete from canonical_application");
        query.executeUpdate();
        query = em.createNativeQuery("delete from app_platform_version");
        query.executeUpdate();
        query = em.createNativeQuery("delete from platform_deployment_portfolio");
        query.executeUpdate();
        query = em.createNativeQuery("delete from app_portfolio_isa");
        query.executeUpdate();
        query = em.createNativeQuery("delete from software_lifecycle");
        query.executeUpdate();
        query = em.createNativeQuery("delete from app_portfolio");
        query.executeUpdate();

        trx.commit();
    }


    public static void main(String[] args)throws Exception{
        log.debug("Started loader");
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});

        MasterLoader loader = (MasterLoader)context.getBean("masterLoader");
        loader.masterLoad();
        log.debug("Finished loader");
    }

    public void masterLoad() throws Exception{
        cleanAllTable();

        for(BaseExtractor extractor: extractors){
            extractor.loadRepository(null);
            extractor.persist();
        }
    }


    public List<BaseExtractor> getExtractors() {
        return extractors;
    }

    public void setExtractors(List<BaseExtractor> extractors) {
        this.extractors = extractors;
    }

    public CommonSettings getCommonSettings() {
        return commonSettings;
    }

    public void setCommonSettings(CommonSettings commonSettings) {
        this.commonSettings = commonSettings;
    }
}
