package com.tmbbank.etl;

import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.CanonicalApplication;
import com.tmbbank.architecture.repo.catalog.SoftwareLifecycle;
import jxl.Cell;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/14/13
 * Time: 1:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class SoftwareLifecycleExtractor extends BaseExtractor<SoftwareLifecycle>{

    //protected String DEFAULT_FILENAME = "Cannonical Application Name.xls";

    //protected String DEFAULT_FILENAME = "Software Lifecycle.xls";

    public static void main(String[] args)throws Exception{
        SoftwareLifecycleExtractor instance = new SoftwareLifecycleExtractor();
        instance.loadRepository(null);
        instance.persist();

    }

    /*
    protected void intercept(List<PlatformDeploymentPortfolio> list){
        String appName = "";
        for(PlatformDeploymentPortfolio port: list){
            if (port.getAppName()!=null && !port.getAppName().equals("")){
                appName = port.getAppName();
            }
            port.setAppName(appName);

        }
    }
    */

    @Override
    public void mapEntity(Object o, Cell[] cells) throws Exception {
        SoftwareLifecycle softwareLifecycle= (SoftwareLifecycle)o;
        int i = 0;

        if (cells != null){
            if (cells.length > i)
                softwareLifecycle.setPlatform(JExcelUtils.extract(cells, i++));

            if (cells.length > i){
                Calendar c = JExcelUtils.extractDateFromCell(cells[i++]);
                if (c != null)
                softwareLifecycle.setEndOfSupportDt( c.getTime());

            }

            if (cells.length > i){

                softwareLifecycle.setPlatformType( JExcelUtils.extract(cells, i++));

            }
        }



    }

    @Override
    public Object createContent() {
        return new SoftwareLifecycle();
    }

    @Override
    public String getFileName() {
        return DEFAULT_FILENAME;
    }

    @Override
    public int getStartRow() {
        return 1;
    }

    public void loadRepository(Properties properties){
        super.loadRepository(properties);



    }

}
