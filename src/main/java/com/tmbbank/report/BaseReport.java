package com.tmbbank.report;

import com.odenzo.core.io.FileUtils;
import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.etl.DeploymentPlatformExtractor;
import com.tmbbank.settings.CommonSettings;
import com.tmbbank.util.jpa.JPAUtils;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.write.*;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by 29759 on 29/06/58.
 */
public abstract class BaseReport {
    protected CommonSettings commonSettings;
    protected DeploymentPlatformExtractor deploymentPlatformExtractor;

    protected static final int COLOR_BLUE = 10;
    protected static final int COLOR_GREEN = 11;

    protected static final int COLOR_BROWN = 12;

    protected static final int COLOR_YELLOW = 13;

    protected Label addLabel(String string, int row, int column, WritableSheet sheet, Orientation orientation, int color)throws Exception{
        Label label = new Label(column, row, string);

        if (orientation!=null){
            WritableCellFormat format = new WritableCellFormat();

            format.setOrientation(orientation);
            format.setBackground(Colour.getInternalColour(color));
            format.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);

            label.setCellFormat(format);

        }

        sheet.addCell(label);
        return label;

    }

    protected DateTime addDate(Date date, int row, int column, WritableSheet sheet, Orientation orientation, int color)throws Exception{

        DateTime dateTime = new DateTime(column, row, date);
        if (orientation!=null){
            WritableCellFormat format = new WritableCellFormat(new DateFormat("dd/MM/yyyy"));
            format.setOrientation(orientation);
            format.setBackground(Colour.getInternalColour(color));
            format.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
            dateTime.setCellFormat(format);

        }
        sheet.addCell(dateTime);
        return dateTime;

    }


    public DeploymentPlatformExtractor getDeploymentPlatformExtractor() {
        return deploymentPlatformExtractor;
    }

    public void setDeploymentPlatformExtractor(DeploymentPlatformExtractor deploymentPlatformExtractor) {
        this.deploymentPlatformExtractor = deploymentPlatformExtractor;
    }
    public String getSQLFromResourceFile(String resourceFileName)throws Exception{
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(resourceFileName);
        String sql = FileUtils.readInputStreamToString(in);

        return sql;

    }

    public EntityManager getEntityManager(){
        return JPAUtils.getEntityManager(commonSettings.getDataSourceName());
    }


    public CommonSettings getCommonSettings() {
        return commonSettings;
    }

    public void setCommonSettings(CommonSettings commonSettings) {
        this.commonSettings = commonSettings;
    }


    public void generateReport(String filename)throws Exception{

        File f = new File(filename);

        WritableWorkbook writableWorkbook = JExcelUtils.createWorkbook(f);

        generateReport(writableWorkbook);
        writableWorkbook.write();
        writableWorkbook.close();

    }

    public void generateReport(WritableWorkbook writableWorkbook)throws Exception{

    }

}
