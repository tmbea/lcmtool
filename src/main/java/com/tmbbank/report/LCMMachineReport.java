package com.tmbbank.report;

import com.odenzo.core.datetime.DateUtils;
import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.architecture.repo.catalog.PlatformDeploymentPortfolio;
import com.tmbbank.architecture.repo.catalog.SoftwareLifecycle;
import com.tmbbank.settings.CommonSettings;
import jxl.CellView;
import jxl.format.Orientation;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.persistence.Query;
import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by 29759 on 29/06/58.
 */
public class LCMMachineReport extends BaseReport{

    private static Logger log = LoggerFactory.getLogger(LCMReport.class);
    public static void main(String[] args)throws Exception{
        log.debug("Started report");
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});

        LCMMachineReport report = (LCMMachineReport)context.getBean("lcmMachineReport");
        report.generateReport(report.getCommonSettings().getLcmReportOutputWorkbook());
        log.debug("Finished report");
    }
    public void generateReport(WritableWorkbook writableWorkbook) throws Exception {

        WritableSheet serversSheet = writableWorkbook.createSheet("Servers", writableWorkbook.getNumberOfSheets());

        addLabel("Application ID", 0,0, serversSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("Application Name", 0, 1, serversSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("Machine", 0,2, serversSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("Software", 0,3, serversSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("End of Support", 0, 4, serversSheet, Orientation.HORIZONTAL, COLOR_BLUE);




        List l = getEndOfSupportPlatforms();
        int i = 1;
        for(int j=0; j < l.size(); j++){
            Object[] port = (Object[])l.get(j);
            addLabel((String)port[0], i, 0, serversSheet, null, 0);
            addLabel((String)port[1], i, 1, serversSheet, null, 0);
            addLabel((String)port[2], i, 2, serversSheet, null, 0);
            addLabel((String)port[3], i, 3, serversSheet, null, 0);
            addDate(new Date(((Timestamp) port[4]).getTime()), i, 4, serversSheet, null, 0);

            //addLabel(key, i, 3, serversSheet, null, 0);
            i++;

        }
        CellView colView;
        colView = serversSheet.getColumnView(0);
        colView.setSize(3000);
        serversSheet.setColumnView(0, colView);


        colView = serversSheet.getColumnView(1);
        colView.setSize(7000);
        serversSheet.setColumnView(1, colView);

        colView = serversSheet.getColumnView(2);
        colView.setSize(5000);
        serversSheet.setColumnView(2, colView);

        colView = serversSheet.getColumnView(3);
        colView.setSize(8000);
        serversSheet.setColumnView(3, colView);

        colView = serversSheet.getColumnView(4);
        colView.setSize(3000);
        serversSheet.setColumnView(4, colView);
    }


    public List getEndOfSupportPlatforms()throws Exception{
        String sSql = getSQLFromResourceFile("report-endoflife2.sql");

        Query query = getEntityManager().createNativeQuery(sSql);
        query.setParameter("startDate", new Date(DateUtils.parseISODate(commonSettings.getStartDate()).getTimeInMillis()));
        query.setParameter("endDate", new Date(DateUtils.parseISODate(commonSettings.getEndDate()).getTimeInMillis()));

        return query.getResultList();
    }

    public CommonSettings getCommonSettings() {
        return commonSettings;
    }

    public void setCommonSettings(CommonSettings commonSettings) {
        this.commonSettings = commonSettings;
    }
}
