package com.tmbbank.report;

import com.odenzo.core.datetime.DateUtils;
import com.odenzo.core.io.FileUtils;
import com.odenzo.jexcel.JExcelUtils;
import java.io.File;
import java.io.InputStream;
import java.util.*;

import com.tmbbank.architecture.repo.catalog.ApplicationPortfolio;
import com.tmbbank.architecture.repo.catalog.PlatformDeploymentPortfolio;
import com.tmbbank.architecture.repo.catalog.SoftwareLifecycle;
import com.tmbbank.etl.BaseExtractor;
import com.tmbbank.etl.DeploymentPlatformExtractor;
import com.tmbbank.settings.CommonSettings;
import com.tmbbank.util.jpa.JPAUtils;
import javafx.application.Platform;
import jxl.CellView;
import jxl.Sheet;
import jxl.format.*;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/5/14
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class LCMReport extends BaseReport{
    private static Logger log = LoggerFactory.getLogger(LCMReport.class);

    private HashMap<String, ShortInfo> cachedAppNameID;



    class ShortInfo{
       private String appId;
        private String appName;
        private String responsible;


    }

    public static void main(String[] args)throws Exception{
        log.debug("Started loader");
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});

        LCMReport report = (LCMReport)context.getBean("lcmReport");
        report.generateReport(report.getCommonSettings().getLcmReportOutputWorkbook());
        log.debug("Finished loader");
    }


    public void generateReport(WritableWorkbook writableWorkbook)throws Exception{
        //deploymentPlatformExtractor.loadRepository(null);

        List<PlatformDeploymentPortfolio> infraApps = getInfraPortfolio(); //deploymentPlatformExtractor.getList();
        List<SoftwareLifecycle> platforms = getEndOfSupportPlatforms();
        HashMap<String, boolean[]> matrix = produceApplicationMatrix(platforms);


        //Blue
        writableWorkbook.setColourRGB(Colour.getInternalColour(10),0x99, 0xcc, 0xff);
        //Green
        writableWorkbook.setColourRGB(Colour.getInternalColour(11),0xcc, 0xff, 0xcc);
        //Brown
        writableWorkbook.setColourRGB(Colour.getInternalColour(12),0xff, 0xcc, 0x99);
        //Yellow
        writableWorkbook.setColourRGB(Colour.getInternalColour(13),0xff, 0xff, 0x99);
        //writableWorkbook.setColourRGB(Colour.getInternalColour(14),0x99, 0xcc, 0xff);

        WritableSheet writableSheet = writableWorkbook.createSheet("LCM Report", writableWorkbook.getNumberOfSheets());

        CellView colView;
        WritableCellFormat colFormat;





        int column = 5;
        int platformRow = 0;
        int dateRow = 1;
        for(SoftwareLifecycle platform: platforms){
            int color;
            if (platform.getPlatformType().equals("1_OS")){
                color =COLOR_GREEN;
            }else if(platform.getPlatformType().equals("2_DB")){
                color = COLOR_BLUE;
            }else if(platform.getPlatformType().equals("3_APP_SERVER")){
                color = COLOR_BROWN;
            }else{
                color = COLOR_YELLOW;
            }

            Label label = addLabel(platform.getPlatform(),platformRow,column, writableSheet, Orientation.PLUS_45, color);
            CellView columnView = writableSheet.getColumnView(column);
            colFormat = new WritableCellFormat();
            colFormat.setBackground(Colour.getInternalColour(color));
            colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);



            columnView.setFormat(colFormat);
            columnView.setSize(1200);
            DateTime dateTime = addDate(platform.getEndOfSupportDt(), dateRow, column, writableSheet, Orientation.PLUS_90, color);
            writableSheet.setColumnView(column, columnView);

            column++;
        }

        CellView rowView = writableSheet.getRowView(1);
        WritableCellFormat rowFormat;
        rowFormat = new WritableCellFormat(new DateFormat("dd/MM/yyyy"));

        rowView.setFormat(rowFormat);

        //String[] keys = (String[]) matrix.keySet().toArray();
        Set<String> keys = matrix.keySet();

        int i=0;
        for(String key: keys){
            boolean[] matrixValues = matrix.get(key);
            ShortInfo shortInfo = cachedAppNameID.get(key);
            String appName = shortInfo.appName;
            String responsible = shortInfo.responsible;
            addLabel(key, i + 3, 0, writableSheet, null, 0);
            addLabel(appName, i + 3, 1, writableSheet, null, 0);
            addLabel(responsible, i + 3, 2, writableSheet, null, 0);
            for(int j=0; j<platforms.size();j++){

                if (matrixValues[j]){
                    Label checkedLabel = addLabel("✔", i+3,j+5, writableSheet, null, 0);
                    //checkedLabel.getCellFeatures().setComment(getServersFor);
                }
            }

            i++;
        }

        colView = writableSheet.getColumnView(0);
        colFormat = new WritableCellFormat();
        colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
        colView.setSize(2000);
        colView.setFormat(colFormat);
        writableSheet.setColumnView(0, colView);


        colView = writableSheet.getColumnView(1);
        colFormat = new WritableCellFormat();
        colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
        colView.setSize(8000);
        colView.setFormat(colFormat);
        writableSheet.setColumnView(1, colView);

        colView = writableSheet.getColumnView(2);
        colFormat = new WritableCellFormat();
        colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
        colView.setSize(8000);
        colView.setFormat(colFormat);
        writableSheet.setColumnView(2, colView);

        colView = writableSheet.getColumnView(3);
        colFormat = new WritableCellFormat();
        colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
        colView.setSize(5000);
        colView.setFormat(colFormat);
        writableSheet.setColumnView(3, colView);

        colView = writableSheet.getColumnView(4);
        colFormat = new WritableCellFormat();
        colView.setSize(5000);
        colView.setFormat(colFormat);
        colFormat.setBorder(Border.ALL, BorderLineStyle.HAIR, Colour.BLACK);
        writableSheet.setColumnView(4, colView);

        addLabel("Application ID", 0,0, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);
        addLabel("", 1,0, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("Application Name", 0, 1, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);
        addLabel("", 1, 1, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("System Owner", 0,2, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);
        addLabel("", 1,2, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("LCM Status", 0,3, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);
        addLabel("", 1,3, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        addLabel("Estimate Upgrade Date", 0,4, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);
        addLabel("", 1,4, writableSheet, Orientation.HORIZONTAL, COLOR_BLUE);

        //lazy coding will fix later
        LCMMachineReport lcmMachineReport = new LCMMachineReport();
        lcmMachineReport.setCommonSettings(commonSettings);
        lcmMachineReport.generateReport(writableWorkbook);

    }




    public List<ApplicationPortfolio> getApplicationPortfolio()throws Exception{
        Query query = getEntityManager().createQuery("from ApplicationPortfolio");
        //query.setParameter("date", new Date(), TemporalType.DATE);

        return query.getResultList();
    }

    public List<PlatformDeploymentPortfolio> getInfraPortfolio()throws Exception{
        Query query = getEntityManager().createQuery("from PlatformDeploymentPortfolio");
        //query.setParameter("date", new Date(), TemporalType.DATE);

        return query.getResultList();
    }

    public List<SoftwareLifecycle> getEndOfSupportPlatforms()throws Exception{
        String sSql = getSQLFromResourceFile("platform-end-of-support.sql");

        Query query = getEntityManager().createQuery(sSql);
        query.setParameter("startDate", new Date(DateUtils.parseISODate(commonSettings.getStartDate()).getTimeInMillis()));
        query.setParameter("endDate", new Date(DateUtils.parseISODate(commonSettings.getEndDate()).getTimeInMillis()));

        return query.getResultList();
    }



    public HashMap<String, boolean[]> produceApplicationMatrix(List<SoftwareLifecycle> platforms)throws Exception{
        HashMap<String, boolean[]> matrix = new LinkedHashMap<String, boolean[]>();
        cachedAppNameID = new LinkedHashMap<String, ShortInfo>();
        String sSql = getSQLFromResourceFile("report-endoflife2.sql");
        Query query = getEntityManager().createNativeQuery(sSql);

        query.setParameter("startDate", new Date(DateUtils.parseISODate(commonSettings.getStartDate()).getTimeInMillis()));
        query.setParameter("endDate", new Date(DateUtils.parseISODate(commonSettings.getEndDate()).getTimeInMillis()));



        List l = query.getResultList();
        for(int i=0; i<l.size(); i++){
            updateMatrix(matrix, (Object[]) l.get(i), platforms);
        }

        return matrix;
    }

    protected int getPlatformIndex(List<SoftwareLifecycle> list, String platform){
        int i=0;
        for(SoftwareLifecycle l: list){
            if (l.getPlatform().equals(platform)){
                return i;
            }
            i++;
        }
        return -1;
    }

    protected void updateMatrix(HashMap<String, boolean[]> matrix, Object[] record, List<SoftwareLifecycle> platforms){
        boolean[] matrixRow;

        if (!cachedAppNameID.containsKey(record[0])){
            ShortInfo shortInfo = new ShortInfo();
            shortInfo.appId = (String)record[0];
            shortInfo.appName = (String)record[1];
            shortInfo.responsible = (String)record[5];
            cachedAppNameID.put((String) record[0], shortInfo);

        }

        if (matrix.containsKey(record[0])){
            matrixRow = matrix.get(record[0]);
        }else{
            matrixRow = new boolean[platforms.size()];
            matrix.put((String)record[0], matrixRow);
        }

        int i = getPlatformIndex(platforms,(String)record[3]);
        if (i>=0){
            matrixRow[i] = true;
        }

    }


}
