package com.tmbbank.report;

import com.tmbbank.etl.BaseExtractor;
import com.tmbbank.settings.CommonSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by 29759 on 29/06/58.
 */
public class MasterReport {
    private static Logger log = LoggerFactory.getLogger(MasterReport.class);
    private CommonSettings commonSettings;
    private List<BaseReport> reports;

    public static void main(String[] args)throws Exception{
        log.debug("Started report");
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});

        MasterReport loader = (MasterReport)context.getBean("masterReport");
        loader.masterReport();
        log.debug("Finished report");
    }

    public void masterReport() throws Exception{

        for(BaseReport report: reports){
            report.generateReport(commonSettings.getLcmReportOutputWorkbook());
        }
    }


}
