package com.tmbbank.settings;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/5/14
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommonSettings {
    private String defaultWorkingDirectory;
    private String canonicalSoftwareWorkbook;
    private String canonicalApplicationWorkbook;
    private String softwareLifecycleWorkbook;
    private String applicationPortfolioWorkbook;
    private String infraPortfolioWorkbook;
    private String isaPortfolioWorkbook;

    private String lcmReportOutputWorkbook;

    private String dataSourceName;



    private String startDate;
    private String endDate;

    public String getDefaultWorkingDirectory() {
        return defaultWorkingDirectory;
    }

    public void setDefaultWorkingDirectory(String defaultWorkingDirectory) {
        this.defaultWorkingDirectory = defaultWorkingDirectory;
    }

    public String getCanonicalSoftwareWorkbook() {
        return canonicalSoftwareWorkbook;
    }

    public void setCanonicalSoftwareWorkbook(String canonicalSoftwareWorkbook) {
        this.canonicalSoftwareWorkbook = canonicalSoftwareWorkbook;
    }

    public String getCanonicalApplicationWorkbook() {
        return canonicalApplicationWorkbook;
    }

    public void setCanonicalApplicationWorkbook(String canonicalApplicationWorkbook) {
        this.canonicalApplicationWorkbook = canonicalApplicationWorkbook;
    }

    public String getSoftwareLifecycleWorkbook() {
        return softwareLifecycleWorkbook;
    }

    public void setSoftwareLifecycleWorkbook(String softwareLifecycleWorkbook) {
        this.softwareLifecycleWorkbook = softwareLifecycleWorkbook;
    }

    public String getApplicationPortfolioWorkbook() {
        return applicationPortfolioWorkbook;
    }

    public void setApplicationPortfolioWorkbook(String applicationPortfolioWorkbook) {
        this.applicationPortfolioWorkbook = applicationPortfolioWorkbook;
    }

    public String getInfraPortfolioWorkbook() {
        return infraPortfolioWorkbook;
    }

    public void setInfraPortfolioWorkbook(String infraPortfolioWorkbook) {
        this.infraPortfolioWorkbook = infraPortfolioWorkbook;
    }

    public String getIsaPortfolioWorkbook() {
        return isaPortfolioWorkbook;
    }

    public void setIsaPortfolioWorkbook(String isaPortfolioWorkbook) {
        this.isaPortfolioWorkbook = isaPortfolioWorkbook;
    }


    public String getDataSourceName() {
        return dataSourceName;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }


    public String getLcmReportOutputWorkbook() {
        return lcmReportOutputWorkbook;
    }

    public void setLcmReportOutputWorkbook(String lcmReportOutputWorkbook) {
        this.lcmReportOutputWorkbook = lcmReportOutputWorkbook;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
