package com.tmbbank.util.jpa;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

/**
 * A loose collection of JPA utilities, mainly for running in JavaSE mode.
 *
 * @author stevef
 * @version $Id: JPAUtils.java 347 2012-06-22 10:10:27Z e1040775 $
 */
public class JPAUtils {
    private static Logger _log = LoggerFactory.getLogger(JPAUtils.class);


    /**
     // * Creates a new manual transaction and executes query getting list of objects,
     * then removes then one by one from DB.
     *
     * @param query   Query in JPA-QL returning a list of entity objects to delete.
     * @param emfName EntityManagerFactory Name.
     * @throws Exception
     */
    public static void deleteObjects(final String query, String emfName) throws Exception {
        _log.info("Deleting Objects Individually for Query: {}", query);


        new ManualTransaction(emfName) {

            @Override
            public void process() throws Exception {
                List objs = em.createQuery(query).getResultList();
                for (Object entity : objs) {
                    em.remove(entity);
                }
            }
        }.invoke();


    }


    /**
     * Creates a new manual transaction and executes query getting list of objects.
     * (Actually doesn't run in explicit transaction)
     *
     * @param query   Native SQL Query no JPA-QL
     * @param emfName
     * @throws Exception
     * @deprecated Because its native SQL and confusing
     */
    public static List query(final String query, String emfName) throws Exception {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(emfName);
        EntityManager em = emf.createEntityManager();
        Query qry = em.createNativeQuery(query);
        return qry.getResultList();
    }

    /**
     * Gets entity manager and runs a query without a transaction.
     *
     * @param query   Native SQL Query no JPA-QL
     * @param emfName
     * @throws Exception
     */
    /*
    public static <T> List<T> find(final String query, String emfName, Class<T> clazz) throws Exception {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(emfName);
        EntityManager em = emf.createEntityManager();
        List<T> res = em.createQuery(query, clazz).getResultList();
        return res;
    }
      */

    /**
     * Get JDBC connection from pure JPA setup.
     * TODO: This is not valid in Hibernate 4.0
     *
     * @param emfName
     * @throws Exception
     */
    @Deprecated
    public static Connection getConnection(String emfName) throws Exception {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(emfName);
        EntityManager em = emf.createEntityManager();

        if (em.getDelegate() instanceof org.hibernate.Session) {
            //return ((org.hibernate.Session)injectedOrNotEM.getDelegate()).connection();
            throw new IllegalStateException(
                    "Hibernate 4 does not support JDBC connection()  and we write JPA code whenever possible");
        }
        return null;
    }


    public static <T> void persistObjects(final Collection<T> objs, String emfName) throws Exception {
        new ManualTransaction(emfName) {
            @Override
            public void process() throws Exception {
                _log.info("Persisting {} objects of type {}", objs.size(), objs.isEmpty()?"None":objs.iterator().next().getClass());
                for (T entity : objs) {
                    em.persist(entity);
                }
            }
        }.invoke();
    }


    public static void testConnection(String entityManagerFactoryName) throws Exception {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(entityManagerFactoryName);
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            // Some dummy query may be needed....
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            em.close();
            throw new Exception("Could not Connect to Database: " + entityManagerFactoryName, e);
        }

    }

    /**
     * Added this back in from SVN conflicts. Not recommended to use, but fixes compile problems.
     *
     * @param entityManagerFactoryName - Entity Manager Factory to get EM from
     * @return The entity manager.
     */
    public static EntityManager getEntityManager(String entityManagerFactoryName) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(entityManagerFactoryName);
        EntityManager em = emf.createEntityManager();
        return em;
    }
}
