/*
 * Copyright (c) 2009. Temenos Party Thailand, Trade Secret. Proprietary and Confidential
 */

package com.tmbbank.util.jpa;
/**
 *
 * User: Steve Franks
 * Date: Sep 27, 2009
 * Source Code Version: $Id: ManualTransaction.java 217 2012-05-20 07:30:41Z e1040775 $
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


/**
 * Helper class to invoke JPA transactions out of a J2EE container.
 * Should be able to make a decent annotation for this instead I think, something like
 *
 * TODO: It seems Seam CDI extensions can help with this. Look into
 * @version $Id: ManualTransaction.java 217 2012-05-20 07:30:41Z e1040775 $
 * @ManualJPATransaction("emfName") which wraps the method it is invoked on, not sure who/how to process the
 * annotation though.
 */
public abstract class ManualTransaction {
	private final static Logger _log = LoggerFactory.getLogger(ManualTransaction.class);
	protected EntityManagerFactory emf;
	protected EntityManager em;
	protected EntityTransaction tx;

	final protected String emfName;

    /**
     * Generic object which process() function can set to return to enclosing context in case
     * you don't want just to set a member variable of the enclosing class object.
     * @return Anything or nothing.
     */
    protected Object genericResult;

	/**
	 * Creates an entity manager and gets a transaction for given entity manager factory.
	 * It does not start the transaction though, call invoke for that.
	 *
	 * @param entityManagerFactoryName
	 * @throws Exception
	 */
	public ManualTransaction(String entityManagerFactoryName) throws Exception {
		_log.info("Using EntityManagerFactory: " + entityManagerFactoryName);
		emfName = entityManagerFactoryName;
		emf = Persistence.createEntityManagerFactory(emfName);

		em = emf.createEntityManager();

		tx = em.getTransaction();
	}

	/**
	 * Call this to start the transaction and call the process() routine in the same
	 * thread but with a new JPA Transaction (and entity manager).
	 */
	public ManualTransaction invoke() throws Exception {

		tx.begin();
		try {
			process();
			tx.commit();
            return this;
		} catch (Exception e) {
			tx.rollback();
			handleException(e);
            e.printStackTrace();
		} finally {
			em.close();
			emf.close();

		}

        return null;
    }

	public abstract void process() throws Exception;

	protected void handleException(Exception e) throws Exception {
		throw new Exception("Exception in Manual JPA Transaction in EMF " + emf, e);
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public EntityManager getEm() {
		return em;
	}

	public EntityTransaction getTx() {
		return tx;
	}

    public Object getGenericResult() {
        return genericResult;
    }

    public void setGenericResult(Object genericResult) {
        this.genericResult = genericResult;
    }
}
