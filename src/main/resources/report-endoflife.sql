--select distinct operatingsystemversion from platform_deployment_portfolio

--drop table canonical_software
select distinct  ver.appid, infra.applicationsystem, infra.servercomputerhostname,  os.platform osname, os.endofsupportdt osenddt,
case (cs.canonicalsoftwarename is null) when true then (ver.platform + ver.version) else cs.canonicalsoftwarename end as softwarename,
slc.endofsupportdt softwareenddt,
--cs.platform,ver.platform,cs.version, ver.version
 app.responsibleby
--select distinct cs.canonicalsoftwarename
from platform_deployment_portfolio infra
inner join app_platform_version2 ver on infra.number = ver.number
inner join canonical_software co on (co.platform = infra.operatingsystemversion)
inner join software_lifecycle os on (os.platform = co.canonicalsoftwarename)

left join app_portfolio app on infra.appid = app.appid
left join canonical_software cs on (cs.platform = ver.platform and cs.version = ver.version)
left join software_lifecycle slc on (cs.canonicalsoftwarename = slc.platform)


--where slc.endofsupportdt > cast('2014-12-31' as date) and slc.endofsupportdt < cast('2016-12-31' as date)
where slc.endofsupportdt < cast('2013-12-31' as date) or
os.endofsupportdt < cast('2013-12-31' as date)

order by ver.appid