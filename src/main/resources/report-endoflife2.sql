
select distinct  ver.appid, infra.applicationsystem, infra.servercomputerhostname,
case (cs.canonicalsoftwarename is null) when true then (ver.platform + ver.version) else cs.canonicalsoftwarename end as softwarename,
slc.endofsupportdt softwareenddt,

 app.responsibleby

from platform_deployment_portfolio infra
inner join app_platform_version ver on infra.number = ver.number and ver.appid = infra.appid


left join app_portfolio app on infra.appid = app.appid
left join canonical_software cs on (cs.platform = ver.platform and cs.version = ver.version)
left join software_lifecycle slc on (cs.canonicalsoftwarename = slc.platform)

where
slc.endofsupportdt > :startDate and
slc.endofsupportdt < :endDate


union


select distinct  ver.appid, infra.applicationsystem, infra.servercomputerhostname,  co.canonicalsoftwarename softwarename, os.endofsupportdt softwareenddt,

 app.responsibleby

from platform_deployment_portfolio infra
inner join app_platform_version ver on infra.number = ver.number and infra.appid = ver.appid
inner join canonical_software co on (co.platform = infra.operatingsystemversion)
inner join software_lifecycle os on (os.platform = co.canonicalsoftwarename)

left join app_portfolio app on infra.appid = app.appid


where
os.endofsupportdt > :startDate and
os.endofsupportdt < :endDate
