
-- Search for applications which are end of support

select distinct cs.canonicalsoftwarename, slc.endofsupportdt, ver.appid, infra.applicationsystem,cs.platform,ver.platform,cs.version, ver.version 
from platform_deployment_portfolio infra 
inner join app_platform_version2 ver on infra.number = ver.number
left join canonical_software cs on (cs.platform = ver.platform and cs.version = ver.version)
left join software_lifecycle slc on (cs.canonicalsoftwarename = slc.platform)
where slc.endofsupportdt < cast('2014-02-01' as date)
order by ver.appid

-- Version 2

--select * from canonical_software

--update canonical_software set version = '' where version is null


-- Search for applications which are end of support

select distinct cs.canonicalsoftwarename, slc.endofsupportdt, ver.appid, infra.applicationsystem, infra.servercomputerhostname
cs.platform,ver.platform,cs.version, ver.version, app.responsibleby
--select distinct cs.canonicalsoftwarename
from platform_deployment_portfolio infra 
inner join app_platform_version2 ver on infra.number = ver.number
left join app_portfolio app on infra.appid = app.appid
left join canonical_software cs on (cs.platform = ver.platform and cs.version = ver.version)
left join software_lifecycle slc on (cs.canonicalsoftwarename = slc.platform)

where slc.endofsupportdt < cast(today as date)
order by ver.appid





-- 



--select * from canonical_software c inner join software_lifecycle slc on c.canonicalsoftwarename = slc.platform


--select distinct infra.appid, infra.applicationsystem, app.responsibleby, infra.operatingsystemversion, infra.servercomputerhostname, 
--case (cs.canonicalsoftwarename is null) when true then (ver.platform + ver.version) else cs.canonicalsoftwarename end as platformname
--from platform_deployment_portfolio infra
--left join app_portfolio app on infra.appid = app.appid
--left join app_platform_version2 ver on infra.number = ver.number
--left join canonical_software cs on ver.platform = cs.platform and ver.version = cs.version
--order by infra.appid

--update canonical_software set version = '' where version is null


--drop table platform_deployment_portfolio;
--drop table app_platform_version2


select distinct cs.canonicalsoftwarename, slc.endofsupportdt, infra.operatingsystemversion, os.endofsupportdt, ver.appid, infra.applicationsystem, infra.servercomputerhostname
cs.platform,ver.platform,cs.version, ver.version, app.responsibleby
--select distinct cs.canonicalsoftwarename
from platform_deployment_portfolio infra 
inner join app_platform_version2 ver on infra.number = ver.number
left join app_portfolio app on infra.appid = app.appid
left join canonical_software cs on (cs.platform = ver.platform and cs.version = ver.version)
left join software_lifecycle slc on (cs.canonicalsoftwarename = slc.platform)
left join software_lifecycle os on (os.platform = infra.operatingsystemversion)

where slc.endofsupportdt > cast('2014-12-31' as date) and slc.endofsupportdt < cast('2016-12-31' as date)
order by ver.appid

