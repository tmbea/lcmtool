import com.odenzo.jexcel.JExcelUtils;
import com.tmbbank.etl.DeploymentPlatformExtractor;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 2/5/14
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExtractorTest extends DeploymentPlatformExtractor{
    private static Logger _log = LoggerFactory.getLogger(ExtractorTest.class);
    @Test
    public void testExtractExcelBorderValue()throws Exception{
        Workbook workbook = JExcelUtils.loadExcelFile(new File("C:\\Users\\Tooy\\Dropbox\\Personal\\workspaces\\TMB\\Data Center Application List as of 4 Feb 2014 by Tui.xls"));
        Sheet worksheet = workbook.getSheet(0);
        for (int i=0;i<worksheet.getRows();i++){
            Cell[] cells = worksheet.getRow(i);

            BorderLineStyle topBorderLineStyles = cells[1].getCellFormat().getBorderLine(Border.TOP);
            BorderLineStyle bottomBorderLineStyles = cells[1].getCellFormat().getBorderLine(Border.BOTTOM);
            _log.debug("Row " + (i+1) + " Examine top " + topBorderLineStyles.getDescription() + " bottom: " + bottomBorderLineStyles.getDescription());



            //System.out.println("Row " + i + " Border Value: " + );
        }

    }

    @Test
    public void testApplicationInSameServerMultiple()throws Exception{
        Workbook workbook = JExcelUtils.loadExcelFile(new File("CMDBOpenSystem201505.xls"));
        Sheet worksheet = workbook.getSheet(0);
        List<String> list = getApplicationInSameServers(1323, worksheet);

        _log.debug("list count: " + list.size() + " ");
        for (String s: list){
            _log.debug("App: " + s);
        }
    }

    @Test
    public void testLoadDeploymentPlatformPortfolio()throws Exception{
        DeploymentPlatformExtractor instance = new DeploymentPlatformExtractor();
        instance.loadRepository(null);
        instance.persist();
    }


}
