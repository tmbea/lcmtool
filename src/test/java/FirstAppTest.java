import com.tmbbank.architecture.repo.catalog.PlatformPortfolio;
import com.tmbbank.util.jpa.JPAUtils;
import org.testng.annotations.Test;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Tooy
 * Date: 9/9/13
 * Time: 11:41 PM
 * To change this template use File | Settings | File Templates.
 */


public class FirstAppTest {
    private static Logger _log = LoggerFactory.getLogger(FirstAppTest.class);

    @Test
    public void testMyApp()throws Exception{
        System.out.println("yeah");

        JPAUtils.testConnection("TMB_ARCH_REPO");


        List<PlatformPortfolio> l = new ArrayList<PlatformPortfolio>();
        l.add(new PlatformPortfolio());


        JPAUtils.persistObjects(l,"TMB_ARCH_REPO" );
    }
}
